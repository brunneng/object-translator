package org.bitbucket.brunneng.ot.property

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.Configuration

/**
 * Implement this interface to be able to convert source value to dest value on the level of separate property.
 * can be added to configuration using [Configuration.Translation.SrcProperty.translatesTo] method.
 */
interface PropertyConverter {

    /**
     * Converts [srcValue] which came from [srcProperty] to destination value which will be set into [destProperty]
     * @param srcValue source value to be converted
     * @param srcProperty source property description
     * @param destProperty destination property description
     * @return converted value
     */
    fun convertValue(srcValue: Any, srcProperty: PropertyDescription, destProperty: PropertyDescription): Any
}