package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.introspection.util.ClassMatcher
import java.lang.reflect.Type

/**
 * Controller for operations which are needed during translation of collections.
 * Extend from this interface to support your own collection classes.
 */
interface CollectionController : ClassMatcher {

    override fun isMatched(clazz: Class<*>): Boolean {
        return isCollectionTypeSupported(clazz)
    }

    /**
     * @param possibleCollectionClass class which can be class of collection
     *
     * @return true, if this collection controller supports given collection class
     */
    fun isCollectionTypeSupported(possibleCollectionClass: Class<*>): Boolean

    /**
     * @param collection collection object
     *
     * @return values of given collection
     */
    fun getCollectionValues(collection: Any): List<Any?>

    /**
     * Attempts to find component type of the given collection
     *
     * @param collectionClass collection class
     * @param accessorValueType type information about the given collection. If present, can be used to get generic parameter
     *
     * @return component type of given collection or null, if it can't be found
     */
    fun findCollectionComponentTypeByAccessor(collectionClass: Class<*>, accessorValueType: AccessorValueType?): Type?

    /**
     * Attempts to find component type of the given collection
     *
     * @param collectionClass collection class
     * @param collectionType type information about the given collection. If present, can be used to get generic parameter
     *
     * @return component type of given collection or null, if it can't be found
     */
    fun findCollectionComponentType(collectionClass: Class<*>, collectionType: Type?): Type?

    /**
     * Takes collection as input, and sets to it given list of values. If collection is not modifiable then new
     * collection object can be created.
     * @param collection collection object
     * @param values values which should be set in collection
     *
     * @return collection, which has given values
     */
    fun setValues(collection: Any, values: List<Any?>): Any

}
