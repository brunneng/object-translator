package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import java.lang.reflect.Type

/**
 * Controller for internal usage which supports mapping of Arrays.
 *
 * @see CollectionController
 */
class ArrayCollectionController : CollectionController {

    override fun isCollectionTypeSupported(possibleCollectionClass: Class<*>): Boolean {
        return possibleCollectionClass.isArray
    }

    override fun getCollectionValues(collection: Any): List<Any?> {
        val res = ArrayList<Any?>()
        val length = java.lang.reflect.Array.getLength(collection)
        for (i in 0 until length) {
            res += java.lang.reflect.Array.get(collection, i)
        }

        return res
    }

    override fun findCollectionComponentTypeByAccessor(
        collectionClass: Class<*>,
        accessorValueType: AccessorValueType?
    ): Type? {
        return collectionClass.componentType
    }

    override fun findCollectionComponentType(collectionClass: Class<*>, collectionType: Type?): Type? {
        return collectionClass.componentType
    }

    override fun setValues(collection: Any, values: List<Any?>): Any {
        var res = collection as Array<Any?>
        if (java.lang.reflect.Array.getLength(collection) != values.size) {
            res = java.lang.reflect.Array.newInstance(res.javaClass.componentType, values.size) as Array<Any?>
        }
        for ((i, value) in values.withIndex()) {
            res[i] = value
        }
        return res
    }
}
