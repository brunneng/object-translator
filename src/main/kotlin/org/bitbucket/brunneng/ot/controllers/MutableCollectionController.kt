package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Type

/**
 * Controller for internal usage which supports mapping of MutableCollection's (in terms of kotlin). It means support
 * of such commonly used maps as ArrayList, LinkedList, HashSet etc.
 *
 * @see CollectionController
 * @see MutableCollection
 */
class MutableCollectionController : CollectionController {

    override fun isCollectionTypeSupported(possibleCollectionClass: Class<*>): Boolean {
        return MutableCollection::class.java.isAssignableFrom(possibleCollectionClass)
    }

    override fun getCollectionValues(collection: Any): List<Any?> {
        val res = ArrayList<Any?>()
        res.addAll(collection as MutableCollection<*>)
        return res
    }

    override fun findCollectionComponentTypeByAccessor(
        collectionClass: Class<*>,
        accessorValueType: AccessorValueType?
    ): Type? {
        return accessorValueType?.getTypeOfGenericParameter(0)
    }

    override fun findCollectionComponentType(collectionClass: Class<*>, collectionType: Type?): Type? {
        if (collectionType == null) {
            return null
        }

        return ReflectionUtil.getActualTypeVariablesTypes(collectionType).values.firstOrNull()
    }

    override fun setValues(collection: Any, values: List<Any?>): Any {
        val c = collection as MutableCollection<Any?>
        c.clear()
        c.addAll(values)
        return c
    }
}
