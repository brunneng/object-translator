package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Type

/**
 * Controller for internal usage which supports mapping of [MutableMap]s (in terms of kotlin). It means support of
 * such commonly used maps as: [HashMap][java.util.HashMap], [LinkedHashMap][java.util.LinkedHashMap],
 * [TreeMap][java.util.TreeMap] etc.
 *
 * @see MapController
 * @see MutableMap
 */
class MutableMapController : MapController {

    override fun isMapTypeSupported(possibleMapClass: Class<*>): Boolean {
        return MutableMap::class.java.isAssignableFrom(possibleMapClass)
    }

    override fun getMapContent(map: Any): Map<Any, Any?> {
        val res = HashMap<Any, Any?>()
        res.putAll(map as MutableMap<Any, Any?>)
        return res
    }

    override fun findMapKeyTypeByAccessor(mapClass: Class<*>, accessorValueType: AccessorValueType?): Type? {
        return accessorValueType?.getTypeOfGenericParameter(0)
    }

    override fun findMapValueTypeByAccessor(mapClass: Class<*>, accessorValueType: AccessorValueType?): Type? {
        return accessorValueType?.getTypeOfGenericParameter(1)
    }

    override fun findMapValueType(mapClass: Class<*>, mapType: Type): Type? {
        val actualTypeVariablesTypes = ReflectionUtil.getActualTypeVariablesTypes(mapType)

        val valueTypeParameter = MutableMap::class.java.typeParameters[1]
        if (actualTypeVariablesTypes.contains(valueTypeParameter)) {
            return actualTypeVariablesTypes[valueTypeParameter]
        }

        for ((key, type) in actualTypeVariablesTypes) {
            if (key.name == "V") {
                return type
            }
        }

        return actualTypeVariablesTypes.values.lastOrNull()
    }

    override fun setContent(map: Any, values: Map<Any, Any?>): Any {
        val res = (map as MutableMap<Any, Any?>)
        res.clear()
        res.putAll(values)
        return res
    }
}
