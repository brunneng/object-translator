package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.introspection.util.ClassMatcher
import java.lang.reflect.Type

/**
 * Controller for operations which are needed during translation of maps.
 * Extend from this interface to support your own map classes.
 */
interface MapController : ClassMatcher {

    override fun isMatched(clazz: Class<*>): Boolean {
        return isMapTypeSupported(clazz)
    }

    /**
     * @param possibleMapClass class which can be a class of collection
     *
     * @return true, if this map controller supports given map class
     */
    fun isMapTypeSupported(possibleMapClass: Class<*>): Boolean

    /**
     * @param map map object
     *
     * @return values of given map
     */
    fun getMapContent(map: Any): Map<Any, Any?>

    /**
     * Attempts to find key type of given map
     *
     * @param mapClass map class
     * @param accessorValueType type information about given map. If present, can be used to get generic parameter
     *
     * @return value type of given map or null, if it can't be found
     */
    fun findMapKeyTypeByAccessor(mapClass: Class<*>, accessorValueType: AccessorValueType?): Type?

    /**
     * Attempts to find value type of given map
     *
     * @param mapClass map class
     * @param accessorValueType type information about given map. If present, can be used to get generic parameter
     *
     * @return value type of given map or null, if it can't be found
     */
    fun findMapValueTypeByAccessor(mapClass: Class<*>, accessorValueType: AccessorValueType?): Type?

    /**
     * Attempts to find value type of given map
     *
     * @param mapClass map class
     * @param mapType type information about given map. If present, can be used to get generic parameter
     *
     * @return value type of given map or null, if it can't be found
     */
    fun findMapValueType(mapClass: Class<*>, mapType: Type): Type?

    /**
     * Takes map as input, and sets to it the content of given map. If map is not modifiable then a new
     * map object can be created.
     * @param map map object
     * @param values values which should be set in collection
     *
     * @return map, which has given values
     */
    fun setContent(map: Any, values: Map<Any, Any?>): Any

}
