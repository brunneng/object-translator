package org.bitbucket.brunneng.ot.exceptions

class ImpossibleToConvertException(message: String) : ConfigurationException(message)
