package org.bitbucket.brunneng.ot.exceptions

class TranslationAlreadyConfiguredException(message: String) : ConfigurationException(message)
