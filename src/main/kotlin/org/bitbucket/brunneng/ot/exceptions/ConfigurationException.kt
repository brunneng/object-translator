package org.bitbucket.brunneng.ot.exceptions

open class ConfigurationException(message: String) : RuntimeException(message)
