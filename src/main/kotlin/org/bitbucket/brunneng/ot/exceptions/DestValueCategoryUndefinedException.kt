package org.bitbucket.brunneng.ot.exceptions

class DestValueCategoryUndefinedException(message: String) : TranslationException(message)
