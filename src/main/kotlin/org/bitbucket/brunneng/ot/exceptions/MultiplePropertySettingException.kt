package org.bitbucket.brunneng.ot.exceptions

class MultiplePropertySettingException(message: String, e: Exception? = null) : TranslationException(message, e)
