package org.bitbucket.brunneng.ot.exceptions

class PropertyMappingException(message: String, e: Exception? = null) : TranslationException(message, e)
