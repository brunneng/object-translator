package org.bitbucket.brunneng.ot.exceptions

class WrongConfigurationOrderException(message: String) : ConfigurationException(message)
