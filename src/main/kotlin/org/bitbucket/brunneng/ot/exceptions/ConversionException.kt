package org.bitbucket.brunneng.ot.exceptions

class ConversionException(message: String, e: Exception? = null) : TranslationException(message, e)
