package org.bitbucket.brunneng.ot.exceptions

class NoGettersException(message: String) : ConfigurationException(message)
