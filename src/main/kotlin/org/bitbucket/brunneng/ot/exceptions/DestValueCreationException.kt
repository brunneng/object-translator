package org.bitbucket.brunneng.ot.exceptions

class DestValueCreationException(message: String, e: Exception? = null) : TranslationException(message, e)
