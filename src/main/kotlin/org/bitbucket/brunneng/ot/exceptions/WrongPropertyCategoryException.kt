package org.bitbucket.brunneng.ot.exceptions

class WrongPropertyCategoryException(message: String) : ConfigurationException(message)
