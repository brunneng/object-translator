package org.bitbucket.brunneng.ot.exceptions

class CantResolvePropertyPathException(message: String, e: Exception? = null) : TranslationException(message, e)
