package org.bitbucket.brunneng.ot.exceptions

class NoSettersException(message: String) : ConfigurationException(message)
