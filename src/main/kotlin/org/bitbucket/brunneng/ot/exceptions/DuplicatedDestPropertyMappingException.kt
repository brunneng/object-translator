package org.bitbucket.brunneng.ot.exceptions

class DuplicatedDestPropertyMappingException(message: String) : ConfigurationException(message)
