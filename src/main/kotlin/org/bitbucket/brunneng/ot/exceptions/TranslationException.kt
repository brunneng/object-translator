package org.bitbucket.brunneng.ot.exceptions

open class TranslationException(message: String, e: Exception? = null) : RuntimeException(message, e)
