package org.bitbucket.brunneng.ot.exceptions

class DefaultValueWrongClassException(message: String) : ConfigurationException(message)
