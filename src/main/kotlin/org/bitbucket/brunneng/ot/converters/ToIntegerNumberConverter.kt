package org.bitbucket.brunneng.ot.converters

/**
 * Converter which converts [Byte] or [Short] to [Integer]
 */
class ToIntegerNumberConverter : AbstractNumberConverter(
    listOf(
        java.lang.Byte::class.java,
        java.lang.Short::class.java
    ),
    listOf(java.lang.Integer::class.java)
)
