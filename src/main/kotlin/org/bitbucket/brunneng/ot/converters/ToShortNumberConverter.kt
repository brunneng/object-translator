package org.bitbucket.brunneng.ot.converters

/**
 * Converter which converts [Byte] to [Short]
 */
class ToShortNumberConverter : AbstractNumberConverter(
    listOf(java.lang.Byte::class.java),
    listOf(java.lang.Short::class.java)
)
