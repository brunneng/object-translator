package org.bitbucket.brunneng.ot.converters

/**
 * Converter which converts [Byte], [Short], [Integer] or [Long] to [Float].
 */
class ToFloatNumberConverter : AbstractNumberConverter(
    listOf(
        java.lang.Byte::class.java,
        java.lang.Short::class.java,
        java.lang.Integer::class.java,
        java.lang.Long::class.java
    ),
    listOf(java.lang.Float::class.java)
)
