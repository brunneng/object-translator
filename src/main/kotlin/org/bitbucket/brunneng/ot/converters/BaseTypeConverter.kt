package org.bitbucket.brunneng.ot.converters

/**
 * Base abstract implementation of type converter which can convert from any of given [srcClasses],
 * to any of [destClasses]. Note, that classes assignable to given are also supported.
 *
 * @param srcClasses source classes, supported by this converter
 * @param destClasses destination classes supported by this converter
 */
abstract class BaseTypeConverter(protected val srcClasses: List<Class<*>>, protected val destClasses: List<Class<*>>) :
    TypeConverter {

    override fun canConvert(srcClass: Class<*>, destClass: Class<*>): Boolean {
        return srcClasses.stream().anyMatch { c -> c.isAssignableFrom(srcClass) } &&
                destClasses.stream().anyMatch { c -> c.isAssignableFrom(destClass) }
    }
}
