package org.bitbucket.brunneng.ot.converters

import org.bitbucket.brunneng.ot.exceptions.ConversionException
import java.sql.Time
import java.sql.Timestamp
import java.time.*
import java.util.*

/**
 * Converter to convert different types of dates between each other.
 * Supported dates: [Date], [Calendar], [Instant], [LocalDateTime], [ZonedDateTime], [OffsetDateTime].
 * It uses epoch milliseconds as intermediate representation, so milliseconds are maximum supported precision.
 * If src type does not support timezones (like [LocalDateTime]) converter assumes it's represented in timezone
 * [srcZone]. To convert to destination date timezone [destZone].
 *
 * @param srcZone timezone is used if source date is of type [LocalDateTime], default value is system default timezone
 * @param destZone timezone is used to convert intermediate epoch millisecond to target date of types [LocalDateTime],
 * [ZonedDateTime], [OffsetDateTime]; default value is [srcZone]
 */
class DatesConverter(
    var srcZone: ZoneId = ZoneId.systemDefault(),
    var destZone: ZoneId = srcZone
) : BaseTypeConverter(
    listOf(
        Date::class.java, Calendar::class.java, Instant::class.java, LocalDateTime::class.java,
        ZonedDateTime::class.java, OffsetDateTime::class.java
    ),
    listOf(
        Date::class.java, Calendar::class.java, Instant::class.java, LocalDateTime::class.java,
        ZonedDateTime::class.java, OffsetDateTime::class.java
    )
) {

    protected fun getTimeInMillis(obj: Any): Long {
        if (obj is Date) {
            return obj.time
        }
        if (obj is Calendar) {
            return obj.time.time
        }
        if (obj is Instant) {
            return obj.toEpochMilli()
        }
        if (obj is LocalDateTime) {
            return obj.atZone(srcZone).toInstant().toEpochMilli()
        }
        if (obj is ZonedDateTime) {
            return obj.toInstant().toEpochMilli()
        }
        if (obj is OffsetDateTime) {
            return obj.toInstant().toEpochMilli()
        }

        throw ConversionException("Can't get milliseconds from $obj")
    }

    protected fun convertToTargetDate(timeInMillis: Long, destClass: Class<*>): Any {

        if (Date::class.java == destClass) {
            return Date(timeInMillis)
        }
        if (java.sql.Date::class.java == destClass) {
            return java.sql.Date(timeInMillis)
        }
        if (Timestamp::class.java == destClass) {
            return Timestamp(timeInMillis)
        }
        if (Time::class.java == destClass) {
            return Time(timeInMillis)
        }
        if (Calendar::class.java == destClass) {
            val c = Calendar.getInstance()
            c.time = Date(timeInMillis)
            return c
        }
        if (GregorianCalendar::class.java == destClass) {
            val c = GregorianCalendar.getInstance()
            c.time = Date(timeInMillis)
            return c
        }
        if (Instant::class.java == destClass) {
            return Instant.ofEpochMilli(timeInMillis)
        }
        if (LocalDateTime::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return LocalDateTime.ofInstant(instant, destZone)
        }
        if (ZonedDateTime::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return ZonedDateTime.ofInstant(instant, destZone)
        }
        if (OffsetDateTime::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return OffsetDateTime.ofInstant(instant, destZone)
        }

        throw ConversionException("Date of type $destClass is not supported")
    }

    override fun convert(srcObject: Any, destClass: Class<*>): Any {
        return convertToTargetDate(getTimeInMillis(srcObject), destClass)
    }
}
