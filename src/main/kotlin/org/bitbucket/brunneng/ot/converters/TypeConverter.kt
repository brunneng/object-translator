package org.bitbucket.brunneng.ot.converters

/**
 * Converter between instances of given types. Typically it's used to convert between incompatible value types.
 * After conversion the value is no longer changed. If this is bean - it's not mapped but directly assigned to
 * destination property; if it's collection or map - it's not merged, as it is usually done for collections and maps.
 */
interface TypeConverter {

    /**
     * @param srcClass source class
     * @param destClass destination class
     * @return can this converter convert instances of source class to destination class?
     */
    fun canConvert(srcClass: Class<*>, destClass: Class<*>): Boolean

    /**
     * @param srcObject source object - instance of source class
     * @param destClass destination class
     * @return converted value of destination class
     */
    fun convert(srcObject: Any, destClass: Class<*>): Any
}