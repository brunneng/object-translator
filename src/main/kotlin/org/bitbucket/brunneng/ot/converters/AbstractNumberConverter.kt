package org.bitbucket.brunneng.ot.converters

import org.bitbucket.brunneng.ot.exceptions.ConversionException

/**
 * Abstract converter to convert numbers to specific other number type: Double, Float, Integer, Long, Byte or Short.
 *
 * @see BaseTypeConverter
 * @see Number
 */
abstract class AbstractNumberConverter(srcClasses: List<Class<*>>, destClasses: List<Class<*>>) :
    BaseTypeConverter(srcClasses, destClasses) {

    override fun convert(srcObject: Any, destClass: Class<*>): Any {
        val number = srcObject as Number

        if (destClass == java.lang.Double::class.java || destClass == java.lang.Double.TYPE) {
            return number.toDouble()
        } else if (destClass == java.lang.Float::class.java || destClass == java.lang.Float.TYPE) {
            return number.toFloat()
        } else if (destClass == java.lang.Integer::class.java || destClass == Integer.TYPE) {
            return number.toInt()
        } else if (destClass == java.lang.Long::class.java || destClass == java.lang.Long.TYPE) {
            return number.toLong()
        } else if (destClass == java.lang.Byte::class.java || destClass == java.lang.Byte.TYPE) {
            return number.toByte()
        } else if (destClass == java.lang.Short::class.java || destClass == java.lang.Short.TYPE) {
            return number.toShort()
        } else {
            throw ConversionException("Type of value $destClass is not supported")
        }
    }
}
