package org.bitbucket.brunneng.ot.converters

import org.bitbucket.brunneng.ot.exceptions.ConversionException
import java.util.*
import java.util.stream.Collectors

/**
 * Converter which converts between two enums if they have some common enum constants. If during translation
 * source enum constant is not present in destination enum then [ConversionException] will be thrown.
 */
class EnumsConverter : TypeConverter {

    override fun canConvert(srcClass: Class<*>, destClass: Class<*>): Boolean {
        if (!srcClass.isEnum || !destClass.isEnum) {
            return false
        }

        val srcEnumNames = Arrays.stream(srcClass.enumConstants)
            .map { v -> (v as Enum<*>).name }.collect(Collectors.toSet())

        val destEnumNames = Arrays.stream(destClass.enumConstants)
            .map { v -> (v as Enum<*>).name }.collect(Collectors.toSet())

        return srcEnumNames.intersect(destEnumNames).isNotEmpty()
    }

    override fun convert(srcObject: Any, destClass: Class<*>): Any {
        val name = (srcObject as Enum<*>).name
        try {
            @Suppress("UPPER_BOUND_VIOLATED")
            return java.lang.Enum.valueOf<Enum<*>>(destClass as Class<Enum<*>>, name)
        } catch (e: Exception) {
            throw ConversionException("Can't convert $name to enum of type $destClass", e)
        }

    }

}
