package org.bitbucket.brunneng.ot.converters

/**
 * Converter which converts [Byte], [Short] or [Integer] to [Long].
 */
class ToLongNumberConverter : AbstractNumberConverter(
    listOf(
        java.lang.Byte::class.java,
        java.lang.Short::class.java,
        java.lang.Integer::class.java
    ),
    listOf(java.lang.Long::class.java)
)
