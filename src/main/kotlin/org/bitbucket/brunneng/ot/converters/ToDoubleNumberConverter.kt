package org.bitbucket.brunneng.ot.converters

/**
 * Converter which converts [Byte], [Short], [Integer], [Long] or [Float] to [Double].
 */
class ToDoubleNumberConverter : AbstractNumberConverter(
    listOf(
        java.lang.Byte::class.java,
        java.lang.Short::class.java,
        java.lang.Integer::class.java,
        java.lang.Long::class.java,
        java.lang.Float::class.java
    ),
    listOf(java.lang.Double::class.java)
)
