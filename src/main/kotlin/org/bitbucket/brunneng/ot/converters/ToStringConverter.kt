package org.bitbucket.brunneng.ot.converters

/**
 * Converter which converts object of any class to [String] by calling [Object.toString] method
 */
class ToStringConverter : TypeConverter {

    override fun canConvert(srcClass: Class<*>, destClass: Class<*>): Boolean {
        return String::class.java == destClass
    }

    override fun convert(srcObject: Any, destClass: Class<*>): Any {
        return srcObject.toString()
    }
}
