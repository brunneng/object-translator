package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.log.TranslationLogEntry
import org.bitbucket.brunneng.ot.log.TreeLogEntry

/**
 * Represents result of translation with tree log which describes progress of translation.
 * For description of usages of tree log see [TreeLogEntry].
 *
 * @param destValue destination value - result of translation
 * @param logRoot root of tree log
 */
class ResultWithLog<T>(val destValue: T, val logRoot: TranslationLogEntry)