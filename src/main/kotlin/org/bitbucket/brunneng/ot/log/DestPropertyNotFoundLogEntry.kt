package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.PropertyDescription

class DestPropertyNotFoundLogEntry(val srcProperty: PropertyDescription) : TreeLogEntry() {
    override fun getMessage(): String {
        return "Source property [$srcProperty] skipped because destination property is not found"
    }
}
