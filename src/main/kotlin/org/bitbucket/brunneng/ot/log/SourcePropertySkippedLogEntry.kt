package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.PropertyDescription

class SourcePropertySkippedLogEntry(
    val property: PropertyDescription,
    val dynamicallySkipped: Boolean
) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Property [$property] skipped" + if (dynamicallySkipped) " dynamically" else ""
    }
}
