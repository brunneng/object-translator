package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.PropertyDescription

class SetPropertyValueLogEntry(
    val propertyDescription: PropertyDescription,
    val destValue: Any?
) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Set value [$destValue] to property [$propertyDescription]"
    }
}
