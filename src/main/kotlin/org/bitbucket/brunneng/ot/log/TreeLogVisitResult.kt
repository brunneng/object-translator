package org.bitbucket.brunneng.ot.log

/**
 * Returned by [TreeLogVisitor] and controls process of traversing tree log in method [TreeLogEntry.traverse]
 */
enum class TreeLogVisitResult {

    /**
     * continue process of traversing
     */
    CONTINUE,

    /**
     * stop expanding (traversing) children of current node, maybe because there is nothing interesting in them.
     */
    STOP_EXPAND_CHILDREN,

    /**
     * stop all process of traversing completely. Should be returned if you have found what you are looking for in
     * tree log, and there is no sense to continue.
     */
    STOP
}