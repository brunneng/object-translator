package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.property.PropertyConverter

class PropertyConverterUsedLogEntry(
    val propertyConverter: PropertyConverter,
    val srcPropertyDescription: PropertyDescription,
    val destPropertyDescription: PropertyDescription,
    val destValue: Any
) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Property converter [$propertyConverter] used to convert from [$srcPropertyDescription] " +
                "to [$destPropertyDescription] and value [$destValue] returned"
    }
}
