package org.bitbucket.brunneng.ot.log

interface TreeLogVisitor {

    fun onVisit(entry: TreeLogEntry): TreeLogVisitResult

}