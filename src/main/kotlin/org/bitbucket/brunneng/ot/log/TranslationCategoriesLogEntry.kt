package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.TypeCategory

class TranslationCategoriesLogEntry(val srcTypeCategory: TypeCategory, val destTypeCategory: TypeCategory) :
    TreeLogEntry() {

    override fun getMessage(): String {
        return "Translation of [$srcTypeCategory] to [$destTypeCategory]"
    }

}
