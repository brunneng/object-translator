package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.PropertyDescription

class MapPropertyLogEntry(val srcProperty: PropertyDescription, val destProperty: PropertyDescription) :
    TreeLogEntry() {

    override fun getMessage(): String {
        return "Map property [$srcProperty] to [$destProperty]"
    }
}
