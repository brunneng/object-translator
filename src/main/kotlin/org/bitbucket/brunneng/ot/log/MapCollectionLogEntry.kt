package org.bitbucket.brunneng.ot.log

class MapCollectionLogEntry(val srcCollection: Any, val destCollection: Any) : TreeLogEntry() {
    override fun getMessage(): String {
        return "Map collection [$srcCollection] to collection [$destCollection]"
    }
}