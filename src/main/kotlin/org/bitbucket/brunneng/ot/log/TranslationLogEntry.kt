package org.bitbucket.brunneng.ot.log

class TranslationLogEntry(val srcObject: Any, val destClass: Class<*>) : TreeLogEntry() {
    override fun getMessage(): String {
        return "Translation [$srcObject] to destination class [${destClass.name}]"
    }
}