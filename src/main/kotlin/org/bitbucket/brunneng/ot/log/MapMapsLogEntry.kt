package org.bitbucket.brunneng.ot.log

class MapMapsLogEntry(val srcMap: Any, val destMap: Any) : TreeLogEntry() {
    override fun getMessage(): String {
        return "Map maps: [$srcMap] to [$destMap]"
    }
}