package org.bitbucket.brunneng.ot.log

class ClarifiedDestValueClassLogEntry(val destObjectClass: Class<*>, val clarifiedObjectClass: Class<*>) :
    TreeLogEntry() {

    override fun getMessage(): String {
        return "Destination object class [${destObjectClass.name}] clarified to [${clarifiedObjectClass.name}]"
    }
}
