package org.bitbucket.brunneng.ot.log

class MapBeanLogEntry(val srcBean: Any, val destBean: Any) : TreeLogEntry() {
    override fun getMessage(): String {
        return "Map bean [$srcBean] to bean [$destBean]"
    }
}