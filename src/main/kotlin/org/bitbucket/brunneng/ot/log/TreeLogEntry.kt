package org.bitbucket.brunneng.ot.log

/**
 * Tree log describes the whole process of translation, and serves 2 purposes:
 * * debug and understand why entity is translated/mapped in some way, if it's not as expected. It can be
 * misunderstanding of how library works, wrong configuration or bug in library (hope not, but who knows!).
 * All tree log entries have overridden method [Object.toString], so you can just call this method on root
 * of tree log and analyze produced string.
 * * It's audit trail of made changes. You can programmatically analyze it and, for example, save
 * some additional information to db if some properties have been changed.
 */
abstract class TreeLogEntry {
    private val children = ArrayList<TreeLogEntry>()
    var parent: TreeLogEntry? = null

    internal fun add(child: TreeLogEntry): TreeLogEntry {
        child.parent = this
        children += child
        return child
    }

    /**
     * @return message/description of given node
     */
    abstract fun getMessage(): String

    private fun toStringInternal(depth: Int): String {
        val sb = StringBuilder()
        sb.append(this::class.java.simpleName).append(": ")
        sb.append(getMessage()).append(System.lineSeparator())
        for (child in children) {
            sb.append("   ".repeat(depth)).append(child.toStringInternal(depth + 1))
        }
        return sb.toString()
    }

    /**
     * Traverses tree log recursively applying visitor to every node. This method can be used if you want to
     * find something interesting within tree log and react to it. Process of traversing is controlled by
     * [TreeLogVisitResult] which is returned by [visitor]:
     * * [TreeLogVisitResult.CONTINUE] process is continued
     * * [TreeLogVisitResult.STOP_EXPAND_CHILDREN] stop expanding (traversing) children of the current node, maybe because
     * there is nothing interesting in them.
     * * [TreeLogVisitResult.STOP] stop process of traversing completely. Should be returned if you have found what
     * you are looking for in tree log, and there is no sense to continue.
     *
     * @param visitor visitor which will be called on every node until stop commands are returned.
     *
     * @return result which controls how process of traversing should be continued by parent node.
     */
    fun traverse(visitor: TreeLogVisitor): TreeLogVisitResult {
        val visitResult = visitor.onVisit(this)
        if (visitResult == TreeLogVisitResult.CONTINUE) {
            for (child in children) {
                if (child.traverse(visitor) == TreeLogVisitResult.STOP) {
                    return TreeLogVisitResult.STOP
                }
            }
        }
        return visitResult
    }

    /**
     * @param logEntriesType type of log entries to find
     *
     * @return log entries of given [logEntriesType]
     */
    fun <T : TreeLogEntry> findEntriesOfType(logEntriesType: Class<T>): List<T> {
        val res = ArrayList<T>()
        traverse(object : TreeLogVisitor {
            override fun onVisit(entry: TreeLogEntry): TreeLogVisitResult {
                if (logEntriesType.isAssignableFrom(entry::class.java)) {
                    res.add(entry as T)
                }
                return TreeLogVisitResult.CONTINUE
            }
        })
        return res
    }

    override fun toString(): String {
        return toStringInternal(1)
    }


}
