package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.ot.converters.TypeConverter

class TypeConverterUsedLogEntry(val typeConverter: TypeConverter, val conversionResult: Any) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Type converter [$typeConverter] used and value [$conversionResult] is returned"
    }
}