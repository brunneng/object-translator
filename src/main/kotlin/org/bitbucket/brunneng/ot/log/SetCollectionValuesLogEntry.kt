package org.bitbucket.brunneng.ot.log

class SetCollectionValuesLogEntry(val values: List<Any?>) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Set values $values to collection"
    }
}
