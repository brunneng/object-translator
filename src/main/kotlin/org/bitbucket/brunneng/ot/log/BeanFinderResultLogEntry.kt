package org.bitbucket.brunneng.ot.log

class BeanFinderResultLogEntry(val id: Any, val beanClass: Class<*>, val bean: Any?) : TreeLogEntry() {
    override fun getMessage(): String {
        if (bean != null) {
            return "Destination bean $bean is found by id=$id"
        }
        return "Destination bean of class [${beanClass.name}] is not found by id=$id"
    }
}