package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.ot.Configuration

open class MappingOfDestPathSkippedLogEntry(val property: Configuration.Translation.DestProperty) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Mapping of [$property] is skipped because it ends with identifier and source identifier value is null"
    }
}
