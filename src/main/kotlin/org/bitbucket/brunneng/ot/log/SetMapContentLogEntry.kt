package org.bitbucket.brunneng.ot.log

class SetMapContentLogEntry(val content: Map<Any, Any?>) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Set content $content to map"
    }
}
