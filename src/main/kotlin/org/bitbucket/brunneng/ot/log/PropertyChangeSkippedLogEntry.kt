package org.bitbucket.brunneng.ot.log

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.Configuration.ChangeType

open class PropertyChangeSkippedLogEntry(
    val property: PropertyDescription,
    vararg val changeTypes: ChangeType
) : TreeLogEntry() {

    override fun getMessage(): String {
        return "Change types ${changeTypes.toList()} skipped for destination property [$property]"
    }
}
