package org.bitbucket.brunneng.ot.log

class ValueReturnedLogEntry(val value: Any?) : TreeLogEntry() {
    override fun getMessage(): String {
        return "Value [$value] is returned"
    }
}