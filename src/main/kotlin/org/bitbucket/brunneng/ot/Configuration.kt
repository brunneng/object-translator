package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.introspection.AbstractBeanIntrospector
import org.bitbucket.brunneng.introspection.AbstractIntrospector
import org.bitbucket.brunneng.introspection.AbstractProperty
import org.bitbucket.brunneng.introspection.property.*
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import org.bitbucket.brunneng.ot.Configuration.ChangeType.*
import org.bitbucket.brunneng.ot.beans.BeanFinder
import org.bitbucket.brunneng.ot.controllers.*
import org.bitbucket.brunneng.ot.converters.*
import org.bitbucket.brunneng.ot.creators.ArrayDestValueCreator
import org.bitbucket.brunneng.ot.creators.ConstructorWithoutArgsDestValueCreator
import org.bitbucket.brunneng.ot.creators.DefaultImplDestValueCreator
import org.bitbucket.brunneng.ot.creators.DestValueCreator
import org.bitbucket.brunneng.ot.exceptions.*
import org.bitbucket.brunneng.ot.property.PropertyConverter
import org.bitbucket.brunneng.ot.util.TypeUtil
import org.bitbucket.brunneng.ot.util.getOrPutSupportedNulls
import java.lang.reflect.Type
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantReadWriteLock
import java.util.stream.Stream
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * Central class to programmatically configure all translation process.
 * Prepared configuration is passed to [ObjectTranslator]. Configuration can be changed after translation but
 * it’s not recommended to do it . If you need to have several different logics of translation of the same classes then
 * it’s better to create several configuration objects with corresponding [ObjectTranslator]'s.
 * After you prepare configuration object please call [Configuration.validate] to make sure that everything is ok.
 *
 * Through configuration it's possible to access nested *sub-configurations*, such as:
 *
 * [Configuration.Bean],
 *
 * [Configuration.Bean.Property],
 *
 * [Configuration.PropertyDetectorConfig]
 *
 * [Configuration.Translation],
 *
 * [Configuration.Translation.SrcProperty],
 *
 * [Configuration.Translation.DestProperty]
 *
 * A  few examples:
 *
 *    configuration.beanOfClass(Order.class)
 *
 * returns [Configuration.Bean] from which you can  access all configuration related to Order class.
 *
 *    configuration.beanOfClass(Order.class).property("title")
 *
 * returns [Configuration.Bean.Property] with configurations related to property "title" of Order. This
 * line will throw exception if such property doesn't exist, so you will be able to find possible typos in
 * configuration earlier, before actual translation.
 *
 *    configuration.beanOfClass(Order.class).translationTo(OrderDTO.class)
 *
 * returns [Configuration.Translation] with configuration about particular translation between Order and OrderDTO.
 *
 * Configuration is **thread safe** and uses fine grained locking to achieve this.
 */
class Configuration : AbstractIntrospector<Configuration.Bean.Property, Configuration.Bean>() {

    private val collectionControllers = LinkedList<CollectionController>()
    private val mapControllers = LinkedList<MapController>()

    private val baseTypeConverters = ArrayList<TypeConverter>()
    private val customTypeConverters = ArrayList<TypeConverter>()

    private val baseDestValueCreators = ArrayList<DestValueCreator>()
    private val customDestValueCreators = ArrayList<DestValueCreator>()

    private val allTranslations = ArrayList<Translation>()

    private val typeConvertersCache = HashMap<List<Class<*>>, TypeConverter?>()
    private val destValueCreatorCache = HashMap<Class<*>, DestValueCreator?>()
    private val collectionControllersCache = ConcurrentHashMap<Class<*>, CollectionController>()
    private val mapControllersCache = ConcurrentHashMap<Class<*>, MapController>()

    init {
        collectionClassMatchers.clear()
        val arrayCollectionController = ArrayCollectionController()
        val mutableCollectionController = MutableCollectionController()

        collectionControllers += arrayCollectionController
        collectionControllers += mutableCollectionController

        collectionClassMatchers += arrayCollectionController
        collectionClassMatchers += mutableCollectionController

        val mutableMapController = MutableMapController()
        mapControllers += mutableMapController
        mapClassMatchers += mutableMapController

        baseTypeConverters += DatesConverter()
        baseTypeConverters += ToDoubleNumberConverter()
        baseTypeConverters += ToFloatNumberConverter()
        baseTypeConverters += ToLongNumberConverter()
        baseTypeConverters += ToIntegerNumberConverter()
        baseTypeConverters += ToShortNumberConverter()
        baseTypeConverters += ToStringConverter()
        baseTypeConverters += EnumsConverter()

        baseDestValueCreators += ArrayDestValueCreator()
        baseDestValueCreators += ConstructorWithoutArgsDestValueCreator()
        baseDestValueCreators += DefaultImplDestValueCreator()
    }

    /**
     * Registers new [collection controller][CollectionController]. It's used to teach translator to work with new
     * types of collections.
     *
     * @param collectionController collection controller
     *
     * @see Configuration.isCollectionType
     */
    fun addCollectionController(collectionController: CollectionController) {
        synchronized(collectionControllers) {
            typeCategoryCache.clear()
            collectionControllersCache.clear()
            collectionControllers.addFirst(collectionController)
            collectionClassMatchers += collectionController
        }
    }

    /**
     * Registers new [map controller][MapController]. It's used to teach translator to work with new
     * types of maps.
     *
     * @param mapController map controller
     *
     * @see Configuration.isMapType
     */
    fun addMapController(mapController: MapController) {
        synchronized(mapControllers) {
            typeCategoryCache.clear()
            mapControllersCache.clear()
            mapControllers.addFirst(mapController)
            mapClassMatchers += mapController
        }
    }

    /**
     * @param collectionClass class of collection
     *
     * @return collection controller of given [collectionClass]
     */
    internal fun getCollectionController(collectionClass: Class<*>): CollectionController {
        val res = collectionControllersCache[collectionClass]
        if (res != null) {
            return res
        }

        synchronized(collectionControllers) {
            return collectionControllersCache.getOrPut(collectionClass) {
                collectionControllers.stream()
                    .filter { c -> c.isMatched(collectionClass) }.findFirst().orElseThrow(
                        { ConfigurationException("Collection controller of class $collectionClass is not found") })

            }
        }
    }

    /**
     * @param mapClass class of map
     *
     * @return map controller of given [mapClass]
     */
    internal fun getMapController(mapClass: Class<*>): MapController {
        val res = mapControllersCache[mapClass]
        if (res != null) {
            return res
        }

        synchronized(mapControllers) {
            return mapControllersCache.getOrPut(mapClass) {
                mapControllers.stream()
                    .filter { c -> c.isMatched(mapClass) }.findFirst().orElseThrow(
                        { ConfigurationException("Map controller of class $mapClass is not found") })
            }
        }
    }

    /**
     * Adds custom [type converter][TypeConverter] which have priority over default ones:
     * [DatesConverter], [ToDoubleNumberConverter], [ToFloatNumberConverter], [ToLongNumberConverter],
     * [ToIntegerNumberConverter], [ToShortNumberConverter], [ToStringConverter], [EnumsConverter].
     *
     * @param typeConverter type converter to add
     */
    fun addCustomTypeConverter(typeConverter: TypeConverter) {
        synchronized(typeConvertersCache) {
            typeConvertersCache.clear()
            customTypeConverters += typeConverter
        }
    }

    /**
     * Adds custom [destination value creator][DestValueCreator] which have priority over default ones:
     * [ArrayDestValueCreator], [ConstructorWithoutArgsDestValueCreator], [DefaultImplDestValueCreator].
     *
     * @param destValueCreator destination value creator to add
     */
    fun addCustomDestValueCreator(destValueCreator: DestValueCreator) {
        synchronized(destValueCreatorCache) {
            destValueCreatorCache.clear()
            customDestValueCreators += destValueCreator
        }
    }

    /**
     * @param srcClass source class
     * @param destClass dest class
     *
     * @return type converter to convert from source to destination class, or *null* if no such converter found
     */
    fun getTypeConverter(srcClass: Class<*>, destClass: Class<*>): TypeConverter? {
        synchronized(typeConvertersCache) {
            return typeConvertersCache.getOrPutSupportedNulls(listOf(srcClass, destClass)) {
                Stream.concat(customTypeConverters.stream(), baseTypeConverters.stream())
                    .filter { tc -> tc.canConvert(srcClass, destClass) }.findFirst().orElse(null)
            }
        }
    }

    /**
     * @param objClass class of object we want to create
     *
     * @return destination value creator for given [objClass], or *null* if no such creator found
     */
    fun getDestValueCreator(objClass: Class<*>): DestValueCreator? {
        synchronized(destValueCreatorCache) {
            return destValueCreatorCache.getOrPutSupportedNulls(objClass) {
                Stream.concat(customDestValueCreators.stream(), baseDestValueCreators.stream())
                    .filter { oc -> oc.isCanCreate(objClass) }.findFirst().orElse(null)
            }
        }
    }

    /**
     * Usually, configuration attempts to validate every change. But sometimes it's not possible or not necessary to
     * do it immediately. This method is recommended to be called after configuration object is ready and before first
     * translation. If it's not called, then it's possible to get a runtime exception related to wrong configuration
     * during some translations.
     */
    fun validate() {
        synchronized(allTranslations) {
            for (t in allTranslations) {
                t.validate()
            }
        }
    }

    override fun createBean(beanType: Type): Bean {
        return Bean(beanType)
    }

    /**
     * Configuration for beans of type [beanType] which applies to all translations where this bean
     * participates. [beanType] can be either [java.lang.reflect.ParameterizedType] or [Class].
     * Can be accessed with [Configuration.beanOfClass]. If you want to specify special
     * configuration for particular translation, use [Configuration.Translation], which will override the current
     * configuration.
     *
     * **Bean** in the terminology of this library is the object of class whose properties will be mapped/translated
     * recursively to another bean. So beans can act as source or as destination.
     *
     * Requirements to source beans:
     * * Should have getters or fields. Standard getters with prefix 'get' and 'is' are
     * supported out of the box, but it's possible to configure to detect other types of getters, like fluent ones.
     * See [Configuration.PropertyDetectorConfig.addGetterDetector].
     *
     * Requirements to destination beans:
     * * Should have constructor without arguments. Otherwise you should register [DestValueCreator] for it.
     * See [Configuration.addCustomDestValueCreator].
     * * Should have setters or fields. Standard setters with prefix 'set' are supported out of the box,
     * but it's possible to detect other types of setters, like fluent ones.
     * See [Configuration.PropertyDetectorConfig.addSetterDetector].
     *
     * *Note 1*: during translation, public accessors will have priority over non-public and method accessors will
     * have priority over field accessors. Use [Configuration.Bean.Property.allowOnlyPublicAccessors] to allow only
     * public accessors for given property.
     *
     * *Note 2*: this configuration extends [Configuration.Bean] for [beanClass].superclass, if superclass is not
     * Object.class, and has priority over it.
     *
     * *Note 3*: beans can have identifier, to match same beans during translation of properties, collections, maps.
     * See [Configuration.Bean.Property.markAsIdentifier], [Configuration.Bean.setIdentifierProperty], [BeanFinder]
     *
     * @param beanClass class of bean, it should be not: Object.class, value type, collection type, map type.
     *
     * @throws ConversionException if [beanClass] is one of: Object.class, value type, collection type,
     * map type.
     */
    inner class Bean internal constructor(beanType: Type) :
        AbstractBeanIntrospector<Bean.Property, Bean>(beanType, this) {
        private val translations = HashMap<Bean, Translation>()

        private var identifierProperty: Bean.Property? = null
        private var beanFinder: BeanFinder<Any, Any>? = null

        private var dynamicMappedPropertiesCollection: Bean.Property? = null

        internal fun getDynamicMappedPropertiesCollection(): Bean.Property? {
            if (dynamicMappedPropertiesCollection == null && parentBeanIntrospector != null) {
                return parentBeanIntrospector!!.getDynamicMappedPropertiesCollection()
            }
            return dynamicMappedPropertiesCollection
        }

        /**
         * @param destBeanClass destination bean class
         *
         * @return configuration of translation from this bean to bean [destBeanClass]. It will be created,
         * if doesn't exist yet, and cached.
         *
         * @see [Configuration.Translation]
         */
        fun translationTo(destBeanClass: Class<*>): Translation {
            val destBean = beanOfClass(destBeanClass)
            synchronized(translations) {
                return getOrCreateTranslation(destBean)
            }
        }

        /**
         * @param destBeanClass destination bean class
         * @param destBeanType optional type representation of destination property.
         * Can be either [java.lang.reflect.ParameterizedType] or [Class] equal to [destBeanClass].
         *
         * @return configuration of translation from this bean to bean [destBeanClass]. It will be created,
         * if doesn't exist yet, and cached.
         *
         * @see [Configuration.Translation]
         */
        internal fun translationTo(destBeanClass: Class<*>, destBeanType: Type?): Translation {
            val destBean = beanOfClass(destBeanClass, destBeanType)
            synchronized(translations) {
                return getOrCreateTranslation(destBean)
            }
        }

        private fun getOrCreateTranslation(destBean: Bean): Translation {
            var translation = translations[destBean]
            if (translation == null) {
                val parentDestBean = destBean.parentBeanIntrospector
                val parentTranslation = if (parentDestBean != null) getOrCreateTranslation(parentDestBean) else null

                translation = Translation(this, destBean, parentTranslation)
                synchronized(allTranslations) {
                    allTranslations.add(translation)
                }
                translations[destBean] = translation
            }
            return translation
        }

        override fun createProperty(name: String, getters: List<Getter>, setters: List<Setter>): Property {
            return Property(name, getters, setters)
        }

        /**
         * @return identifier property of current bean, or *null* if no identifier specified
         *
         * @see [Configuration.Bean.Property.markAsIdentifier]
         * @see [Configuration.Bean.setIdentifierProperty]
         */
        fun getIdentifierProperty(): Bean.Property? {
            if (identifierProperty != null) {
                return identifierProperty
            }
            val parent = parentBeanIntrospector
            if (parent != null) {
                val parentIdentifier = parent.getIdentifierProperty()
                if (parentIdentifier != null) {
                    return property(parentIdentifier.name)
                }
            }
            return null
        }

        /**
         * Sets given property as identifier.
         *
         * It's common practice to set identifier property on most common superclass of entities, which contains
         * this property. So you do not need to repeat this configuration on every single bean class.
         *
         * For example, you have 3 classes: abstract class Pet, class Dog extends Pet, class Cat extends Pet.
         * Pet has property "id", which should be an identifier. Then use:
         *
         *    configuration.beanOfClass(Pet.class).setIdentifierProperty("id")
         *
         * and it will be automatically applied to Dog and Cat.
         *
         * @param name name of property in bean
         *
         * @throws NoSuchPropertyException if no such property exists in the bean
         *
         * @see BeanFinder
         */
        fun setIdentifierProperty(propertyName: String) {
            property(propertyName).markAsIdentifier()
        }

        /**
         * @return bean finder which finds instances of given bean by identifier, or *null*, if no bean finder specified
         *
         * @see BeanFinder
         * @see setBeanFinder
         */
        fun getBeanFinder(): BeanFinder<Any, Any>? {
            if (beanFinder != null) {
                return beanFinder
            }
            val parent = parentBeanIntrospector
            if (parent != null) {
                return parent.getBeanFinder()
            }
            return null
        }

        /**
         * Sets bean finder for given bean, which will find its instances by identifier. It's common practice
         * to set bean finder on most common superclass of your entities, which has identifier.
         *
         * @param beanFinder bean finder
         *
         * @see BeanFinder
         */
        fun setBeanFinder(beanFinder: BeanFinder<*, *>) {
            this.beanFinder = beanFinder as BeanFinder<Any, Any>
        }

        /**
         * Configuration of property of bean class. Can be accessed with [Configuration.Bean.property].
         *
         * @param name name of property
         * @param getters list of property getters
         * @param setters list of property setters
         */
        inner class Property(name: String, getters: List<Getter>, setters: List<Setter>) :
            AbstractProperty<Property, Bean>(name, getters, setters, this) {

            private var skippedAsSource: Boolean = false
            private val skippedChangeTypes = EnumSet.noneOf(ChangeType::class.java)
            private var defaultValue: Any? = null

            private val skipChangesLock = ReentrantReadWriteLock()

            /**
             * Marks given property as a collection that will contain names of properties which should be mapped
             * dynamically, when this bean acts as a source. The properties, which names are not listed in this
             * collection will be skipped during translation.
             *
             * Note that this property will be also skipped as source.
             *
             * If value of collection is `null`, then all properties of bean will be mapped.
             *
             * @throws ConfigurationException if given property is not a collection of strings or if
             * the dynamic collection property is already defined for this bean
             */
            fun markAsDynamicMappedPropertiesCollection() {
                if (dynamicMappedPropertiesCollection != null) {
                    throw ConfigurationException(
                        "Dynamic mapped properties collection already defined. " +
                                "And it's $dynamicMappedPropertiesCollection"
                    )
                }
                val accessorValueType = getPropertyDescription().propertyAccessorValueType
                if (accessorValueType.typeCategory != TypeCategory.Collection) {
                    throw ConfigurationException(
                        "Dynamic mapped properties collection property $this " +
                                "is not actually a collection"
                    )
                }
                if (accessorValueType.getTypeOfGenericParameter(0) != String::class.java) {
                    throw ConfigurationException(
                        "Dynamic mapped properties collection property $this " +
                                "should have string elements"
                    )
                }

                dynamicMappedPropertiesCollection = this
                skipAsSource()
                skipAllChanges()
            }

            /**
             * Marks given property as identifier for instances of current bean.
             *
             * It's common practice to set identifier property on most common superclass of entities, which contains
             * this property. So you do not need to repeat this configuration on every single bean class.
             *
             * For example, you have 3 classes: abstract class Pet, class Dog extends Pet, class Cat extends Pet.
             * Pet has property "id", which should be an identifier. Then use:
             *
             *    configuration.beanOfClass(Pet.class).property("id").markAsIdentifier()
             *
             * and it will be automatically applied to Dog and Cat.
             *
             * @see BeanFinder
             */
            fun markAsIdentifier() {
                identifierProperty = this
            }

            /**
             * @return is current property an identifier?
             */
            fun isIdentifier(): Boolean {
                val id = getIdentifierProperty()
                return id != null && (id == this || id.name == name)
            }

            /**
             * Mark this destination property as skipped, by skipping all [types of changes][ChangeType].
             */
            fun skipAllChanges() {
                skipChangesLock.write {
                    skippedChangeTypes.addAll(values())
                }
            }

            /**
             * Skips specified [changeType], if this property acts as destination.
             *
             * @param changeType change type to skip
             *
             * @see ChangeType
             */
            fun skipChange(changeType: ChangeType) {
                skipChangesLock.write {
                    skippedChangeTypes.add(changeType)
                }
            }

            /**
             * Skips all specified [changeTypes], if this property acts as destination.
             *
             * @param changeTypes change types to skip
             *
             * @see ChangeType
             */
            fun skipChanges(vararg changeTypes: ChangeType) {
                skipChangesLock.write {
                    skippedChangeTypes.addAll(changeTypes)
                }
            }

            /**
             * @param changeType change type
             *
             * @return is specified [changeType] skipped?
             */
            fun isSkippedChange(changeType: ChangeType): Boolean {
                skipChangesLock.read {
                    return skippedChangeTypes.contains(changeType) ||
                            (parentBeanProperty != null && parentBeanProperty!!.isSkippedChange(changeType))
                }
            }

            /**
             * @param changeTypes change types
             *
             * @return are all specified [changeTypes] skipped?
             */
            fun isSkippedChanges(vararg changeTypes: ChangeType): Boolean {
                for (changeType in changeTypes) {
                    if (!isSkippedChange(changeType)) {
                        return false
                    }
                }
                return true
            }

            /**
             * Skip the property to act as source property.
             */
            fun skipAsSource() {
                skippedAsSource = true
            }

            /**
             * @return Is this property skipped as source property?
             */
            fun isSkippedAsSource(): Boolean {
                return skippedAsSource || (parentBeanProperty != null && parentBeanProperty!!.isSkippedAsSource())
            }

            /**
             * Rebuilds configuration of given property to allow to use only public accessors during translation.
             */
            @Synchronized
            override fun allowOnlyPublicAccessors() {
                if (onlyPublic) {
                    return
                }

                val getterBefore = getter()
                val setterBefore = setter()
                onlyPublic = true
                updateGetterAndSetter()
                val getterChanged = getterBefore != getter()
                val setterChanged = setterBefore != setter()
                if (!getterChanged && !setterChanged) {
                    return
                }

                for (translation in allTranslations) {
                    if (getterChanged && translation.srcBean.beanClass == beanClass
                        && translation.isInitializedSrcProperty(name)
                    ) {
                        throw WrongConfigurationOrderException(
                            "Getter changed! allowOnlyPublicAccessors should be performed " +
                                    "before configuration of translations for this bean!"
                        )
                    }
                    if (setterChanged && translation.destBean.beanClass == beanClass
                        && translation.isInitializedDestProperty(name)
                    ) {
                        throw WrongConfigurationOrderException(
                            "Setter changed! allowOnlyPublicAccessors should be performed " +
                                    "before configuration of translations for this bean!"
                        )
                    }
                }
            }

            /**
             * Sets default value for current property. This default value can be used when this property acts both
             * as source or as destination.
             *
             * @param defaultValue default value of property
             *
             * @throws DefaultValueWrongClassException if class of [defaultValue] is not assignable to property getter
             * return value or to property setter parameter value.
             *
             * @see Configuration.Translation.TranslationProperty.setDefaultValue
             */
            fun setDefaultValue(defaultValue: Any) {
                val defaultValueClass = defaultValue::class.java
                val g = getter
                if (g != null) {
                    val getterActualClass = g.accessorValueType.actualClass
                    if (!getterActualClass.isAssignableFrom(defaultValueClass)) {
                        throw DefaultValueWrongClassException(
                            "Default value class $defaultValueClass should be" +
                                    "assignable to getter actual class $getterActualClass"
                        )
                    }
                }
                val s = setter
                if (s != null) {
                    val setterActualClass = s.accessorValueType.actualClass
                    if (!setterActualClass.isAssignableFrom(defaultValueClass)) {
                        throw DefaultValueWrongClassException(
                            "Default value class $defaultValueClass should be" +
                                    "assignable to setter actual class $setterActualClass"
                        )
                    }
                }

                this.defaultValue = defaultValue
            }

            /**
             * @return default value of this property when it acts as source, or *null*. Used during translation when
             * actual source property value is null. Will attempt to lookup in parent configuration. Has lower priority
             * then default value defined in [Configuration.Translation.SrcProperty].
             *
             * @see Configuration.Translation.SrcProperty.getDefaultValue
             */
            fun getSrcDefaultValue(): Any? {
                return getDefaultValue(getter())
            }

            /**
             * @return default value of this property when it acts as destination, or *null*. Used during translation
             * when actual destination property value is going to be *null*. Will attempt to lookup in parent
             * configuration. Has lower priority then default value defined in [Configuration.Translation.DestProperty].
             *
             * @see Configuration.Translation.DestProperty.getDefaultValue
             */
            fun getDestDefaultValue(): Any? {
                return getDefaultValue(setter())
            }

            internal fun getDefaultValue(a: Accessor?): Any? {
                if (a == null) {
                    return null
                }

                if (defaultValue != null) {
                    return defaultValue
                }

                if (parentBeanProperty != null) {
                    val parentDefaultValue = parentBeanProperty!!.getDefaultValue(a)
                    if (parentDefaultValue != null
                        && a.accessorValueType.actualClass.isAssignableFrom(parentDefaultValue::class.java)
                    ) {
                        return parentDefaultValue
                    }
                }

                return null
            }

            /**
             * @return bean class of [Configuration.Bean] for which this [Configuration.Bean.Property] belongs
             */
            fun getOwnerBeanClass(): Class<*> {
                return beanClass
            }

            override fun toString(): String {
                return "${beanClass.simpleName}.$name"
            }
        }
    }

    /**
     * Represents configuration of translation from source bean to destination bean. Use it if you need to specify
     * some custom rules of translation, related to given pair of source and dest bean classes.
     * Has priority over configurations [Configuration.Bean] of source and destination beans. However it is better to use
     * [Configuration.Bean] if the same configuration should be applied to all translations, where this bean
     * participates.
     *
     * @param srcBean source bean configuration
     * @param destBean destination bean configuration
     */
    inner class Translation internal constructor(
        val srcBean: Bean, val destBean: Bean,
        val parentTranslation: Translation?
    ) {
        private val srcPropertiesConfigs = HashMap<List<Bean.Property>, Translation.SrcProperty>()
        private val destPropertiesConfigs = HashMap<List<Bean.Property>, Translation.DestProperty>()

        private val srcPropertiesLock = ReentrantReadWriteLock()
        private val destPropertiesLock = ReentrantReadWriteLock()

        /**
         * @param propertyPath relative path to property starting from source bean with dot '.' delimiter
         * (like: employee.person.firstName)
         *
         * @return [configuration of source property][Configuration.Translation.SrcProperty] with given [propertyPath].
         * It will be created, if doesn't exist yet, and cached. So subsequent similar calls to this method will return
         * the same configuration.
         *
         * @throws NoSuchPropertyException if during traversing path any of its part is not valid property name.
         * @throws WrongPropertyCategoryException if any of parts of the path before the last one is not bean property
         */
        fun srcProperty(propertyPath: String): Translation.SrcProperty {
            val path = getPropertyPath(this.srcBean, propertyPath)
            srcPropertiesLock.write {
                return srcPropertiesConfigs.getOrPut(path) { SrcProperty(path) }
            }
        }

        /**
         * @param propertyPath relative path to property starting from destination bean with dot '.' delimiter
         * (like: employee.person.firstName)
         *
         * @return [configuration of destination property][Configuration.Translation.DestProperty] with given
         * [propertyPath].
         * It will be created, if doesn't exist yet, and cached. So subsequent similar calls to this method will return
         * the same configuration.
         *
         * @throws NoSuchPropertyException if during traversing path some of it's part is not valid property name.
         * @throws WrongPropertyCategoryException if any of parts of the path before the last one is not bean property
         */
        fun destProperty(propertyPath: String): Translation.DestProperty {
            val path = getPropertyPath(this.destBean, propertyPath)
            destPropertiesLock.write {
                return destPropertiesConfigs.getOrPut(path) { DestProperty(path) }
            }
        }

        private fun getPropertyPath(bean: Bean, propertyPath: String): List<Bean.Property> {
            val res = ArrayList<Bean.Property>()
            val pathDelimiter = "."
            val splittedPropertyPath = propertyPath.split(pathDelimiter)
            val length = splittedPropertyPath.size
            var currentBean = bean
            val currentPath = StringJoiner(pathDelimiter)
            for ((i, propertyName) in splittedPropertyPath.withIndex()) {
                currentPath.add(propertyName)
                val property = currentBean.property(propertyName)
                res.add(property)
                if (i < length - 1) {
                    val getter = property.getter() ?: throw NoGettersException(
                        "No getters for property [$property] (path: $currentPath)"
                    )
                    val valueType = getter.accessorValueType
                    if (valueType.typeCategory != TypeCategory.Bean) {
                        throw WrongPropertyCategoryException(
                            "Property [$property] (path: $currentPath) should be " +
                                    "of category property ${TypeCategory.Bean} but it's ${valueType.typeCategory}"
                        )
                    }
                    currentBean = beanOfClass(valueType.actualClass, valueType.valueType)
                }
            }
            return res
        }

        internal fun isInitializedSrcProperty(propertyPath: String): Boolean {
            val path = getPropertyPath(this.srcBean, propertyPath)
            srcPropertiesLock.read {
                return srcPropertiesConfigs.containsKey(path)
            }
        }

        internal fun isInitializedDestProperty(propertyPath: String): Boolean {
            val path = getPropertyPath(this.destBean, propertyPath)
            destPropertiesLock.read {
                return destPropertiesConfigs.containsKey(path)
            }
        }

        /**
         * @return list of configurations of all [source properties][SrcProperty]
         */
        fun getSrcProperties(): List<Translation.SrcProperty> {
            for (p in srcBean.getProperties()) {
                if (p.getter() != null) {
                    srcProperty(p.name)
                }
            }

            srcPropertiesLock.read {
                return srcPropertiesConfigs.values.toList()
            }
        }

        /**
         * @return list of configurations of all [destination properties][DestProperty]
         */
        fun getDestProperties(): List<Translation.DestProperty> {
            for (p in destBean.getProperties()) {
                if (p.setter() != null) {
                    destProperty(p.name)
                }
            }

            destPropertiesLock.read {
                return destPropertiesConfigs.values.toList()
            }
        }

        /**
         * Shortcut for `getDestProperties().forEach { p -> p.skipChange(SET_NULL)}`.
         * Added, because it's common use case when we want to implement *patch* functionality
         */
        fun mapOnlyNotNullProperties() {
            getDestProperties().forEach { p -> p.skipChange(SET_NULL) }
        }

        internal fun validate() {
            for (srcProperty in getSrcProperties()) {
                srcProperty.validateTranslation()
            }
            buildDestToSourceMapping(null, null)
        }

        private fun buildDestToSourceMapping(
            changedSrcProperty: Translation.SrcProperty?,
            newTargetDestProperty: Translation.DestProperty?,
            visitedTranslations: MutableSet<Translation> = HashSet()
        ):
                Map<List<Bean.Property>, List<Bean.Property>> {
            val res = HashMap<List<Bean.Property>, List<Bean.Property>>()
            for (srcProperty in getSrcProperties()) {
                val destProperty = if (srcProperty == changedSrcProperty) newTargetDestProperty else null
                for (entry in srcProperty.buildDestToSourceMapping(destProperty, visitedTranslations)) {
                    putPathMappingWithValidation(res, entry.key, entry.value)
                }
            }
            return res
        }

        private fun putPathMappingWithValidation(
            res: MutableMap<List<Bean.Property>, List<Bean.Property>>,
            destPath: List<Bean.Property>,
            srcPath: List<Bean.Property>
        ) {
            val currentSrcPath = res[destPath]
            if (currentSrcPath != null && currentSrcPath != srcPath) {
                throw DuplicatedDestPropertyMappingException(
                    "Impossible to map destination property $destPath from" +
                            " two paths, $currentSrcPath and $srcPath"
                )
            }

            res[destPath] = srcPath
        }

        /**
         * Abstract configuration of translation property accessed by relative [path] from [srcBean] or [destBean].
         *
         * @param path path to property, represented as list of [Bean.Property] configurations
         * @param accessor accessor of given translation property configuration
         */
        abstract inner class TranslationProperty<A : Accessor> internal constructor(
            val path: List<Bean.Property>,
            val accessor: A
        ) {
            /**
             * Last property configuration of path
             */
            val property: Bean.Property = path.last()

            /**
             * Property description of [property] with [accessor]
             */
            val propertyDescription = PropertyDescription(
                property.getOwnerBeanClass(), property.name,
                accessor.accessorValueType
            )

            private var defaultValue: Any? = null

            val pathString = getPropertyPathString(path)

            private fun getPropertyPathString(path: List<Bean.Property>): String {
                return path.joinToString(separator = ".") { it -> it.name }
            }

            /**
             * Sets default value for the current property. This default value can be used when this property acts both
             * as source or as destination. Has priority over default value defined in [Configuration.Bean.Property].
             *
             * @param defaultValue default value of property
             *
             * @throws DefaultValueWrongClassException if the class of [defaultValue] is not assignable to value type of
             * [accessor]
             *
             * @see Configuration.Bean.Property.setDefaultValue
             */
            fun setDefaultValue(defaultValue: Any) {
                val defaultValueClass = defaultValue::class.java
                val accessorActualClass = accessor.accessorValueType.actualClass
                if (!accessorActualClass.isAssignableFrom(defaultValueClass)) {
                    throw DefaultValueWrongClassException(
                        "Default value class $defaultValueClass should be" +
                                "assignable to getter actual class $accessorActualClass"
                    )
                }
                this.defaultValue = defaultValue
            }

            /**
             * @return default value of this translation property. If not set, then it will attempt to
             * lookup in [property] configuration. Can be *null*
             *
             * @see Configuration.Bean.Property.getDefaultValue
             */
            fun getDefaultValue(): Any? {
                return defaultValue ?: property.getDefaultValue(accessor)
            }
        }

        /**
         * Configuration of source property of [srcBean] accessed by relative [path] during translation to [destBean].
         * Has higher priority then [configuration][Bean.Property] of corresponding property.
         *
         * @param path path to property within [srcBean], represented as list of [Bean.Property] configurations
         */
        inner class SrcProperty(path: List<Bean.Property>) : Translation.TranslationProperty<Getter>(
            path,
            path.last().getter()
                ?: throw NoGettersException("Property ${path.last()} has no getters to act as source property")
        ) {
            private var targetDestProperty: Translation.DestProperty? = null
            private var inheritedTargetDestPropertyIsNull: Boolean = false

            var propertyConverter: PropertyConverter? = null
            internal var skippedAsSource: Boolean? = false

            /**
             * Specifies target destination property path in [destBean] to which current property should be translated.
             *
             * @param destPropertyPath relative path to property in [destBean] starting from source bean
             * with dot '.' delimiter (like: employee.person.firstName)
             */
            fun translatesTo(destPropertyPath: String): Translation.DestProperty {
                return translatesTo(destPropertyPath, this.propertyConverter)
            }

            /**
             * Specifies destination property path in [destBean] to which current property should be translated with
             * applying [propertyConverter].
             *
             * @param destPropertyPath relative path to property in [destBean] starting from source bean
             * with dot '.' delimiter (like: employee.person.firstName)
             * @param propertyConverter property converter, which should be applied during translations. Can be
             * used when special logic is needed to convert source value to destination value.
             *
             * @see [PropertyConverter]
             */
            @Synchronized
            fun translatesTo(
                destPropertyPath: String,
                propertyConverter: PropertyConverter?
            ): Translation.DestProperty {
                val target = destProperty(destPropertyPath)
                validateNotTranslatedToDifferentProperty(target)

                var clarifiedPropertyConverter = propertyConverter
                if (propertyConverter == null) {
                    clarifiedPropertyConverter = findInheritedPropertyConverter(destPropertyPath)
                }

                // Validate all except interfaces or abstracts classes, because actual classes will be dynamically
                // clarified during translation.
                val destPropAccessorValueType = target.property.getPropertyDescription().propertyAccessorValueType
                if (!(destPropAccessorValueType.typeCategory == TypeCategory.Bean
                            && TypeUtil.isInterfaceOrAbstractClass(destPropAccessorValueType.actualClass))
                ) {
                    validateCanTranslate(target, clarifiedPropertyConverter)
                }

                if (targetDestProperty != null) {
                    // already configured, nothing to do
                    return targetDestProperty!!
                }

                targetDestProperty = target
                this.propertyConverter = clarifiedPropertyConverter
                return target
            }

            private fun findInheritedPropertyConverter(destPropertyPath: String): PropertyConverter? {
                if (parentTranslation == null) {
                    return null
                }

                val srcProperty = parentTranslation.srcProperty(pathString)
                val parentPropertyConverter = srcProperty.propertyConverter
                    ?: return srcProperty.findInheritedPropertyConverter(destPropertyPath)

                val parentDestProperty = srcProperty.targetDestProperty
                if (parentDestProperty == null) {
                    if (pathString == destPropertyPath) {
                        return parentPropertyConverter
                    }
                } else if (parentDestProperty.pathString == destPropertyPath) {
                    return parentPropertyConverter
                }
                return null
            }

            private fun validateCanTranslate(
                destProperty: Translation.DestProperty,
                propertyConverter: PropertyConverter?
            ) {
                val srcTypeInfo = this.accessor.accessorValueType
                val destTypeInfo = destProperty.accessor.accessorValueType

                val srcActualClass = srcTypeInfo.actualClass
                val destActualClass = destTypeInfo.actualClass

                if (propertyConverter == null) {
                    checkCanConvert(
                        srcActualClass, srcTypeInfo, destProperty, destActualClass, destTypeInfo,
                        "value type", listOf(SET_NOT_NULL)
                    )
                }

                buildDestToSourceMapping(this, destProperty)
            }

            private fun validateNotTranslatedToDifferentProperty(destProperty: DestProperty) {
                if (targetDestProperty != null && targetDestProperty != destProperty) {
                    throw TranslationAlreadyConfiguredException(
                        "Translation target for '$this' already configured as different property"
                    )
                }
            }

            private fun checkCanConvert(
                srcValueType: Type, srcAccessorValueType: AccessorValueType?,
                destProperty: Translation.DestProperty,
                destValueType: Type, destAccessorValueType: AccessorValueType?,
                componentTypeName: String,
                changeTypesToCheck: Collection<ChangeType>, checkAsValueType: Boolean = false
            ) {
                val srcValueClass = ReflectionUtil.getRawClass(srcValueType)
                val destValueClass = ReflectionUtil.getRawClass(destValueType)

                if (getTypeConverter(srcValueClass, destValueClass) != null) {
                    return
                }

                val srcTypeCategory = srcAccessorValueType?.typeCategory ?: getTypeCategory(srcValueClass)
                val destTypeCategory = destAccessorValueType?.typeCategory ?: getTypeCategory(destValueClass)

                if ((srcTypeCategory == TypeCategory.Value && destTypeCategory == TypeCategory.Value)
                    || checkAsValueType
                ) {
                    if (!destValueClass.isAssignableFrom(srcValueClass)) {
                        if (destProperty.isSkippedChanges(changeTypesToCheck)) {
                            return
                        }

                        throw ImpossibleToConvertException(
                            "For property mapping $property -> ${destProperty.property}: " +
                                    "impossible to convert $componentTypeName " +
                                    "from $srcValueClass to $destValueClass"
                        )
                    }
                } else if (srcTypeCategory == TypeCategory.Bean && destTypeCategory == TypeCategory.Bean) {
                    // no checks required
                } else if (srcTypeCategory == TypeCategory.Collection && destTypeCategory == TypeCategory.Collection) {
                    val srcCollectionController = getCollectionController(srcValueClass)
                    val destCollectionController = getCollectionController(destValueClass)

                    val srcComponentType = srcCollectionController.findCollectionComponentTypeByAccessor(
                        srcValueClass,
                        srcAccessorValueType
                    )

                    val destComponentType = destCollectionController.findCollectionComponentTypeByAccessor(
                        destValueClass,
                        destAccessorValueType
                    )

                    if (srcComponentType != null && destComponentType != null) {
                        checkCanConvert(
                            srcComponentType, null, destProperty,
                            destComponentType, null, "component type",
                            listOf(COLLECTION_ADD, COLLECTION_REMOVE)
                        )
                    }
                } else if (srcTypeCategory == TypeCategory.Map && destTypeCategory == TypeCategory.Map) {
                    val srcMapController = getMapController(srcValueClass)
                    val destMapController = getMapController(destValueClass)

                    val srcMapValueType =
                        srcMapController.findMapValueTypeByAccessor(srcValueClass, srcAccessorValueType)
                    val destMapValueType =
                        destMapController.findMapValueTypeByAccessor(destValueClass, destAccessorValueType)

                    if (srcMapValueType != null && destMapValueType != null) {
                        checkCanConvert(
                            srcMapValueType, null, destProperty,
                            destMapValueType, null, "map value type",
                            listOf(MAP_ADD, MAP_REMOVE)
                        )
                    }

                    val srcKeyType = srcMapController.findMapKeyTypeByAccessor(srcValueClass, srcAccessorValueType)
                    val destKeyType = destMapController.findMapKeyTypeByAccessor(destValueClass, destAccessorValueType)

                    if (srcKeyType != null && destKeyType != null) {
                        checkCanConvert(
                            srcKeyType, null, destProperty,
                            destKeyType, null, "map key type",
                            listOf(MAP_ADD, MAP_REMOVE, MAP_REPLACE_VALUE), true
                        )
                    }
                } else {
                    throw ImpossibleToConvertException(
                        "For property mapping $property -> ${destProperty.property}: " +
                                "can't convert from value of category $srcTypeCategory " +
                                "to value of category $destTypeCategory"
                    )
                }
            }

            /**
             * @return target destination property specified by [translatesTo], or *null*
             */
            fun getTargetDestProperty(): Translation.DestProperty? {
                return targetDestProperty
            }

            internal fun translatesToInheritedOrDefaultTargetDestProperty(): Translation.DestProperty? {
                val inheritedProperty = findInheritedTargetProperty()
                if (inheritedProperty?.setter() != null) {
                    return translatesTo(inheritedProperty.name)
                }

                val defaultProperty = findDefaultTargetProperty()
                if (defaultProperty?.setter() != null) {
                    return translatesTo(defaultProperty.name)
                }
                return targetDestProperty
            }

            internal fun findInheritedTargetDestProperty(): Translation.DestProperty? {
                var res: Translation.DestProperty? = null
                if (parentTranslation != null && !inheritedTargetDestPropertyIsNull) {
                    val srcProperty = parentTranslation.srcProperty(pathString)
                    res = srcProperty.targetDestProperty
                        ?: srcProperty.translatesToInheritedOrDefaultTargetDestProperty()
                }

                if (res == null) {
                    inheritedTargetDestPropertyIsNull = true
                }

                return res
            }

            private fun findInheritedTargetProperty(): Bean.Property? {
                val parentTargetDestProperty = findInheritedTargetDestProperty()
                return parentTargetDestProperty?.property
            }

            private fun findDefaultTargetProperty(): Bean.Property? {
                return destBean.getProperties().stream()
                    .filter { p -> p.name == property.name }.findFirst().orElse(null)
            }

            internal fun validateTranslation() {
                if (targetDestProperty == null) { // if not null, then already validated
                    val property = findDefaultTargetProperty()
                    if (property != null) {
                        validateCanTranslate(destProperty(property.name), propertyConverter)
                    }
                }
            }

            internal fun buildDestToSourceMapping(
                newDestProperty: Translation.DestProperty?,
                visitedTranslations: MutableSet<Translation> = HashSet()
            ):
                    Map<List<Bean.Property>, List<Bean.Property>> {
                val res = HashMap<List<Bean.Property>, List<Bean.Property>>()

                if (isSkippedAsSource()) {
                    return res
                }

                var target = newDestProperty ?: targetDestProperty
                if (target == null) {
                    val property = findDefaultTargetProperty()
                    if (property?.setter() != null) {
                        target = destProperty(property.name)
                    }
                }

                if (target != null) {
                    val srcCategory = accessor.accessorValueType.typeCategory
                    val destCategory = target.accessor.accessorValueType.typeCategory
                    if (srcCategory == TypeCategory.Bean && destCategory == TypeCategory.Bean
                        && !target.isSkippedChange(BEAN_MAP)
                    ) {
                        val t = beanOfClass(
                            accessor.accessorValueType.actualClass,
                            accessor.accessorValueType.valueType
                        ).translationTo(
                            target.accessor.accessorValueType.actualClass
                        )
                        if (!visitedTranslations.contains(t)) {
                            visitedTranslations += t
                            for (entry in t.buildDestToSourceMapping(
                                null, null,
                                visitedTranslations
                            ).entries) {
                                val destPath = ArrayList(target.path)
                                val srcPath = ArrayList(path)

                                destPath.addAll(entry.key)
                                srcPath.addAll(entry.value)

                                putPathMappingWithValidation(res, destPath, srcPath)
                            }
                        }
                    }
                    var putTerminalMapping = false
                    if (srcCategory == TypeCategory.Value && destCategory == TypeCategory.Value
                        && !target.isSkippedChanges(SET_NULL, SET_NOT_NULL)
                    ) {
                        putTerminalMapping = true
                    }
                    if (srcCategory == TypeCategory.Collection && destCategory == TypeCategory.Collection
                        && !target.isSkippedChanges(COLLECTION_ADD, COLLECTION_REMOVE)
                    ) {
                        putTerminalMapping = true
                    }
                    if (putTerminalMapping) {
                        val destPath = ArrayList(target.path)
                        val srcPath = ArrayList(path)

                        putPathMappingWithValidation(res, destPath, srcPath)
                    }
                }

                return res
            }

            /**
             * Skip this property to act as source property.
             */
            fun skipAsSource() {
                skippedAsSource = true
            }

            /**
             * @return is this property skipped to act as source property?
             */
            fun isSkippedAsSource(): Boolean {
                return skippedAsSource == true || findInheritedSkippedAsSource() == true || property.isSkippedAsSource()
            }

            private fun findInheritedSkippedAsSource(): Boolean? {
                if (parentTranslation == null) {
                    return null
                }

                val srcProperty = parentTranslation.srcProperty(pathString)
                val parentSkippedAsSource = srcProperty.skippedAsSource
                if (parentSkippedAsSource != null) {
                    return parentSkippedAsSource
                }
                return srcProperty.findInheritedSkippedAsSource()
            }

            override fun toString(): String {
                return "src property $path"
            }

        }

        /**
         * Configuration of destination property of [destBean] accessed by relative [path] during translation
         * from [srcBean]. Has higher priority then [configuration][Bean.Property] of corresponding property.
         *
         * @param path path to property within [destBean], represented as list of [Bean.Property] configurations
         */
        inner class DestProperty(path: List<Bean.Property>) :
            Translation.TranslationProperty<Setter>(
                path,
                path.last().setter()
                    ?: throw NoSettersException("Property ${path.last()} has no setters to act as destination property")
            ) {

            private val skippedChangeTypes = EnumSet.noneOf(ChangeType::class.java)
            private val skipChangesLock = ReentrantReadWriteLock()

            private var inheritedSkippedChangeTypes: EnumSet<ChangeType>? = null
            private val inheritedSkipChangesLock = ReentrantReadWriteLock()

            private var skippedChangesCached: Set<ChangeType>? = null

            /**
             * Mark this destination property as skipped, by skipping all [types of changes][ChangeType].
             */
            fun skipAllChanges() {
                skipChangesLock.write {
                    skippedChangeTypes.addAll(values())
                }
            }

            /**
             * Skips specified [changeType] of this destination property.
             *
             * @param changeType change type to skip
             *
             * @see ChangeType
             */
            fun skipChange(changeType: ChangeType) {
                skipChangesLock.write {
                    skippedChangeTypes.add(changeType)
                }
            }

            /**
             * Skips all specified [changeTypes] of this destination property.
             *
             * @param changeTypes change types to skip
             *
             * @see ChangeType
             */
            fun skipChanges(vararg changeTypes: ChangeType) {
                skipChangesLock.write {
                    skippedChangeTypes.addAll(changeTypes)
                }
            }

            /**
             * @param changeType change type
             *
             * @return is specified [changeType] skipped?
             */
            fun isSkippedChange(changeType: ChangeType): Boolean {
                skipChangesLock.read {
                    if (skippedChangeTypes.contains(changeType)) {
                        return true
                    }
                }

                if (isSkippedInInheritedProperty(changeType)) {
                    return true
                }

                return property.isSkippedChange(changeType)
            }

            private fun isSkippedInInheritedProperty(changeType: ChangeType): Boolean {
                tryLazyInitInheritedSkippedChangeTypes()
                val localInheritedSkippedChangeTypes = inheritedSkippedChangeTypes
                if (localInheritedSkippedChangeTypes != null) {
                    inheritedSkipChangesLock.read {
                        if (localInheritedSkippedChangeTypes.contains(changeType)) {
                            return true
                        }
                    }
                }
                return false
            }

            private fun tryLazyInitInheritedSkippedChangeTypes() {
                if (inheritedSkippedChangeTypes == null) {
                    if (findCorrespondingSrcProperty() == null) {
                        return
                    }

                    inheritedSkipChangesLock.write {
                        val skippedChanges = EnumSet.noneOf(ChangeType::class.java)
                        for (ch in values()) {
                            if (findInheritedSkippedChange(ch) == true) {
                                skippedChanges.add(ch)
                            }
                        }
                        inheritedSkippedChangeTypes = skippedChanges
                    }
                }
            }

            private fun findInheritedSkippedChange(changeType: ChangeType): Boolean? {
                val srcProperty = findCorrespondingSrcProperty() ?: return null
                val inheritedTargetDestProperty = srcProperty.findInheritedTargetDestProperty()
                if (inheritedTargetDestProperty == null || inheritedTargetDestProperty.pathString != pathString) {
                    return null
                }

                return inheritedTargetDestProperty.isSkippedChange(changeType)
            }

            private fun findCorrespondingSrcProperty(): SrcProperty? {
                return getSrcProperties().firstOrNull { it.getTargetDestProperty() == this }
            }

            /**
             * @param changeTypes change types
             *
             * @return are all specified [changeTypes] skipped?
             */
            fun isSkippedChanges(vararg changeTypes: ChangeType): Boolean {
                for (changeType in changeTypes) {
                    if (!isSkippedChange(changeType)) {
                        return false
                    }
                }
                return true
            }

            /**
             * @param changeTypes change types
             *
             * @return are all specified [changeTypes] skipped?
             */
            fun isSkippedChanges(changeTypes: Collection<ChangeType>): Boolean {
                for (changeType in changeTypes) {
                    if (!isSkippedChange(changeType)) {
                        return false
                    }
                }
                return true
            }

            private fun calcSkippedChanges(): Set<ChangeType> {
                val res = EnumSet.noneOf(ChangeType::class.java)
                for (changeType in values()) {
                    if (isSkippedChange(changeType)) {
                        res += changeType
                    }
                }

                return res
            }

            /**
             * @return list of all skipped changes for this destination property, it's cached on first call
             */
            internal fun getSkippedChangesCached(): Set<ChangeType> {
                if (skippedChangesCached == null) {
                    skippedChangesCached = calcSkippedChanges()
                }

                return skippedChangesCached!!
            }

            override fun toString(): String {
                return "dest property $path"
            }
        }
    }

    /**
     * Change types which can be skipped in configuration.
     * @see Configuration.Bean.Property.skipChange
     * @see Configuration.Bean.Property.skipChanges
     * @see Configuration.Translation.DestProperty.skipChange
     * @see Configuration.Translation.DestProperty.skipChanges
     */
    enum class ChangeType {

        /**
         * Setting null value, if previous value was not null
         */
        SET_NULL,

        /**
         * Setting not null value
         */
        SET_NOT_NULL,

        /**
         * Mapping properties of bean
         */
        BEAN_MAP,

        /**
         * Adding element to collection
         */
        COLLECTION_ADD,

        /**
         * Removing element from collection
         */
        COLLECTION_REMOVE,

        /**
         * Adding key-value to map
         */
        MAP_ADD,

        /**
         * Removing key-value from map
         */
        MAP_REMOVE,

        /**
         * Replacing value by key in map
         */
        MAP_REPLACE_VALUE,
    }
}
