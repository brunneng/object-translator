package org.bitbucket.brunneng.ot.beans

/**
 * Represents strategy of finding beans by its identifier to match same beans during translation of properties,
 * collections and maps. For example, it can be used to find destination entity in a db, so after mapping it will be:
 * saved back to the db automatically, using a framework such as Hibernate.
 *
 * Bean finder can be set into bean configuration.
 *
 * @see org.bitbucket.brunneng.ot.Configuration.Bean.setBeanFinder
 */
interface BeanFinder<T, I> {
    /**
     * @param beanClass class of the bean
     * @param identifier identifier of the bean
     * @return found bean with given identifier or null if it's not found
     */
    fun findBean(beanClass: Class<*>, identifier: I): T?
}