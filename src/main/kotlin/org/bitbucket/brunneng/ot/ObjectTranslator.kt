package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.introspection.property.TypeCategory
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import org.bitbucket.brunneng.ot.Configuration.Bean.Property
import org.bitbucket.brunneng.ot.Configuration.ChangeType
import org.bitbucket.brunneng.ot.Configuration.ChangeType.*
import org.bitbucket.brunneng.ot.Configuration.Translation.DestProperty
import org.bitbucket.brunneng.ot.Configuration.Translation.SrcProperty
import org.bitbucket.brunneng.ot.creators.ValueCreationContext
import org.bitbucket.brunneng.ot.exceptions.*
import org.bitbucket.brunneng.ot.log.*
import org.bitbucket.brunneng.ot.util.ObjectsUtil
import org.bitbucket.brunneng.ot.util.TypeReference
import org.bitbucket.brunneng.ot.util.TypeUtil
import java.lang.reflect.Modifier
import java.lang.reflect.Type
import java.util.*

/**
 * Object translator which translates/maps source object into new or existing object according to [Configuration].
 *
 * Methods:
 * * [translate]
 * * [translateWithLog]
 * * [mapBean]
 * * [mapBeanWithLog]
 *
 * @param configuration configuration of translation
 */
class ObjectTranslator(private val configuration: Configuration) {

    private data class MappedBeansCacheKey(val srcBean: Any, val destBeanClass: Class<*>) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as MappedBeansCacheKey

            if (srcBean !== other.srcBean) return false
            if (destBeanClass != other.destBeanClass) return false

            return true
        }

        override fun hashCode(): Int {
            var result = srcBean.hashCode()
            result = 31 * result + destBeanClass.hashCode()
            return result
        }
    }

    private class TranslationContext(var currentLogEntry: TreeLogEntry?) {
        val mappedBeansCache: MutableMap<MappedBeansCacheKey, Any> = HashMap()

        val setPropertiesMap = LinkedHashMap<BeanWithProperty, Any?>()

        fun withCurrentLogEntry(newLogEntry: TreeLogEntry?): TranslationContext {
            currentLogEntry = newLogEntry
            return this
        }

        fun isPropertyAlreadySet(bean: Any, property: Property): Boolean {
            return setPropertiesMap.contains(BeanWithProperty(bean, property))
        }

        fun addSetDestProperty(beanWithProperty: BeanWithProperty, destValue: Any?) {
            if (setPropertiesMap.contains(beanWithProperty)) {
                val previousValue = setPropertiesMap.get(beanWithProperty)
                throw MultiplePropertySettingException(
                    "Setting into $beanWithProperty twice! " +
                            "First value was [$previousValue], and second value is [$destValue]"
                )
            }

            setPropertiesMap.put(beanWithProperty, destValue)
        }
    }

    private class BeanWithProperty(val bean: Any, val property: Property) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as BeanWithProperty

            if (bean !== other.bean) return false
            if (property != other.property) return false

            return true
        }

        override fun hashCode(): Int {
            var result = bean.hashCode()
            result = 31 * result + property.hashCode()
            return result
        }

        override fun toString(): String {
            return "property $property of bean $bean"
        }


    }

    /**
     * Creates object translator with default configuration.
     */
    constructor() : this(Configuration())

    /**
     * Translate [srcObject] to new object of class [destObjectClass]
     *
     * @param srcObject source object
     * @param destObjectClass destination object class
     *
     * @return translated object
     */
    fun <T> translate(srcObject: Any, destObjectClass: Class<T>): T? {
        return translate(srcObject, destObjectClass as Type)
    }

    /**
     * Translate [srcObject] to new object of type referenced by [typeReference]
     *
     * @param srcObject source object
     * @param typeReference reference to type with some generic component
     *
     * @return translated object
     * @see [TypeReference]
     */
    fun <T> translate(srcObject: Any, typeReference: TypeReference<T>): T? {
        return translate(srcObject, typeReference.getGenericParameterType())
    }

    /**
     * Translate [srcObject] to new object of type [destObjectType]
     *
     * @param srcObject source object
     * @param destObjectType destination object type
     *
     * @return translated object
     */
    fun <T> translate(srcObject: Any, destObjectType: Type): T? {
        return translateInternal(
            srcObject, srcObject::class.java, destObjectType, null,
            null, null, EnumSet.noneOf(ChangeType::class.java),
            TranslationContext(null)
        )
    }

    /**
     * Translate [srcObject] to new object of class [destObjectClass] with producing [result with log][ResultWithLog]
     *
     * @param srcObject source object
     * @param destObjectClass destination object class
     *
     * @return translated object with log
     */
    fun <T> translateWithLog(srcObject: Any, destObjectClass: Class<T>): ResultWithLog<T?> {
        val logEntry = TranslationLogEntry(srcObject, destObjectClass)
        val destValue = translateInternal<T>(
            srcObject, srcObject::class.java, destObjectClass,
            null, null, null,
            EnumSet.noneOf(ChangeType::class.java), TranslationContext(logEntry)
        )
        return ResultWithLog(destValue, logEntry)
    }

    private fun <T> translateInternal(
        srcObject: Any, srcObjectType: Type, destObjectType: Type,
        srcAccessorValueType: AccessorValueType?,
        destAccessorValueType: AccessorValueType?, prevDestValue: Any?,
        skippedChanges: Set<ChangeType>,
        translationContext: TranslationContext
    ): T? {

        val log = translationContext.currentLogEntry
        val srcObjectClass = srcObject::class.java

        val destObjectClass = ReflectionUtil.getRawClass(destObjectType)
        val valueCreationContext = ValueCreationContext(srcObject, prevDestValue)
        val clarifiedDestValueClass = clarifyDestValueClass(destObjectClass, valueCreationContext)
        var clarifiedDestValueType = destObjectType
        if (destObjectClass != clarifiedDestValueClass) {
            log?.add(ClarifiedDestValueClassLogEntry(destObjectClass, clarifiedDestValueClass))
            clarifiedDestValueType = clarifiedDestValueClass
        }

        val srcTypeCategory = srcAccessorValueType?.typeCategory ?: configuration.getTypeCategory(srcObjectClass)
        val destTypeCategory =
            destAccessorValueType?.typeCategory ?: configuration.getTypeCategory(clarifiedDestValueClass)

        if (destTypeCategory == TypeCategory.Undefined) {
            throw DestValueCategoryUndefinedException("Can't detect type category of dest class $destObjectClass")
        }

        val converter = if (srcObjectClass != destObjectClass)
            configuration.getTypeConverter(srcObjectClass, destObjectClass) else null
        if (converter != null) {

            val res = converter.convert(srcObject, destObjectClass)

            log?.add(TypeConverterUsedLogEntry(converter, res))
            log?.add(ValueReturnedLogEntry(res))

            return res as T
        }

        var destObject = prevDestValue

        if (srcTypeCategory == TypeCategory.Bean && destTypeCategory == TypeCategory.Bean) {

            val srcIdentifierProperty = configuration.beanOfClass(
                srcObjectClass,
                srcAccessorValueType?.valueType
            ).getIdentifierProperty()

            var clarifiedDestAccessorValueType = destAccessorValueType?.valueType
            if (TypeUtil.isInterfaceOrAbstractClass(clarifiedDestAccessorValueType)) {
                clarifiedDestAccessorValueType = clarifiedDestValueType
            }

            val destBeanConfig = configuration.beanOfClass(
                clarifiedDestValueClass,
                clarifiedDestAccessorValueType
            )
            val destIdentifierProperty = destBeanConfig.getIdentifierProperty()
            val beanFinder = destBeanConfig.getBeanFinder()

            if (srcIdentifierProperty != null && destIdentifierProperty != null && beanFinder != null) {
                val id = getValueIdentifier(
                    srcObject, srcIdentifierProperty,
                    destIdentifierProperty.getter()?.accessorValueType?.actualClass
                )
                if (id != null) {
                    destObject = beanFinder.findBean(clarifiedDestValueClass, id)

                    log?.add(BeanFinderResultLogEntry(id, clarifiedDestValueClass, destObject))
                }
            }

            if (destObject == null) {
                destObject = createDestValueOfClass(destObjectClass, valueCreationContext)
            }

            if (!skippedChanges.contains(BEAN_MAP)) {
                mapBeanInternal(
                    srcObject, srcAccessorValueType?.valueType ?: srcObjectType,
                    destObject, clarifiedDestAccessorValueType ?: clarifiedDestValueType,
                    translationContext.withCurrentLogEntry(log?.add(MapBeanLogEntry(srcObject, destObject)))
                )
            }
        } else if (srcTypeCategory == TypeCategory.Value && destTypeCategory == TypeCategory.Value) {
            if (destObjectClass.isAssignableFrom(srcObjectClass)) {
                destObject = srcObject
            } else {
                throw TranslationException(
                    "Can't convert from value class $srcObjectClass to $clarifiedDestValueClass. " +
                            "No corresponding converter exist"
                )
            }
        } else if (srcTypeCategory == TypeCategory.Collection && destTypeCategory == TypeCategory.Collection) {
            if (destObject == null) {
                destObject = createDestValueOfClass(destObjectClass, valueCreationContext)
            }
            destObject = mapCollectionInternal(
                srcObject, destObject, srcAccessorValueType, destAccessorValueType,
                clarifiedDestValueType, skippedChanges,
                translationContext.withCurrentLogEntry(log?.add(MapCollectionLogEntry(srcObject, destObject)))
            )
        } else if (srcTypeCategory == TypeCategory.Map && destTypeCategory == TypeCategory.Map) {
            if (destObject == null) {
                destObject = createDestValueOfClass(destObjectClass, valueCreationContext)
            }
            destObject = mapMapsInternal(
                srcObject, destObject, srcAccessorValueType, destAccessorValueType,
                clarifiedDestValueType, skippedChanges,
                translationContext.withCurrentLogEntry(log?.add(MapMapsLogEntry(srcObject, destObject)))
            )
        } else {
            throw TranslationException(
                "Can't translate value of category $srcTypeCategory into value of category $destTypeCategory"
            )
        }

        log?.add(ValueReturnedLogEntry(destObject))
        return destObject as T
    }

    protected fun createDestValueOfClass(destValueClass: Class<*>, valueCreationContext: ValueCreationContext): Any {
        val clarifiedDestValueClass = clarifyDestValueClass(destValueClass, valueCreationContext)
        val objectCreator = configuration.getDestValueCreator(clarifiedDestValueClass)
            ?: throw DestValueCreationException(
                "Dest value creator for objects of class " +
                        "${clarifiedDestValueClass.name} is not found"
            )
        try {
            return objectCreator.create(clarifiedDestValueClass, valueCreationContext)
        } catch (e: Exception) {
            throw DestValueCreationException("Can't create dest value of class ${clarifiedDestValueClass.name}", e)
        }
    }

    private fun clarifyDestValueClass(destValueClass: Class<*>, valueCreationContext: ValueCreationContext): Class<*> {
        val srcValue = valueCreationContext.srcValue
        val prevDestValue = valueCreationContext.prevDestValue
        if (prevDestValue != null && destValueClass.isAssignableFrom(prevDestValue::class.java)) {
            return prevDestValue::class.java
        }
        if ((destValueClass.isInterface || Modifier.isAbstract(destValueClass.modifiers))
            && !MutableCollection::class.java.isAssignableFrom(destValueClass)
            && !MutableMap::class.java.isAssignableFrom(destValueClass)
            && destValueClass.isAssignableFrom(srcValue::class.java)
        ) {
            return srcValue::class.java
        }
        return destValueClass
    }


    /**
     * Maps source bean to destination bean, with translation of corresponding properties recursively.
     *
     * What is considered as *bean* described in documentation to [Configuration.Bean].
     *
     * @param srcBean source bean
     * @param destBean destination bean
     */
    fun mapBean(srcBean: Any, destBean: Any) {
        mapBeanInternal(srcBean, null, destBean, null, TranslationContext(null))
    }

    /**
     * Maps source bean to destination bean, with translation of corresponding properties recursively, and producing
     * tree log of translation process.
     *
     * What is considered as *bean* described in documentation to [Configuration.Bean].
     *
     * @param srcBean source bean
     * @param destBean destination bean
     *
     * @return [tree log][TreeLogEntry] of translation process
     */
    fun mapBeanWithLog(srcBean: Any, destBean: Any): MapBeanLogEntry {
        val logEntry = MapBeanLogEntry(srcBean, destBean)
        mapBeanInternal(srcBean, null, destBean, null, TranslationContext(logEntry))
        return logEntry
    }

    private fun mapBeanInternal(
        srcBean: Any, srcGenericType: Type?,
        destBean: Any, destGenericType: Type?, translationContext: TranslationContext
    ) {
        val log = translationContext.currentLogEntry

        val translation = configuration.beanOfClass(srcBean.javaClass, srcGenericType)
            .translationTo(destBean.javaClass, destGenericType)
        translationContext.mappedBeansCache[MappedBeansCacheKey(srcBean, destBean::class.java)] = destBean

        val destToSrcPropertiesMapping = HashMap<DestProperty, MutableList<SrcProperty>>()
        val sortedDestProperties = ArrayList<DestProperty>()

        val dynamicMappedProperties = getDynamicMappedProperties(translation, srcBean)
        for (srcProperty in translation.getSrcProperties()) {
            if (srcProperty.isSkippedAsSource()) {
                log?.add(SourcePropertySkippedLogEntry(srcProperty.propertyDescription, false))
                continue
            }

            if (dynamicMappedProperties != null && !dynamicMappedProperties.contains(srcProperty.property.name)) {
                log?.add(SourcePropertySkippedLogEntry(srcProperty.propertyDescription, true))
                continue
            }

            val destProperty = srcProperty.getTargetDestProperty()
                ?: srcProperty.translatesToInheritedOrDefaultTargetDestProperty()
            if (destProperty != null) {
                if (destProperty.getSkippedChangesCached().size == ChangeType.values().size) {
                    log?.add(PropertyChangeSkippedLogEntry(destProperty.propertyDescription, *ChangeType.values()))
                    continue
                }
            } else {
                log?.add(DestPropertyNotFoundLogEntry(srcProperty.propertyDescription))
                continue
            }
            sortedDestProperties.add(destProperty)
            val srcProperties = destToSrcPropertiesMapping.getOrPut(destProperty) { ArrayList() }

            srcProperties.add(srcProperty)
        }

        // mapping with increasing of dest path depth and ids first
        sortedDestProperties.sortBy {
            val path = it.path
            var orderNumber = path.size
            if (isLongPathEndedWithIdentifier(path)) {
                orderNumber -= 1000
            }
            return@sortBy orderNumber
        }

        for (destProperty in sortedDestProperties) {
            val srcProperties = destToSrcPropertiesMapping[destProperty] ?: continue

            for (srcProperty in srcProperties) {
                val destPropertyDescription = destProperty.propertyDescription

                val srcBeanWithProperty = getLastBeanWithProperty(
                    srcBean, srcProperty.path, false, null,
                    translationContext
                )
                var srcValue: Any? = null
                if (srcBeanWithProperty != null) {
                    srcValue = srcProperty.accessor.get(srcBeanWithProperty.bean)
                }

                if (srcValue == null) {
                    srcValue = srcProperty.getDefaultValue()
                }

                try {
                    mapToDestProperty(
                        destProperty, destBean, srcValue, srcProperty,
                        translationContext.withCurrentLogEntry(
                            log?.add(
                                MapPropertyLogEntry(srcProperty.propertyDescription, destPropertyDescription)
                            )
                        )
                    )
                } catch (e: Exception) {
                    throw PropertyMappingException("Error while mapping $srcProperty to $destProperty", e)
                }
            }
        }
    }

    private fun getDynamicMappedProperties(translation: Configuration.Translation, srcBean: Any): Collection<String>? {
        val collectionOfSourceProperties = translation.srcBean.getDynamicMappedPropertiesCollection()
        if (collectionOfSourceProperties != null) {
            val getter = collectionOfSourceProperties.getter()
            if (getter != null) {
                return getter.get(srcBean) as Collection<String>?
            }
        }
        return null
    }

    private fun mapToDestProperty(
        destProperty: DestProperty, destBean: Any,
        srcValue: Any?, srcProperty: SrcProperty,
        translationContext: TranslationContext
    ) {
        val log = translationContext.currentLogEntry

        val destPropertyDescription = destProperty.propertyDescription

        if (isLongPathEndedWithIdentifier(destProperty.path) && srcValue == null) {
            log?.add(MappingOfDestPathSkippedLogEntry(destProperty))
            return
        }

        val destBeanWithProperty = getLastBeanWithProperty(
            destBean, destProperty.path, true, srcValue,
            translationContext
        )
            ?: throw CantResolvePropertyPathException("Can't resolve property path ${destProperty.path}")

        val prevDestValue = destBeanWithProperty.property.getter()?.get(destBeanWithProperty.bean)

        var destValue: Any? = if (srcValue != null) translationContext.mappedBeansCache[MappedBeansCacheKey(
            srcValue,
            destPropertyDescription.propertyAccessorValueType.actualClass
        )] else null

        val skipSetNotNull = destProperty.isSkippedChange(SET_NOT_NULL)
        if (srcValue != null && destValue == null) {
            if (skipSetNotNull) {
                if (prevDestValue == null) {
                    log?.add(PropertyChangeSkippedLogEntry(destPropertyDescription, SET_NOT_NULL))
                    return
                }
                val destTypeCategory = destProperty.accessor.accessorValueType.typeCategory
                if (destTypeCategory == TypeCategory.Value) {
                    log?.add(PropertyChangeSkippedLogEntry(destPropertyDescription, SET_NOT_NULL))
                    return
                }
                if (destTypeCategory == TypeCategory.Bean && destProperty.isSkippedChange(BEAN_MAP)) {
                    // prev dest value of bean will be unchanged in any case, so skip this property
                    log?.add(PropertyChangeSkippedLogEntry(destPropertyDescription, SET_NOT_NULL))
                    return
                }
                if (destTypeCategory == TypeCategory.Collection
                    && destProperty.isSkippedChanges(BEAN_MAP, COLLECTION_ADD, COLLECTION_REMOVE)
                ) {
                    // prev dest value of collection will be unchanged in any case, so skip this property
                    log?.add(PropertyChangeSkippedLogEntry(destPropertyDescription, SET_NOT_NULL))
                    return
                }
            }

            val propertyConverter = srcProperty.propertyConverter

            val srcTypeInfo = srcProperty.accessor.accessorValueType
            val destTypeInfo = destProperty.accessor.accessorValueType
            var destClass = destTypeInfo.actualClass
            if (prevDestValue != null && destClass.isInterface) {
                destClass = prevDestValue.javaClass
            }

            if (propertyConverter != null) {
                val srcPropertyDescription = srcProperty.propertyDescription
                destValue = propertyConverter.convertValue(
                    srcValue,
                    srcPropertyDescription, destPropertyDescription
                )
                log?.add(
                    PropertyConverterUsedLogEntry(
                        propertyConverter,
                        srcPropertyDescription, destPropertyDescription, destValue
                    )
                )
            } else {
                destValue = translateInternal(
                    srcValue, srcTypeInfo.valueType, destClass, srcTypeInfo,
                    destTypeInfo, prevDestValue, destProperty.getSkippedChangesCached(),
                    translationContext.withCurrentLogEntry(log?.add(TranslationLogEntry(srcValue, destClass)))
                )
            }
        }

        if (destValue == null) {
            destValue = destProperty.getDefaultValue()
        }

        if (!ObjectsUtil.equals(destValue, prevDestValue)) {
            if (destValue == null && destProperty.isSkippedChange(SET_NULL)) {
                log?.add(PropertyChangeSkippedLogEntry(destPropertyDescription, SET_NULL))
                return
            }
            if (destValue != null && skipSetNotNull) {
                log?.add(PropertyChangeSkippedLogEntry(destPropertyDescription, SET_NOT_NULL))
                return
            }

            translationContext.addSetDestProperty(destBeanWithProperty, destValue)
            destProperty.accessor.set(destBeanWithProperty.bean, destValue)
            log?.add(SetPropertyValueLogEntry(destPropertyDescription, destValue))
        }
    }

    private fun getLastBeanWithProperty(
        rootBean: Any, path: List<Property>,
        forceCreatePath: Boolean, srcValue: Any?,
        translationContext: TranslationContext
    ): BeanWithProperty? {
        val log = translationContext.currentLogEntry
        var currBean: Any? = rootBean
        val length = path.size
        for ((i, p) in path.withIndex()) {
            if (currBean == null) {
                return null
            }

            if (i < length - 1) {
                val getter = p.getter() ?: return null
                val prevBean = currBean
                currBean = getter.get(currBean)
                if (currBean == null && forceCreatePath) {
                    val setter = p.setter() ?: return null

//                    if (translationContext.isPropertyAlreadySet(prevBean, p)) {
//                        return null
//                    }

                    if (srcValue != null && i == length - 2) {
                        val lastProperty = path[i + 1]
                        if (lastProperty.isIdentifier()) {
                            currBean = tryToFindBean(p, lastProperty, srcValue, translationContext)
                        }
                    }

                    if (currBean == null) {
                        currBean = createDestValueOfClass(
                            getter.accessorValueType.actualClass,
                            ValueCreationContext(Object(), null)
                        )
                    }

                    log?.add(SetPropertyValueLogEntry(p.getPropertyDescription(), currBean))
                    setter.set(prevBean, currBean)
                } else if (srcValue != null && i == length - 2) {
                    val lastProperty = path[i + 1]
                    if (lastProperty.isIdentifier()) {
                        currBean = tryToFindBean(p, lastProperty, srcValue, translationContext)
                        val setter = p.setter() ?: return null

                        log?.add(SetPropertyValueLogEntry(p.getPropertyDescription(), currBean))
                        setter.set(prevBean, currBean)
                    }
                }
            } else {
                return BeanWithProperty(currBean, p)
            }
        }
        return null
    }

    private fun tryToFindBean(
        destBeanProperty: Property, identifierProperty: Property,
        srcIdentifierValue: Any, translationContext: TranslationContext
    ): Any? {
        val log = translationContext.currentLogEntry
        val destIdentifierType = identifierProperty.getPropertyDescription().propertyAccessorValueType.actualClass
        val destBeanType = destBeanProperty.getPropertyDescription().propertyAccessorValueType.actualClass
        val beanFinder = configuration.beanOfClass(destBeanType).getBeanFinder()
        if (beanFinder != null) {
            val id = convertToTypeIfNeeded(srcIdentifierValue, destIdentifierType, false)
            if (id != null) {
                val res = beanFinder.findBean(destBeanType, id)
                log?.add(BeanFinderResultLogEntry(id, destBeanType, res))
                return res
            }
        }
        return null
    }

    private fun mapCollectionInternal(
        srcCollection: Any, destCollection: Any,
        srcAccessorValueType: AccessorValueType?,
        destAccessorValueType: AccessorValueType?,
        destValueType: Type?,
        skippedChanges: Set<ChangeType>,
        translationContext: TranslationContext
    ): Any {
        val log = translationContext.currentLogEntry

        val srcCollectionController = configuration.getCollectionController(srcCollection.javaClass)
        val destCollectionController = configuration.getCollectionController(destCollection.javaClass)

        val srcValues = srcCollectionController.getCollectionValues(srcCollection)
        val destValues = destCollectionController.getCollectionValues(destCollection)

        val srcComponentType = srcCollectionController.findCollectionComponentTypeByAccessor(
            srcCollection::class.java,
            srcAccessorValueType
        ) ?: ReflectionUtil.findCommonType(srcValues)

        val destComponentType =
            destCollectionController.findCollectionComponentTypeByAccessor(
                destCollection::class.java,
                destAccessorValueType
            ) ?: destCollectionController.findCollectionComponentType(destCollection::class.java, destValueType)
            ?: ReflectionUtil.findCommonType(destValues)

        val srcComponentClass = ReflectionUtil.getRawClass(srcComponentType)
        val destComponentClass = ReflectionUtil.getRawClass(destComponentType)

        val srcValuesTypeCategory = configuration.getTypeCategory(srcComponentClass)
        val destValuesTypeCategory = configuration.getTypeCategory(destComponentClass)

        val srcIdentifierProperty = tryGetIdentifierProperty(srcComponentClass, srcValuesTypeCategory)
        val destIdentifierProperty = tryGetIdentifierProperty(destComponentClass, destValuesTypeCategory)

        val beanFinder = if (destValuesTypeCategory == TypeCategory.Bean)
            configuration.beanOfClass(destComponentClass, destComponentType).getBeanFinder() else null

        val destValuesMap = buildIdentifiersMap(destValues, destIdentifierProperty, null)

        val skipMapBean = skippedChanges.contains(BEAN_MAP)
        val skipCollectionAdd = skippedChanges.contains(COLLECTION_ADD)
        val skipCollectionRemove = skippedChanges.contains(COLLECTION_REMOVE)

        val resultValues = ArrayList<Any?>()
        for (srcValue in srcValues) {
            if (srcValue == null) {
                resultValues.add(null)
                continue
            }

            val id = getValueIdentifier(
                srcValue, srcIdentifierProperty,
                destIdentifierProperty?.getter()?.accessorValueType?.actualClass
            )
            var destValue = destValuesMap[id]
            if (destValue == null && skipCollectionAdd) {
                continue
            }

            if (destValue == null && id != null && beanFinder != null) {
                destValue = beanFinder.findBean(destComponentClass, id)
            }

            if (destValue == null) {
                destValue = translateInternal(
                    srcValue, srcComponentType, destComponentType,
                    null, null, null, skippedChanges,
                    translationContext.withCurrentLogEntry(
                        log?.add(TranslationLogEntry(srcValue, destComponentClass))
                    )
                )
            } else if (srcValuesTypeCategory == TypeCategory.Bean && destValuesTypeCategory == TypeCategory.Bean
                && !skipMapBean
            ) {
                mapBeanInternal(
                    srcValue, null, destValue, null, translationContext.withCurrentLogEntry(
                        log?.add(MapBeanLogEntry(srcValue, destValue))
                    )
                )
            }
            resultValues.add(destValue)
        }
        if (skipCollectionRemove) {
            for ((i, v) in destValues.withIndex()) {
                val requiredCount = ObjectsUtil.countInCollection(v, destValues)
                val actualCount = ObjectsUtil.countInCollection(v, resultValues)
                if (actualCount < requiredCount) {
                    resultValues.add(i, v)
                }
            }
        }
        if (skipCollectionAdd) {
            for ((i, v) in resultValues.reversed().withIndex()) {
                val requiredCount = ObjectsUtil.countInCollection(v, destValues)
                val actualCount = ObjectsUtil.countInCollection(v, resultValues)
                if (actualCount > requiredCount) {
                    resultValues.remove(i)
                }
            }
        }

        val res = destCollectionController.setValues(destCollection, resultValues)
        log?.add(SetCollectionValuesLogEntry(resultValues))
        return res
    }

    private fun mapMapsInternal(
        srcMap: Any, destMap: Any,
        srcAccessorValueType: AccessorValueType?,
        destAccessorValueType: AccessorValueType?,
        mapType: Type,
        skippedChanges: Set<ChangeType>,
        translationContext: TranslationContext
    ): Any {
        val log = translationContext.currentLogEntry

        val srcMapController = configuration.getMapController(srcMap.javaClass)
        val destMapController = configuration.getMapController(destMap.javaClass)

        val srcValuesMap = srcMapController.getMapContent(srcMap)
        val destValuesMap = destMapController.getMapContent(destMap)

        val destKeyType = destMapController.findMapKeyTypeByAccessor(
            destMap::class.java,
            destAccessorValueType
        ) ?: ReflectionUtil.findCommonType(destValuesMap.keys)

        val srcValueType = srcMapController.findMapValueTypeByAccessor(
            srcMap::class.java,
            srcAccessorValueType
        ) ?: ReflectionUtil.findCommonType(srcValuesMap.values)

        val destValueType = destMapController.findMapValueTypeByAccessor(destMap::class.java, destAccessorValueType)
            ?: destMapController.findMapValueType(destMap::class.java, mapType) ?: ReflectionUtil.findCommonType(
                destValuesMap.values
            )

        val destKeyClass = ReflectionUtil.getRawClass(destKeyType)
        val srcValueClass = ReflectionUtil.getRawClass(srcValueType)
        val destValueClass = ReflectionUtil.getRawClass(destValueType)

        val srcValuesTypeCategory = configuration.getTypeCategory(srcValueClass)
        val destValuesTypeCategory = configuration.getTypeCategory(destValueClass)

        val skipMapBean = skippedChanges.contains(BEAN_MAP)
        val skipMapAdd = skippedChanges.contains(MAP_ADD)
        val skipMapRemove = skippedChanges.contains(MAP_REMOVE)
        val skipMapReplaceValue = skippedChanges.contains(MAP_REPLACE_VALUE)

        val resultMap = HashMap<Any, Any?>()
        for (srcEntry in srcValuesMap) {
            val id = convertToTypeIfNeeded(srcEntry.key, destKeyClass, true)!!
            val srcValue = srcEntry.value

            if (srcValue == null) {
                resultMap[id] = null
                continue
            }

            if (!destValuesMap.containsKey(id) && skipMapAdd) {
                continue
            }
            var destValue = destValuesMap[id]

            if (destValue != null && srcValuesTypeCategory == TypeCategory.Bean &&
                destValuesTypeCategory == TypeCategory.Bean && !skipMapBean
            ) {
                mapBeanInternal(
                    srcValue, srcValueType, destValue, destValueType,
                    translationContext.withCurrentLogEntry(log?.add(MapBeanLogEntry(srcValue, destValue)))
                )
            } else if (!skipMapReplaceValue) {
                destValue = translateInternal(
                    srcValue, srcValueType, destValueType,
                    null, null, null, skippedChanges,
                    translationContext.withCurrentLogEntry(
                        log?.add(TranslationLogEntry(srcValue, destValueClass))
                    )
                )
            }
            resultMap[id] = destValue
        }
        if (skipMapRemove) {
            for ((id, value) in destValuesMap) {
                val requiredCount = ObjectsUtil.countInCollection(id, destValuesMap.keys)
                val actualCount = ObjectsUtil.countInCollection(id, resultMap.keys)
                if (actualCount < requiredCount) {
                    resultMap[id] = value
                }
            }
        }
        if (skipMapAdd) {
            for (id in resultMap.keys) {
                val requiredCount = ObjectsUtil.countInCollection(id, destValuesMap.keys)
                val actualCount = ObjectsUtil.countInCollection(id, resultMap.keys)
                if (actualCount > requiredCount) {
                    resultMap.remove(id)
                }
            }
        }

        val res = destMapController.setContent(destMap, resultMap)
        log?.add(SetMapContentLogEntry(resultMap))
        return res
    }

    private fun tryGetIdentifierProperty(valueType: Class<*>, valueTypeCategory: TypeCategory): Property? {
        if (valueTypeCategory == TypeCategory.Bean) {
            return configuration.beanOfClass(valueType).getIdentifierProperty()
        }
        return null
    }

    private fun buildIdentifiersMap(
        values: List<Any?>,
        identifierProperty: Property?,
        expectedIdentifierType: Class<*>?
    ): Map<Any, Any> {
        val res = HashMap<Any, Any>()
        for (v in values) {
            if (v != null) {
                val id = getValueIdentifier(v, identifierProperty, expectedIdentifierType)
                if (id != null) {
                    res.put(id, v)
                }
            }
        }
        return res
    }

    private fun getValueIdentifier(
        value: Any, identifierProperty: Property?,
        expectedIdentifierType: Class<*>?
    ): Any? {
        var res: Any? = value
        if (identifierProperty != null) {
            res = identifierProperty.getter()!!.get(value)
        }
        return convertToTypeIfNeeded(res, expectedIdentifierType, false)
    }

    private fun isLongPathEndedWithIdentifier(path: List<Property>) =
        path.size > 1 && path.last().isIdentifier()

    private fun convertToTypeIfNeeded(value: Any?, expectedClass: Class<*>?, failIfCantConvert: Boolean): Any? {
        if (expectedClass != null && value != null && !expectedClass.isAssignableFrom(value::class.java)) {
            val converter = configuration.getTypeConverter(value::class.java, expectedClass)
            if (converter != null) {
                return converter.convert(value, expectedClass)
            }
            if (failIfCantConvert) {
                throw ConversionException("Can't convert $value to type $expectedClass")
            }
        }
        return value
    }


}
