package org.bitbucket.brunneng.ot.creators

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Constructor

/**
 * Creator which can create values of classes which have constructor without arguments.
 */
class ConstructorWithoutArgsDestValueCreator : DestValueCreator {

    override fun isCanCreate(destValueClass: Class<*>): Boolean {
        return getDefaultConstructor(destValueClass) != null
    }

    override fun create(destValueClass: Class<*>, valueCreationContext: ValueCreationContext): Any {
        return getDefaultConstructor(destValueClass)!!.newInstance()
    }

    private fun getDefaultConstructor(objClass: Class<*>): Constructor<*>? {
        try {
            val res = objClass.getDeclaredConstructor()
            ReflectionUtil.ensureAccessible(res)
            return res
        } catch (e: Exception) {
            return null
        }
    }
}
