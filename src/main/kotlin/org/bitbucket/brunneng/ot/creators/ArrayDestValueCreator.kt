package org.bitbucket.brunneng.ot.creators

import org.bitbucket.brunneng.ot.exceptions.DestValueCreationException

/**
 * Creator to create arrays. Length is deduced from source array length, or source collection size. If lengths can't
 * be found, then [DestValueCreationException] is thrown.
 */
class ArrayDestValueCreator : DestValueCreator {

    override fun isCanCreate(destValueClass: Class<*>): Boolean {
        return destValueClass.isArray
    }

    override fun create(destValueClass: Class<*>, valueCreationContext: ValueCreationContext): Any {
        val length = findArrayLength(valueCreationContext.srcValue) ?: throw DestValueCreationException(
            "Can't find size of new array, from src object ${valueCreationContext.srcValue}"
        )
        return java.lang.reflect.Array.newInstance(destValueClass.componentType, length)
    }

    protected fun findArrayLength(srcObject: Any): Int? {
        if (srcObject is Collection<*>) {
            return srcObject.size
        }
        if (srcObject is Array<*>) {
            return srcObject.size
        }
        return null
    }
}
