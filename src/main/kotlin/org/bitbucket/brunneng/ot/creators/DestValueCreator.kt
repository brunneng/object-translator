package org.bitbucket.brunneng.ot.creators

import org.bitbucket.brunneng.ot.Configuration

/**
 * Creator for destination values, which creates instance of supported class.
 * Result object should be assignable to required class.
 *
 * Use method [Configuration.addCustomDestValueCreator] to add it to configuration.
 */
interface DestValueCreator {

    /**
     * @param destValueClass required destination value class
     *
     * @return is can create instance of desired [destValueClass] or its inheritor? If yes, then [create] will be
     * called
     */
    fun isCanCreate(destValueClass: Class<*>): Boolean

    /**
     * Creates instance assignable to [destValueClass]
     *
     * @param destValueClass required destination value class
     * @param valueCreationContext additional information which can help to create destination value
     *
     * @return new instance assignable to [destValueClass]
     */
    fun create(destValueClass: Class<*>, valueCreationContext: ValueCreationContext): Any

}