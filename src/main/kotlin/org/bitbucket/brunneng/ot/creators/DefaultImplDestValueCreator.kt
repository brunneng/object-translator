package org.bitbucket.brunneng.ot.creators

/**
 * When destination property is type of interface, translator should choose default implementation of it, to create
 * instance which will be assignable to this interface.
 * Current creator provides next default implementations: [Collection] -> [ArrayList], [List] -> [ArrayList],
 * [Set] -> [HashSet], [Map] -> [HashMap].
 */
class DefaultImplDestValueCreator : DestValueCreator {

    private val defaultImplementations: HashMap<Class<*>, Class<*>> = HashMap()
    private val creator = ConstructorWithoutArgsDestValueCreator()

    init {
        defaultImplementations[Collection::class.java] = ArrayList::class.java
        defaultImplementations[List::class.java] = ArrayList::class.java
        defaultImplementations[Set::class.java] = HashSet::class.java
        defaultImplementations[Map::class.java] = HashMap::class.java
    }

    override fun isCanCreate(destValueClass: Class<*>): Boolean {
        return defaultImplementations.containsKey(destValueClass)
    }

    override fun create(destValueClass: Class<*>, valueCreationContext: ValueCreationContext): Any {
        return creator.create(defaultImplementations[destValueClass]!!, valueCreationContext)
    }
}
