package org.bitbucket.brunneng.ot.creators

/**
 * Holds information which helps to create new destination value.
 *
 * @param srcValue source value which will be mapped to created destination value
 * @param prevDestValue previous destination value. If not *null* then its class can be used to create instance of
 * same class.
 */
class ValueCreationContext(val srcValue: Any, val prevDestValue: Any?)
