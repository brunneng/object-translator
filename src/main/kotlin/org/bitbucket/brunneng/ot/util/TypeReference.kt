package org.bitbucket.brunneng.ot.util

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import org.bitbucket.brunneng.ot.ObjectTranslator
import java.lang.reflect.Type

/**
 * Auxiliary class used to reference the target type with generic component for translation.
 * You should create anonymous class of it when passing to ObjectTranslator.translate.
 *
 * Example in java to convert the array of integers to the list of longs:
 *
 *     ObjectTranslator ot = new ObjectTranslator();
 *     int[] src = new int[] {1, 2}
 *     List<Long> target = ot.translate(src, new TypeReference<List<Long>>() {});
 *
 * Note, that if the target class has no generic component, then using [TypeReference] is not required.
 * @see [ObjectTranslator.translate]
 */
abstract class TypeReference<T> {

    /**
     * @return the generic parameter type of this type reference
     */
    fun getGenericParameterType(): Type {
        val genericTypesValues = ReflectionUtil.getActualTypeVariablesTypes(this.javaClass).values
        return genericTypesValues.firstOrNull() ?: Any::class.java
    }
}