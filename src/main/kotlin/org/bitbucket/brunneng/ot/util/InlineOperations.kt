package org.bitbucket.brunneng.ot.util

inline fun <K, V> MutableMap<K, V>.getOrPutSupportedNulls(key: K, defaultValue: () -> V): V? {
    val value = get(key)

    return if (value == null && !contains(key)) {
        val answer = defaultValue()
        put(key, answer)
        answer
    } else {
        value
    }
}