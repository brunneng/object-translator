package org.bitbucket.brunneng.ot.util

import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.Modifier
import java.lang.reflect.Type

object TypeUtil {

    /**
     * @param type type to check
     * @return is type is interface or abstract class?
     */
    @JvmStatic
    fun isInterfaceOrAbstractClass(type: Type?): Boolean {
        if (type == null) {
            return false
        }
        val rawClass = ReflectionUtil.getRawClass(type)
        return rawClass.isInterface || Modifier.isAbstract(rawClass.modifiers)
    }
}