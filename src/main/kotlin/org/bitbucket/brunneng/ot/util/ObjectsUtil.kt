package org.bitbucket.brunneng.ot.util

import java.util.*

object ObjectsUtil {

    /**
     * Same as [Objects.equals] but returns false, if [o1] is not null and [o2] is null. Which protects
     * from wrongly implemented method equals, which in this case can throw [NullPointerException].
     *
     * @param o1 first object
     * @param o2 second object
     *
     * @return are objects equal?
     */
    @JvmStatic
    fun equals(o1: Any?, o2: Any?): Boolean {
        if (o1 != null && o2 == null) {
            return false
        }
        return Objects.equals(o1, o2)
    }

    /**
     * @param testObject object which will be tested on equality, can be null
     * @param collection collection of objects
     *
     * @return count of values in [collection] which are equal to [testObject]
     */
    @JvmStatic
    fun countInCollection(testObject: Any?, collection: Collection<Any?>): Int {
        return collection.count { equals(testObject, it) }
    }
}
