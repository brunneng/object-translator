package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorDynamicFieldsTest extends Specification {

  def 'test dynamic translation'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).property("includedProperties").markAsDynamicMappedPropertiesCollection()
    def ot = new ObjectTranslator(c)
    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)
    o1.includedProperties = ["title"]

    then:
    def o2 = ot.translate(o1, Bean2.class)
    o2 != null
    o2.id != o1.id
    o2.id == null
    o2.title == o1.title
    o2.includedProperties == null
  }

  def 'test dynamic translation - null dynamic properties'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).property("includedProperties").markAsDynamicMappedPropertiesCollection()
    def ot = new ObjectTranslator(c)
    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)
    o1.includedProperties = null

    then:
    def o2 = ot.translate(o1, Bean2.class)
    o2 != null
    o2.id == o1.id
    o2.title == o1.title
    o2.includedProperties == null
  }

  static class Bean1 {
    Integer id
    String title
    Set<String> includedProperties = new HashSet<>()
  }

  static class Bean2 {
    Integer id
    String title
    Set<String> includedProperties
  }
}
