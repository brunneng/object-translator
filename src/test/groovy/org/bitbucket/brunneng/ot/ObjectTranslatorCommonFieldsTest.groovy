package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorCommonFieldsTest extends Specification {

  def 'test translation of common fields'() {
    when:
    def ot = new ObjectTranslator()
    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)
    then:
    o1.age != null
    def o2 = ot.translate(o1, Bean2.class)
    o2 != null
    o2.name == o1.name // name is the only common field here
    o2.lastName == null
  }

  static class Bean1 {
    String name
    Integer age
  }

  static class Bean2 {
    String name
    String lastName
  }

}
