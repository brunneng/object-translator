package org.bitbucket.brunneng.ot


import spock.lang.Specification

class PropertyPathTranslationTest extends Specification {

  def 'test translate deep paths'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Bean1.class).translationTo(Bean1DTO.class)
    t.srcProperty("v1").translatesTo("bean2.v1")
    t.srcProperty("bean2.v2").translatesTo("v2")
    ObjectTranslator ot = new ObjectTranslator(c)

    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)

    def o2 = ot.translate(o1, Bean1DTO.class)
    then:
    o2.bean2.v1 == o1.v1
    o2.v2 == o1.bean2.v2

    when:
    o1.v1 = null
    ot.mapBean(o1, o2)
    then:
    o2.bean2.v1 == null

    when:
    o1.v1 = "v1"
    o1.bean2 = null
    ot.mapBean(o1, o2)
    then:
    o2.v2 == null
    o2.bean2 != null
    o2.bean2.v1 == o1.v1
  }

  static class Bean1 {
    String v1
    Bean2 bean2
  }

  static class Bean2 {
    String v2
  }

  static class Bean1DTO {
    String v2
    Bean2DTO bean2
  }

  static class Bean2DTO {
    String v1
  }

}
