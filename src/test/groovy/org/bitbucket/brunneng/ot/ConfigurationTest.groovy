package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.exceptions.DefaultValueWrongClassException
import org.bitbucket.brunneng.ot.exceptions.WrongConfigurationOrderException
import spock.lang.Specification

import java.lang.reflect.Method
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class ConfigurationTest extends Specification {

  def 'test get property getter() & onlyPublic'(String propertyName, boolean onlyPublic, Boolean methodAccessor) {
    when:
    def c = new Configuration()
    def propertyConfig = c.beanOfClass(AccessorsTestBean.class).property(propertyName)
    if (onlyPublic) {
      propertyConfig.allowOnlyPublicAccessors()
    }
    then:
    def getter = propertyConfig.getter()
    if (methodAccessor == null) {
      assert getter == null
    } else {
      assert getter.fieldOrMethod instanceof Method == methodAccessor
    }
    where:
    propertyName | onlyPublic | methodAccessor
    "s1"         | false      | false
    "s1"         | true       | null
    "s2"         | false      | false
    "s2"         | true       | false
    "s3"         | false      | true
    "s3"         | true       | true
    "s4"         | false      | false
    "s4"         | true       | false
    "s5"         | false      | true
    "s5"         | true       | true
    "s6"         | false      | true
    "s6"         | true       | null
  }

  def 'test get property setter() & onlyPublic'(String propertyName, boolean onlyPublic, Boolean methodAccessor) {
    when:
    def c = new Configuration()
    def propertyConfig = c.beanOfClass(AccessorsTestBean.class).property(propertyName)
    if (onlyPublic) {
      propertyConfig.allowOnlyPublicAccessors()
    }
    then:
    def setter = propertyConfig.setter()
    if (methodAccessor == null) {
      assert setter == null
    } else {
      assert setter.fieldOrMethod instanceof Method == methodAccessor
    }
    where:
    propertyName | onlyPublic | methodAccessor
    "s1"         | false      | false
    "s1"         | true       | null
    "s2"         | false      | false
    "s2"         | true       | false
    "s3"         | false      | true
    "s3"         | true       | true
    "s4"         | false      | false
    "s4"         | true       | false
    "s5"         | false      | true
    "s5"         | true       | true
    "s6"         | false      | true
    "s6"         | true       | null
  }

  def 'test allow only public wrong configuration order 1'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AccessorsTestBean.class).translationTo(AccessorsTestBean.class).srcProperty("s1")
    def propertyConfig = c.beanOfClass(AccessorsTestBean.class).property("s1")
    propertyConfig.allowOnlyPublicAccessors()
    then:
    thrown WrongConfigurationOrderException
  }

  def 'test allow only public wrong configuration order 2'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AccessorsTestBean.class).translationTo(AccessorsTestBean.class).destProperty("s1")
    def propertyConfig = c.beanOfClass(AccessorsTestBean.class).property("s1")
    propertyConfig.allowOnlyPublicAccessors()
    then:
    thrown WrongConfigurationOrderException
  }

  def 'test is value type'(Class testClass, boolean matched) {
    when:
    def c = new Configuration()
    then:
    c.isValueType(testClass) == matched
    where:
    testClass           | matched
    Integer.TYPE        | true
    Integer.class       | true
    BigDecimal.class    | true
    Date.class          | true
    Calendar.class      | true
    LocalTime.class     | true
    LocalDate.class     | true
    LocalDateTime.class | true
    SomeEnum.class      | true
    UUID.class          | true
    List.class          | false
    ArrayList.class     | false
    Collection.class    | false
    Map.class           | false
    new int[0].class    | false
    new int[0][0].class | false
    BeanChild1.class    | false
  }

  def 'test is collection type'(Class testClass, boolean matched) {
    when:
    def c = new Configuration()
    then:
    c.isCollectionType(testClass) == matched
    where:
    testClass           | matched
    List.class          | true
    ArrayList.class     | true
    Collection.class    | true
    Map.class           | false
    new int[0].class    | true
    new int[0][0].class | true
    BeanChild1.class    | false
    Integer.class       | false
  }

  def 'test is map type'(Class testClass, boolean matched) {
    when:
    def c = new Configuration()
    then:
    c.isMapType(testClass) == matched
    where:
    testClass     | matched
    List.class    | false
    Map.class     | true
    HashMap.class | true
  }

  def 'test is bean type'(Class testClass, boolean matched) {
    when:
    def c = new Configuration()
    then:
    c.isBeanType(testClass) == matched
    where:
    testClass        | matched
    List.class       | false
    new int[0].class | false
    Map.class        | false
    Integer.class    | false
    BeanParent.class | true
    BeanChild1.class | true
  }

  def 'test has type converter'(Class srcClass, Class destClass, boolean hasConverter) {
    when:
    def c = new Configuration()
    then:
    (c.getTypeConverter(srcClass, destClass) != null) == hasConverter
    where:
    srcClass   | destClass           | hasConverter
    Byte.class | Double.class        | true
    Byte.class | Float.class         | true
    Byte.class | Long.class          | true
    Byte.class | Integer.class       | true
    Byte.class | Short.class         | true
    Date.class | LocalDateTime.class | true
    Date.class | String.class        | true
    E1.class   | E2.class            | true
  }

  def 'test has type converter, check cache'() {
    when:
    def c = new Configuration()
    then:
    c.getTypeConverter(Byte.class , E2.class) == null
    c.getTypeConverter(Byte.class , E2.class) == null
  }

  def 'test set/get identifier property'() {
    when:
    def c = new Configuration()
    def parentBeanConfig = c.beanOfClass(BeanParent.class)
    parentBeanConfig.setIdentifierProperty("i")

    def beanChildConfig = c.beanOfClass(BeanChild1.class)
    then:
    parentBeanConfig.getIdentifierProperty() == parentBeanConfig.property("i")
    beanChildConfig.getIdentifierProperty() == beanChildConfig.property("i")
  }

  def 'test set skipped change type on property'() {
    when:
    def c = new Configuration()
    def parentBeanConfig = c.beanOfClass(BeanParent.class)
    def beanChildConfig = c.beanOfClass(BeanChild1.class)
    def parentProperty = parentBeanConfig.property("i")
    def childProperty = beanChildConfig.property("i")
    def changeType = Configuration.ChangeType.SET_NULL
    then:
    !parentProperty.isSkippedChange(changeType)
    !childProperty.isSkippedChange(changeType)

    when:
    parentProperty.skipChange(changeType)
    then:
    parentProperty.isSkippedChange(changeType)
    childProperty.isSkippedChange(changeType)
  }

  def 'test set skip all changes on property'() {
    when:
    def c = new Configuration()
    def parentBeanConfig = c.beanOfClass(BeanParent.class)
    def beanChildConfig = c.beanOfClass(BeanChild1.class)
    def parentProperty = parentBeanConfig.property("i")
    def childProperty = beanChildConfig.property("i")

    then:
    for (changeType in Configuration.ChangeType.values()) {
      assert !parentProperty.isSkippedChange(changeType)
      assert !childProperty.isSkippedChange(changeType)
    }

    when:
    parentProperty.skipAllChanges()
    then:
    for (changeType in Configuration.ChangeType.values()) {
      assert parentProperty.isSkippedChange(changeType)
      assert childProperty.isSkippedChange(changeType)
    }
  }

  def 'test set skipped as source on property'() {
    when:
    def c = new Configuration()
    def parentBeanConfig = c.beanOfClass(BeanParent.class)
    def beanChildConfig = c.beanOfClass(BeanChild1.class)
    def parentProperty = parentBeanConfig.property("i")
    def childProperty = beanChildConfig.property("i")

    then:
    !parentProperty.isSkippedAsSource()
    !childProperty.isSkippedAsSource()

    when:
    parentProperty.skipAsSource()
    then:
    parentProperty.isSkippedAsSource()
    childProperty.isSkippedAsSource()
  }

  def 'test set default value'() {
    when:
    def c = new Configuration()
    def parentBeanConfig = c.beanOfClass(BeanParent.class)
    def childBeanConfig = c.beanOfClass(BeanChild1.class)
    def dp = parentBeanConfig.property("d")
    def dc = childBeanConfig.property("d")
    dp.setDefaultValue(0)
    then:
    thrown DefaultValueWrongClassException.class
    when:
    def doubleDefaultValue = 0.0d
    dp.setDefaultValue(doubleDefaultValue)
    then:
    dp.getDestDefaultValue() == doubleDefaultValue
    dp.getSrcDefaultValue() == doubleDefaultValue
    dc.getDestDefaultValue() == doubleDefaultValue
    dc.getSrcDefaultValue() == doubleDefaultValue
    when:
    def stringDefaultValue = ""
    def xp = parentBeanConfig.property("x")
    def xc = childBeanConfig.property("x")
    xp.setDefaultValue(stringDefaultValue)
    then:
    xp.getDestDefaultValue() == stringDefaultValue
    xp.getSrcDefaultValue() == null
    xc.getDestDefaultValue() == stringDefaultValue
    xc.getSrcDefaultValue() == null
  }

  def 'test set default value generic'() {
    when:
    def c = new Configuration()
    def parentBeanConfig = c.beanOfClass(BeanParent.class)
    def childBeanConfig = c.beanOfClass(BeanChild1.class)
    def valueParent = parentBeanConfig.property("value")
    def valueChild = childBeanConfig.property("value")
    valueParent.setDefaultValue(0)
    then:
    valueParent.getDestDefaultValue() == 0
    valueChild.getDestDefaultValue() == null
    when:
    valueChild.setDefaultValue(0)
    then:
    thrown DefaultValueWrongClassException.class
  }

  def 'test set default value generic child'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Bean2.class).translationTo(Bean2.class)
    def childValueProperty = t.destProperty("child.value")
    childValueProperty.setDefaultValue(5)
    then:
    thrown DefaultValueWrongClassException.class
    when:
    def parentBeanValue = c.beanOfClass(BeanParent.class).property("value")
    parentBeanValue.setDefaultValue(5)
    then:
    childValueProperty.getDefaultValue() == null
    when:
    parentBeanValue.setDefaultValue(5.0d)
    then:
    childValueProperty.getDefaultValue() == 5.0d
    when:
    childValueProperty.setDefaultValue(10.0d)
    then:
    childValueProperty.getDefaultValue() == 10.0d
  }

  static class BeanParent<T> {

    private int i
    private Double d
    T value

    int getI() {
      return i;
    }

    void setI(int i) {
      this.i = i;
    }

    void setD(Double d) {
      this.d = d;
    }

    void setX(String s) {

    }
  }

  static class BeanChild1 extends BeanParent<Double> {
    private String s

    String getS() {
      return s;
    }
  }

  static class Bean2 {
    BeanParent<Double> child
  }

  static enum E1 {
    V1,
    V2,
    V3
  }

  static enum E2 {
    V1,
    V2,
  }

  static class Bean1 {
    String title
    Bean1 nested
  }

}
