package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorValueFieldsTest extends Specification {

  def 'test translation of value fields'() {
    when:
    def ot = new ObjectTranslator()
    ValueFields1 o1 = RandomizerUtils.generateObject(ValueFields1.class)
    def o2 = ot.translate(o1, ValueFields2.class)
    then:
    o2 != null
    o2.getClass() == ValueFields2.class
    o1.title == o2.title
    (long)o1.age == o2.age
    o2.calledSetters == ["setTitle", "setAge"].toSet()
    when:
    o2.setAge(3)
    o2.calledSetters.clear()
    ot.mapBean(o1, o2)
    then:
    o1.title == o2.title
    (long)o1.age == o2.age
    o2.calledSetters == ["setAge"].toSet() // title is not modified
    when:
    o1.setTitle(null)
    ot.mapBean(o1, o2)
    then:
    o1.title == o2.title
  }

  static class ValueFields1 {
    String title;
    int age;
  }

  static class ValueFields2 {
    String title
    Long age
    Set<String> calledSetters = new HashSet<>()

    void setTitle(String title) {
      this.title = title
      calledSetters.add("setTitle")
    }

    void setAge(Long age) {
      this.age = age
      calledSetters.add("setAge")
    }
  }
}
