package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.exceptions.ImpossibleToConvertException
import spock.lang.Specification

class ValidateTranslationTest extends Specification {

  def 'test validate'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Bean1.class).translationTo(Bean2.class)
    c.validate()
    then:
    thrown ImpossibleToConvertException.class
    when:
    t.srcProperty("v1").validateTranslation$object_translator()
    then:
    thrown ImpossibleToConvertException.class
    when:
    t.srcProperty("v2").validateTranslation$object_translator()
    then:
    noExceptionThrown()
  }

  class Bean1 {
    String v1
    String v2
  }

  class Bean2 {
    Long v1
    String v2
  }
}
