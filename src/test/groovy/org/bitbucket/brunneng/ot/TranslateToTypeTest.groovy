package org.bitbucket.brunneng.ot


import org.bitbucket.brunneng.ot.util.TypeReference
import spock.lang.Specification

class TranslateToTypeTest extends Specification {

    def 'test translate array to list with type conversion'() {
        when:
        def ot = new ObjectTranslator()
        int[] src = [1, 2]
        def res = ot.translate(src, new TypeReference<List<String>>() {})
        then:
        res instanceof List
        res == ["1", "2"]
    }

    def 'test translate map to map with type conversion'() {
        when:
        def ot = new ObjectTranslator()
        Map<String, Integer> src = ["hi" : 1]
        def res = ot.translate(src, new TypeReference<Map<String, String>>() {})
        then:
        res instanceof Map
        res == ["hi" : "1"]
    }
}
