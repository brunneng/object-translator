package org.bitbucket.brunneng.ot


import spock.lang.Specification

import java.time.LocalDateTime

class ObjectTranslatorSkipAsSourceTest extends Specification {

  def 'test skip as source'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    def o1 = new Bean1()
    o1.setTitle("test")
    o1.setNote("notes1")
    o1.setChild(new GenericBean<Integer>())
    o1.getChild().setValue(5)

    def beanConfig = c.beanOfClass(Bean1.class)
    beanConfig.property("title").skipAsSource()
    beanConfig.property("note").skipAsSource()
    c.beanOfClass(GenericBean.class).property("value").skipAsSource()

    def o2 = ot.translate(o1, Bean2.class)
    then:
    o2.title == null
    o2.note == null
    o2.child != null
    o2.child.value == null
    when:
    ot.mapBean(o1, o2)
    then:
    o2.title == null
    o2.note == null
    o2.child != null
    o2.child.value == null
  }

  static class Bean1 {
    String title
    String note
    GenericBean<Integer> child
  }

  static class Bean2 {
    String title
    LocalDateTime note // non convertable type
    GenericBean<Integer> child
  }

  static class GenericBean<T> {
    T value
  }

}
