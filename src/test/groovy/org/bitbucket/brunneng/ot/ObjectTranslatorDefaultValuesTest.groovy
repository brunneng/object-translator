package org.bitbucket.brunneng.ot


import spock.lang.Specification

class ObjectTranslatorDefaultValuesTest extends Specification {

  def 'test translate default values'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    def o1 = new Bean1()

    def abstractBeanConfig = c.beanOfClass(AbstractBean.class)
    def bean1Config = c.beanOfClass(Bean1.class)
    def bean1DtoConfig = c.beanOfClass(Bean1DTO.class)
    bean1DtoConfig.property("s1").setDefaultValue("v1")
    bean1Config.property("s2").setDefaultValue("v2")
    abstractBeanConfig.property("s3").setDefaultValue("v3")

    def translation = bean1Config.translationTo(Bean1DTO.class)
    translation.srcProperty("s4").setDefaultValue("v4")
    translation.destProperty("s5").setDefaultValue("v5")
    def o2 = ot.translate(o1, Bean1DTO.class)
    then:
    o2.s1 == "v1"
    o2.s2 == "v2"
    o2.s3 == "v3"
    o2.s4 == "v4"
    o2.s5 == "v5"
  }

  static abstract class AbstractBean {
    String s1
    String s2
    String s3
    String s4
    String s5
  }

  static class Bean1 extends AbstractBean {
  }

  static class Bean1DTO extends AbstractBean {
  }
}
