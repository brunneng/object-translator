package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorNoSetterOrGetterTest extends Specification {

  def 'test if no setters then no exception and property ignored'() {
    when:
    def ot = new ObjectTranslator()
    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)
    then:
    def o2 = ot.translate(o1, Bean1.class)
    o2 != null
    o2.value == o1.value
  }

  def 'test if no getter then no exception and property ignored'() {
    when:
    def ot = new ObjectTranslator()
    Bean2 o1 = RandomizerUtils.generateObject(Bean2.class)
    then:
    def o2 = ot.translate(o1, Bean2.class)
    o2 != null
    o2.value == o1.value
  }

  static class Bean1 {
    String value

    private String getFoo() {
      return value
    }
  }

  static class Bean2 {
    String value

    private void setFoo(String value) {
    }
  }
}
