package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.exceptions.DuplicatedDestPropertyMappingException
import spock.lang.Specification

class ValidatePropertyPathTranslationTest extends Specification {

  def 'test validate'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Bean1.class).translationTo(Bean1.class)
    t.srcProperty("v1").translatesTo("bean2.v2")
    then:
    true
    thrown DuplicatedDestPropertyMappingException.class
    when:
    t.srcProperty("bean2.v2").translatesTo("v1")
    then:
    thrown DuplicatedDestPropertyMappingException.class
    when:
    c.beanOfClass(Bean2.class).translationTo(Bean2.class).srcProperty("v2")
            .translatesTo("v2").skipAllChanges()
    t.srcProperty("v1").translatesTo("bean2.v2")
    then:
    noExceptionThrown()
    when:
    c = new Configuration()
    t = c.beanOfClass(Bean1.class).translationTo(Bean1.class)
    t.srcProperty("v1").skipAsSource()
    t.srcProperty("bean2.v2").translatesTo("v1")
    then:
    noExceptionThrown()
    when:
    c = new Configuration()
    t = c.beanOfClass(Bean1.class).translationTo(Bean1.class)
    c.beanOfClass(Bean2.class).translationTo(Bean2.class).srcProperty("v3").skipAsSource()
    t.srcProperty("v1").translatesTo("bean2.v3")
    t.srcProperty("bean2.v2").translatesTo("v1")
    then:
    noExceptionThrown()
  }

  class Bean1 {
    String v1
    Bean2 bean2
  }

  class Bean2 {
    String v2
    String v3
  }

}
