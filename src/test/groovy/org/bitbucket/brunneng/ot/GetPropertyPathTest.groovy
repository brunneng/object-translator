package org.bitbucket.brunneng.ot


import org.bitbucket.brunneng.introspection.exceptions.NoSuchPropertyException
import org.bitbucket.brunneng.ot.exceptions.WrongPropertyCategoryException
import spock.lang.Specification

class GetPropertyPathTest extends Specification {

  def 'get property path test'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Bean1.class).translationTo(Bean1.class)
    then:
    t.srcProperty("v1").path ==
            [c.beanOfClass(Bean1.class).property("v1")]

    t.srcProperty("bean2.v2").path ==
            [c.beanOfClass(Bean1.class).property("bean2"), c.beanOfClass(Bean2.class).property("v2")]

    t.srcProperty("bean2.bean3.v3").path ==
            [c.beanOfClass(Bean1.class).property("bean2"),
             c.beanOfClass(Bean2.class).property("bean3"),
             c.beanOfClass(Bean3.class).property("v3")]

    when:
    t.srcProperty("v1.v2")
    then:
    thrown WrongPropertyCategoryException.class

    when:
    t.srcProperty("")
    then:
    thrown NoSuchPropertyException.class
  }

  class Bean1 {
    String v1
    Bean2 bean2
  }

  class Bean2 {
    String v2
    Bean3 bean3
  }

  class Bean3 {
    String v3
  }
}
