package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.exceptions.MultiplePropertySettingException
import org.bitbucket.brunneng.ot.exceptions.PropertyMappingException
import spock.lang.Specification

class TranslationOnSameBeanTest extends Specification {

  def 'test translate on same bean'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Bean1.class).translationTo(Bean1DTO.class)
    t.srcProperty("bean3").translatesTo("bean2")
    ObjectTranslator ot = new ObjectTranslator(c)

    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)

    def o2 = ot.translate(o1, Bean1DTO.class)
    then:
    o2.bean2.v2 == o1.bean2.v2
    o2.bean2.v3 == o1.bean3.v3

    when:
    o1.bean3 = null
    ot.mapBean(o1, o2)
    then:
    def ex = thrown PropertyMappingException
    ex.getCause() instanceof MultiplePropertySettingException
  }

  static class Bean1 {
    Bean2 bean2
    Bean3 bean3
  }

  static class Bean2 {
    String v2
  }

  static class Bean3 {
    String v3
  }

  static class Bean1DTO {
    Bean2DTO bean2
  }

  static class Bean2DTO {
    String v2
    String v3
  }

}
