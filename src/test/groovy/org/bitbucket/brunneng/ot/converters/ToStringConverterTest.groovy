package org.bitbucket.brunneng.ot.converters

import spock.lang.Specification

import java.time.DayOfWeek
import java.time.LocalDateTime

class ToStringConverterTest extends Specification {
  static def now = LocalDateTime.now()

  def 'test convert to string'(Object srcObject, String destString) {
    when:
    def c = new ToStringConverter()
    then:
    c.canConvert(srcObject.getClass(), destString.getClass())
    c.convert(srcObject, destString.getClass()) == destString
    where:
    srcObject        | destString
    1                | "1"
    2.21             | "2.21"
    DayOfWeek.SUNDAY | "SUNDAY"
    now              | now.toString()
    "test"           | "test"
  }
}
