package org.bitbucket.brunneng.ot.converters

import spock.lang.Specification

class NumberConvertersTest extends Specification {

  def 'test convert to double'(Object srcValue, Double destValue) {
    when:
    def c = new ToDoubleNumberConverter()
    then:
    c.canConvert(srcValue.getClass(), destValue.getClass())
    c.convert(srcValue, destValue.getClass()) == destValue
    where:
    srcValue             | destValue
    new Byte((byte) 1)   | 1.0
    new Short((short) 2) | 2.0
    new Integer(3)       | 3.0
    new Long((long) 4)   | 4.0
    new Float(4.5)       | 4.5
    (byte) 1             | 1.0
    (short) 2            | 2.0
    3                    | 3.0
    (long) 4             | 4.0
    4.5f                 | 4.5
  }

  def 'test cant convert to double'(Class srcClass) {
    when:
    def c = new ToDoubleNumberConverter()
    then:
    !c.canConvert(srcClass, Double.class)
    where:
    srcClass     | _
    Double.class | _
    Double.TYPE  | _
  }

  def 'test convert to float'(Object srcValue, Float destValue) {
    when:
    def c = new ToFloatNumberConverter()
    then:
    c.canConvert(srcValue.getClass(), destValue.getClass())
    c.convert(srcValue, destValue.getClass()) == destValue
    where:
    srcValue             | destValue
    new Byte((byte) 1)   | 1.0f
    new Short((short) 2) | 2.0f
    new Integer(3)       | 3.0f
    new Long((long) 4)   | 4.0f
    (byte) 1             | 1.0f
    (short) 2            | 2.0f
    3                    | 3.0f
    (long) 4             | 4.0f
  }

  def 'test cant convert to float'(Class srcClass) {
    when:
    def c = new ToFloatNumberConverter()
    then:
    !c.canConvert(srcClass, Float.class)
    where:
    srcClass     | _
    Float.class  | _
    Float.TYPE   | _
    Double.class | _
    Double.TYPE  | _
  }

  def 'test convert to long'(Object srcValue, Long destValue) {
    when:
    def c = new ToLongNumberConverter()
    then:
    c.canConvert(srcValue.getClass(), destValue.getClass())
    c.convert(srcValue, destValue.getClass()) == destValue
    where:
    srcValue             | destValue
    new Byte((byte) 1)   | 1L
    new Short((short) 2) | 2L
    new Integer(3)       | 3L
    (byte) 1             | 1L
    (short) 2            | 2L
    3                    | 3L
  }

  def 'test cant convert to long'(Class srcClass) {
    when:
    def c = new ToLongNumberConverter()
    then:
    !c.canConvert(srcClass, Long.class)
    where:
    srcClass     | _
    Long.class   | _
    Long.TYPE    | _
    Float.class  | _
    Float.TYPE   | _
    Double.class | _
    Double.TYPE  | _
  }

  def 'test convert to integer'(Object srcValue, Integer destValue) {
    when:
    def c = new ToIntegerNumberConverter()
    then:
    c.canConvert(srcValue.getClass(), destValue.getClass())
    c.convert(srcValue, destValue.getClass()) == destValue
    where:
    srcValue             | destValue
    new Byte((byte) 1)   | 1
    new Short((short) 2) | 2
    (byte) 1             | 1
    (short) 2            | 2
  }

  def 'test cant convert to int'(Class srcClass) {
    when:
    def c = new ToIntegerNumberConverter()
    then:
    !c.canConvert(srcClass, Integer.class)
    where:
    srcClass      | _
    Integer.class | _
    Integer.TYPE  | _
    Long.class    | _
    Long.TYPE     | _
    Float.class   | _
    Float.TYPE    | _
    Double.class  | _
    Double.TYPE   | _
  }

  def 'test convert to short'(Object srcValue, Short destValue) {
    when:
    def c = new ToShortNumberConverter()
    then:
    c.canConvert(srcValue.getClass(), destValue.getClass())
    c.convert(srcValue, destValue.getClass()) == destValue
    where:
    srcValue           | destValue
    new Byte((byte) 1) | 1
    (byte) 1           | 1
  }

  def 'test cant convert to short'(Class srcClass) {
    when:
    def c = new ToShortNumberConverter()
    then:
    !c.canConvert(srcClass, Short.class)
    where:
    srcClass      | _
    Short.class   | _
    Short.TYPE    | _
    Integer.class | _
    Integer.TYPE  | _
    Long.class    | _
    Long.TYPE     | _
    Float.class   | _
    Float.TYPE    | _
    Double.class  | _
    Double.TYPE   | _
  }
}
