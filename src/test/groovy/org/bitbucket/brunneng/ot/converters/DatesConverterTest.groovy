package org.bitbucket.brunneng.ot.converters

import spock.lang.Specification

import java.sql.Time
import java.time.*

class DatesConverterTest extends Specification {
  private static zoneId = ZoneId.systemDefault()
  private static ZoneOffset zoneOffset = zoneId.getRules().getOffset(Instant.now())
  private static long currentTimeSeconds = (long) (System.currentTimeMillis() / 1000)

  def 'test convert dates'(Object date1, Object date2) {
    setup:
    when:
    def c = new DatesConverter(zoneId, zoneId)
    then:
    c.canConvert(date1.getClass(), date2.getClass())
    c.canConvert(date2.getClass(), date1.getClass())
    def date2Converted = c.convert(date1, date2.getClass())
    date2 == date2Converted
    def date1Converted = c.convert(date2Converted, date1.getClass())
    date1 == date1Converted
    where:
    date1                                                          | date2
    createFixedDate()                                              | createFixedTime()
    createFixedLocalTime()                                         | createFixedLocalTime()
    createFixedLocalTime()                                         | createFixedDate()
    createFixedDate()                                              | createFixedDate()
    LocalDateTime.ofEpochSecond(currentTimeSeconds, 0, zoneOffset) | Instant.ofEpochSecond(currentTimeSeconds)
    Instant.ofEpochSecond(currentTimeSeconds)                      | Instant.ofEpochSecond(currentTimeSeconds)
    createFixedLocalTime()                                         | createFixedCalendar()
    createFixedCalendar()                                          | createFixedCalendar()
    createFixedLocalTime()                                         | createFixedZonedDateTime()
    createFixedZonedDateTime()             | createFixedZonedDateTime()
    createFixedZonedDateTime()             | createFixedDate()
    createFixedOffsetDateTime()            | createFixedOffsetDateTime()
    createFixedOffsetDateTime()            | createFixedDate()
    createFixedOffsetDateTime()            | createFixedLocalTime()
    createFixedCalendar()                  | createFixedOffsetDateTime()
    createFixedLocalTimeWithMilliseconds() | createFixedZonedDateTimeWithMilliseconds()
  }

  static private Calendar createFixedCalendar() {
    Calendar res = Calendar.getInstance(TimeZone.getTimeZone(zoneId))
    res.setTime(createDate(2018, 9, 1, 15, 30))
    return res
  }

  static private LocalDateTime createFixedLocalTime() {
    return LocalDateTime.of(2018, 9, 1, 15, 30)
  }

  static private ZonedDateTime createFixedZonedDateTime() {
    return ZonedDateTime.of(createFixedLocalTime(), zoneId)
  }

  static private LocalDateTime createFixedLocalTimeWithMilliseconds() {
    return LocalDateTime.of(2018, 9, 1, 15, 30, 34, 843000000)
  }

  static private ZonedDateTime createFixedZonedDateTimeWithMilliseconds() {
    return ZonedDateTime.of(createFixedLocalTimeWithMilliseconds(), zoneId)
  }

  static private OffsetDateTime createFixedOffsetDateTime() {
    def time = createFixedLocalTime()
    return OffsetDateTime.of(time, zoneId.getRules().getOffset(time.atZone(zoneId).toInstant()))
  }

  static private Date createFixedDate() {
    return createDate(2018, 9, 1, 15, 30)
  }

  static private Time createFixedTime() {
    return new Time(createFixedDate().getTime())
  }

  static Date createDate(int year, int month, int dayOfMonth, int hour, int minute) {
    return new Date(year - 1900, month - 1, dayOfMonth, hour, minute)
  }
}
