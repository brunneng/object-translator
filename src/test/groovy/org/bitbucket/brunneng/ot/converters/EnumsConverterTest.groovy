package org.bitbucket.brunneng.ot.converters

import org.bitbucket.brunneng.ot.exceptions.ConversionException
import spock.lang.Specification

class EnumsConverterTest extends Specification {

  def 'test convert enums'(Object srcObject, Object destObject) {
    when:
    def c = new EnumsConverter()
    then:
    c.canConvert(srcObject.getClass(), destObject.getClass())
    c.convert(srcObject, destObject.getClass()) == destObject
    where:
    srcObject | destObject
    E1.V1     | E2.V1
    E1.V2     | E2.V2
  }

  def 'test cant convert enums'() {
    when:
    def c = new EnumsConverter()
    then:
    !c.canConvert(E1.class, E3.class)
    !c.canConvert(E1.class, Integer.class)
    !c.canConvert(Integer.class, E2.class)
    when:
    c.convert(E1.V3, E2.class)
    then:
    thrown ConversionException
  }

  static enum E1 {
    V1,
    V2,
    V3
  }

  static enum E2 {
    V1,
    V2,
  }

  static enum E3 {
    A1,
    A2,
  }
}
