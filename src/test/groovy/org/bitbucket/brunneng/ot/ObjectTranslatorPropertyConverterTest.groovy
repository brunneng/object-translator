package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.property.PropertyConverter
import org.jetbrains.annotations.NotNull
import spock.lang.Specification

class ObjectTranslatorPropertyConverterTest extends Specification {

  def 'test translation with property converter'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).translationTo(Bean2.class).srcProperty("value").setPropertyConverter(
            new PropertyConverter() {
      @Override
      Object convertValue(@NotNull Object srcValue,
                          @NotNull PropertyDescription srcProperty,
                          @NotNull PropertyDescription destProperty) {
        return Integer.parseInt((String)srcValue)
      }
    })

    def ot = new ObjectTranslator(c)
    def o1 = new Bean1()
    o1.value = "5"
    then:
    def o2 = ot.translate(o1, Bean2.class)
    o2 != null
    o2.value == 5
  };

  static class Bean1 {
    String value
  }

  static class Bean2 {
    int value
  }

}
