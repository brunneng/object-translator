package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.converters.ToLongNumberConverter
import org.bitbucket.brunneng.ot.log.*
import org.bitbucket.brunneng.ot.property.PropertyConverter
import org.jetbrains.annotations.NotNull
import spock.lang.Specification

class ObjectTranslatorTreeLogTest extends Specification {

  def 'test translation of bean fields with log'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.translationTo(Bean2.class).srcProperty("v1").translatesTo(
            "v1", new PropertyConverter() {
      @Override
      Object convertValue(@NotNull Object srcValue,
                          @NotNull PropertyDescription srcProperty,
                          @NotNull PropertyDescription destProperty) {
        return ((String)srcValue).length()
      }
    })
    bean1Config.property("skipped1").skipAsSource()
    c.beanOfClass(Bean2.class).property("skipped2").skipAllChanges()

    bean1Config.translationTo(NestedBean2.class).srcProperty("v1").translatesTo("title")

    def ot = new ObjectTranslator(c)
    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)
    def n1 = o1.nested
    then:
    def result = ot.translateWithLog(o1, Bean2.class)
    def o2 = result.destValue
    def log = result.logRoot
    System.out.println(log)

    def setPropertyEntries = log.findEntriesOfType(SetPropertyValueLogEntry.class)
    setPropertyEntries.find{ sp -> sp.propertyDescription.propertyName == "title" }.destValue == n1.title
    setPropertyEntries.find{ sp -> sp.propertyDescription.propertyName == "nested" }.destValue == o2.nested
    setPropertyEntries.find{ sp -> sp.propertyDescription.propertyName == "v1" }.destValue == o2.v1

    def typeConverterEntries = log.findEntriesOfType(TypeConverterUsedLogEntry.class)
    typeConverterEntries.size() == 1
    def typeConvertEntry = typeConverterEntries.get(0)
    typeConvertEntry.typeConverter instanceof ToLongNumberConverter
    typeConvertEntry.conversionResult == o2.count

    def mapBeanLogEntries = log.findEntriesOfType(MapBeanLogEntry.class)
    mapBeanLogEntries.size() == 2
    mapBeanLogEntries.find { mb -> mb.srcBean == o1 }.destBean == o2
    mapBeanLogEntries.find { mb -> mb.srcBean == o1.nested }.destBean == o2.nested

    def propertyConverterEntries = log.findEntriesOfType(PropertyConverterUsedLogEntry.class)
    propertyConverterEntries.size() == 1
    def propertyConverterEntry = propertyConverterEntries.get(0)
    propertyConverterEntry.srcPropertyDescription.propertyName == "v1"
    propertyConverterEntry.destPropertyDescription.propertyName == "v1"
    propertyConverterEntry.destValue == o1.v1.length()

    def srcPropertySkippedEntries = log.findEntriesOfType(SourcePropertySkippedLogEntry.class)
    srcPropertySkippedEntries.find{ ps ->
      ps.property.ownerBeanClass == Bean1.class && ps.property.propertyName == "skipped1" } != null

    def propertyChangeSkippedEntries = log.findEntriesOfType(PropertyChangeSkippedLogEntry.class)
    propertyChangeSkippedEntries.find{ ps ->
      ps.property.ownerBeanClass == Bean2.class && ps.property.propertyName == "skipped2" } != null

    def mapCollectionEntries = log.findEntriesOfType(MapCollectionLogEntry.class)
    def mapCollectionEntry = mapCollectionEntries.get(0)
    mapCollectionEntry.srcCollection == o1.collection1

    def setCollectionValuesEntries = log.findEntriesOfType(SetCollectionValuesLogEntry.class)
    def setCollectionValuesEntry = setCollectionValuesEntries.get(0)
    setCollectionValuesEntry.values == o2.collection1

    def mapMapsEntries = log.findEntriesOfType(MapMapsLogEntry.class)
    def mapMapsEntry = mapMapsEntries.get(0)
    mapMapsEntry.srcMap == o1.map1

    def setMapContentEntries = log.findEntriesOfType(SetMapContentLogEntry.class)
    def setMapContentEntry = setMapContentEntries.get(0)
    setMapContentEntry.content == o2.map1
  }

  static class Bean1 {
    int count
    String v1
    String skipped1
    String skipped2
    NestedBean1 nested
    List<Integer> collection1
    Map<String, Integer> map1
  }

  static class Bean2 {
    Long count
    Integer v1
    String skipped1
    String skipped2
    NestedBean2 nested
    List<Integer> collection1
    Map<String, Integer> map1
  }

  static class NestedBean1 {
    String title
  }

  static class NestedBean2 {
    String title
  }

  static class RecursionBean {
    String title
    RecursionBean child
  }
}
