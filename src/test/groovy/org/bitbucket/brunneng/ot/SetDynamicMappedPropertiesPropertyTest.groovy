package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.exceptions.ConfigurationException
import spock.lang.Specification

class SetDynamicMappedPropertiesPropertyTest extends Specification {

  def 'test markAsDynamicMappedPropertiesCollection'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.property("collection2").markAsDynamicMappedPropertiesCollection()
    then:
    bean1Config.getDynamicMappedPropertiesCollection$object_translator().name == "collection2"
  }

  def 'test markAsDynamicMappedPropertiesCollection in parent bean'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(AbstractBean.class)
    bean1Config.property("collection1").markAsDynamicMappedPropertiesCollection()
    then:
    c.beanOfClass(Bean1.class).getDynamicMappedPropertiesCollection$object_translator().name == "collection1"
  }

  def 'test markAsDynamicMappedPropertiesCollection more then once'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.property("collection2").markAsDynamicMappedPropertiesCollection()
    bean1Config.property("collection2").markAsDynamicMappedPropertiesCollection()
    then:
    thrown ConfigurationException
  }

  def 'test markAsDynamicMappedPropertiesCollection not a collection'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.property("value").markAsDynamicMappedPropertiesCollection()
    then:
    thrown ConfigurationException
  }

  def 'test markAsDynamicMappedPropertiesCollection not a string collection'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.property("collection3").markAsDynamicMappedPropertiesCollection()
    then:
    thrown ConfigurationException
  }

  static class AbstractBean {
    int id
    List<String> collection1
  }

  static class Bean1 extends AbstractBean {
    List<String> collection2
    List<Integer> collection3
    String value
  }
}
