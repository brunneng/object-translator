package org.bitbucket.brunneng.ot.controllers

import spock.lang.Specification

class ArrayCollectionControllerTest extends Specification {

  def 'test isCollectionTypeSupported'() {
    when:
    def c = new ArrayCollectionController()
    then:
    c.isCollectionTypeSupported(testClass) == matched
    where:
    testClass           | matched
    new int[0].class    | true
    new int[0][0].class | true
    List.class          | false
  }

  def 'test getCollectionValues'(Object collection, List<Object> expectedValues) {
    when:
    def c = new ArrayCollectionController()
    then:
    c.getCollectionValues(collection) == expectedValues
    where:
    collection             | expectedValues
    [] as Integer[]        | []
    [1, 2, 3] as Integer[] | [1, 2, 3]
  }

  def 'test findCollectionComponentType'(Object collection, Class componentType) {
    when:
    def c = new ArrayCollectionController()
    then:
    c.findCollectionComponentType(collection.getClass(), null) == componentType
    where:
    collection                | componentType
    [] as Integer[]           | Integer.class
    [[1], [2], [3]] as List[] | List.class
  }

  def 'test setValues'(Object collection, List<Object> valuesToSet, boolean sameCollectionObject) {
    when:
    def c = new ArrayCollectionController()
    then:
    def modifiedCollection = c.setValues(collection, valuesToSet)
    c.getCollectionValues(modifiedCollection) == valuesToSet
    (collection == modifiedCollection) == sameCollectionObject
    where:
    collection             | valuesToSet | sameCollectionObject
    [] as Integer[]        | [1, 2]      | false
    [1, 2, 3] as Integer[] | [3, 2, 3]   | true
  }
}
