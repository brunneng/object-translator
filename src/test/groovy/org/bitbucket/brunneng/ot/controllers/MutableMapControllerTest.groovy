package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.ot.Configuration
import org.bitbucket.brunneng.ot.util.TypeReference
import spock.lang.Specification

import java.lang.reflect.Type

class MutableMapControllerTest extends Specification {

  static Configuration.Bean beanConfig = new Configuration().beanOfClass(BeanWithMap.class)

  def 'test isMapTypeSupported'() {
    when:
    def c = new MutableMapController()
    then:
    c.isMapTypeSupported(testClass) == matched
    where:
    testClass           | matched
    new int[0].class    | false
    new int[0][0].class | false
    Collection.class    | false
    List.class          | false
    Map.class           | true
    HashMap.class       | true
    TreeMap.class       | true
  }

  def 'test getMapValues'(Object map, Map<Object, Object> expectedValues) {
    when:
    def c = new MutableMapController()
    then:
    c.getMapContent(map) == expectedValues
    where:
    map                      | expectedValues
    [:]                      | [:]
    [1: "a", 2: "b", 3: "c"] | [1: "a", 2: "b", 3: "c"]
  }

  def 'test findMapKeyTypeByAccessor'(Object map, AccessorValueType typeInfo, Class keyType) {
    when:
    def c = new MutableMapController()
    then:
    c.findMapKeyTypeByAccessor(map.getClass(), typeInfo) == keyType
    where:
    map                      | typeInfo                                                 | keyType
    []                       | null                                                     | null
    [1: "a", 2: "b", 3: "c"] | null                                                     | null
    []                       | beanConfig.property("map").getter().getAccessorValueType() | String.class
  }

  def 'test findMapValueTypeByAccessor'(Object map, AccessorValueType typeInfo, Type expectedType) {
    when:
    def c = new MutableMapController()
    then:
    c.findMapValueTypeByAccessor(map.getClass(), typeInfo) == expectedType
    where:
    map                      | typeInfo                                                 | expectedType
    []                       | null                                                     | null
    [1: "a", 2: "b", 3: "c"] | null                                                     | null
    []                       | beanConfig.property("map").getter().getAccessorValueType() | Integer.class
  }

  def 'test findMapValueType'(Object map, Type mapType, Type expectedType) {
    when:
    def c = new MutableMapController()
    then:
    c.findMapValueType(map.getClass(), mapType) == expectedType
    where:
    map                      | mapType                                                 | expectedType
    []                       | Map.class                                               | null
    [1: "a", 2: "b", 3: "c"] | new TypeReference<Map<Integer, String>>() {}.getGenericParameterType() | String.class
  }

  def 'test setContent'(Object map, Map<Object, Object> valuesToSet) {
    when:
    def c = new MutableMapController()
    then:
    def modifiedMap = c.setContent(map, valuesToSet)
    c.getMapContent(modifiedMap) == valuesToSet
    map == modifiedMap
    where:
    map                      | valuesToSet
    [:]                      | [1: "a", 2: "b"]
    [1: "a", 2: "b", 3: "c"] | [3: "c", 1: "a", 2: "b"]
  }

  static class BeanWithMap {
    Map<String, Integer> map
  }
}
