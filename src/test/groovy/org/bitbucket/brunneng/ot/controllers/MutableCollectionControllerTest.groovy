package org.bitbucket.brunneng.ot.controllers

import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.ot.Configuration
import org.bitbucket.brunneng.ot.util.TypeReference
import spock.lang.Specification

import java.lang.reflect.Type

class MutableCollectionControllerTest extends Specification {

  static Configuration.Bean beanConfig = new Configuration().beanOfClass(BeanWithCollection.class)

  def 'test isCollectionTypeSupported'() {
    when:
    def c = new MutableCollectionController()
    then:
    c.isCollectionTypeSupported(testClass) == matched
    where:
    testClass           | matched
    new int[0].class    | false
    new int[0][0].class | false
    Collection.class    | true
    List.class          | true
    ArrayList.class     | true
    Set.class           | true
  }

  def 'test getCollectionValues'(Object collection, List<Object> expectedValues) {
    when:
    def c = new MutableCollectionController()
    then:
    c.getCollectionValues(collection) == expectedValues
    where:
    collection | expectedValues
    []         | []
    [1, 2, 3]  | [1, 2, 3]
  }

  def 'test findCollectionComponentTypeByAccessor'(Object collection, AccessorValueType typeInfo,
                                                   Type expectedComponentType) {
    when:
    def c = new MutableCollectionController()
    then:
    c.findCollectionComponentTypeByAccessor(collection.getClass(), typeInfo) == expectedComponentType
    where:
    collection | typeInfo                                                    | expectedComponentType
    []         | null                                                        | null
    [1, 2, 3]  | null                                                        | null
    []         | beanConfig.property("values").getter().getAccessorValueType() | String.class
  }

  def 'test findCollectionComponentType'(Object collection, Type collectionType,
                                         Type expectedComponentType) {
    when:
    def c = new MutableCollectionController()
    then:
    c.findCollectionComponentType(collection.getClass(), collectionType) == expectedComponentType
    where:
    collection | collectionType                                                    | expectedComponentType
    []         | List.class                                                        | null
    []         | new TypeReference<List<Integer>>(){}.getGenericParameterType()    | Integer.class
  }

  def 'test setValues'(Object collection, List<Object> valuesToSet) {
    when:
    def c = new MutableCollectionController()
    then:
    def modifiedCollection = c.setValues(collection, valuesToSet)
    c.getCollectionValues(modifiedCollection) == valuesToSet
    collection == modifiedCollection
    where:
    collection | valuesToSet
    []         | [1, 2]
    [1, 2, 3]  | [3, 2, 3]
  }

  static class BeanWithCollection {
    List<String> values
  }
}
