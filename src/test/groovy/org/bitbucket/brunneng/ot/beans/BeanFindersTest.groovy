package org.bitbucket.brunneng.ot.beans

import org.bitbucket.brunneng.ot.Configuration
import org.bitbucket.brunneng.ot.ObjectTranslator
import org.bitbucket.brunneng.ot.log.BeanFinderResultLogEntry
import org.bitbucket.brunneng.ot.log.MappingOfDestPathSkippedLogEntry
import org.bitbucket.brunneng.ot.log.SetPropertyValueLogEntry
import spock.lang.Specification

class BeanFindersTest extends Specification {

  def 'test translation collection with bean finder'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new CollectionFields1()
    o1.setValues4([new Bean1(1, "title1"), new Bean1(2, "title2")])
    then:
    def o2 = ot.translate(o1, CollectionFields2.class)
    o2.values4 == [new Bean2(1, "title1"), new Bean2(2, "title2")]
    o2.values4[0].is(beansMap[1])
  }

  def 'test mapping collection with bean finder'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa"), 2:new Bean2(2, "bbb")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new CollectionFields1()
    o1.setValues4([new Bean1(1, "title1"), new Bean1(2, "title2")])

    def o2 = new CollectionFields2()
    def b22 = new Bean2(2, "-")
    o2.setValues4([b22, new Bean2(3, "-")])

    then:
    ot.mapBean(o1, o2)
    o2.values4 == [new Bean2(1, "title1"), new Bean2(2, "title2")]
    o2.values4[0].is(beansMap[1])
    o2.values4[1].is(b22)
  }

  def 'test bean property translation with bean finder'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new ParentBean1()
    o1.setChild1(new Bean1(1, "title1"))
    o1.setChild2(new Bean1(2, "title2"))
    then:
    def o2 = ot.translate(o1, ParentBean2.class)
    o2.child1 == new Bean2(1, "title1")
    o2.child2 == new Bean2(2, "title2")
    o2.child1.is(beansMap[1])
  }

  def 'test bean property mapping with bean finder'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new ParentBean1()
    o1.setChild1(new Bean1(1, "title1"))

    def o2 = new ParentBean2()
    o2.setChild1(new Bean2(2, "xxx"))
    then:
    ot.mapBean(o1, o2)
    o2.child1 == new Bean2(1, "title1")
    o2.child1.is(beansMap[1])
  }

  def 'test bean translation with bean finder'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(AbstractBean.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new Bean1(1, "title1")
    then:
    def o2 = ot.translate(o1, Bean2.class)
    o2 == new Bean2(1, "title1")
    o2.is(beansMap[1])
  }

  def 'test bean map with bean finder'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new Bean1(1, "title1")

    def o2 = new Bean2(1, "bbb")
    then:
    ot.mapBean(o1, o2)
    o2 == new Bean2(1, "title1")
    !o2.is(beansMap[1]) // bean finder is not used, because we already have destination object
  }

  def 'test bean translation with bean finder, different id types'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1L:new Bean2Long(1L, "aaa")]
    c.beanOfClass(Bean2Long.class).setBeanFinder(new MapBeanFinder(Bean2Long.class, beansMap))

    def ot = new ObjectTranslator(c)
    def o1 = new Bean1(1, "title1")
    then:
    def o2 = ot.translate(o1, Bean2Long.class)
    o2 == new Bean2Long(1L, "title1")
    o2.is(beansMap[1L])
  }

  def 'test map id property on bean'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))
    c.beanOfClass(BeanWithId1.class).translationTo(ParentBean2.class).srcProperty("child1Id")
            .translatesTo("child1.id");

    def ot = new ObjectTranslator(c)
    def o1 = new BeanWithId1(1, new TitleContent("bbb"))
    then:
    def o2 = ot.translate(o1, ParentBean2.class)
    o2.child1.is(beansMap[1])
    o2.child1.title == o1.child1.title
  }

  def 'test map null id property on bean'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    c.beanOfClass(BeanWithId2.class).translationTo(ParentBean2.class).srcProperty("child1Id")
            .translatesTo("child1.id");

    def o1 = new BeanWithId2(null)
    then:
    def res = ot.translateWithLog(o1, ParentBean2.class)
    def logEntry = res.logRoot.findEntriesOfType(MappingOfDestPathSkippedLogEntry.class).get(0)
    def child1IdDestProperty = c.beanOfClass(BeanWithId2.class).translationTo(ParentBean2.class).destProperty(
            "child1.id")
    logEntry.property == child1IdDestProperty

    def o2 = res.destValue
    o2.child1 == null
  }

  def 'test map id property on bean id'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa"), 2:new Bean2(2, "bbb")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    c.beanOfClass(BeanWithId2.class).translationTo(ParentBean2.class).srcProperty("child1Id")
            .translatesTo("child1.id");

    def target = new ParentBean2();
    target.setChild1(beansMap.get(1))

    def o1 = new BeanWithId2(2)
    then:
    ot.mapBean(o1, target)

    target.child1 == new Bean2(2, "bbb")
    target.child1.is(beansMap.get(2))
  }

  def 'test map id property on bean id with log'() {
    when:
    def c = new Configuration()
    c.beanOfClass(AbstractBean.class).setIdentifierProperty("id")

    def beansMap = [1:new Bean2(1, "aaa"), 2:new Bean2(2, "bbb")]
    c.beanOfClass(Bean2.class).setBeanFinder(new MapBeanFinder(Bean2.class, beansMap))

    def ot = new ObjectTranslator(c)
    c.beanOfClass(BeanWithId2.class).translationTo(ParentBean2.class).srcProperty("child1Id")
            .translatesTo("child1.id");

    def target = new ParentBean2();
    target.setChild1(beansMap.get(1))

    def o1 = new BeanWithId2(2)
    then:
    def log = ot.mapBeanWithLog(o1, target)

    target.child1 == new Bean2(2, "bbb")
    target.child1.is(beansMap.get(2))

    log.findEntriesOfType(BeanFinderResultLogEntry.class).size() == 1
    def entries = log.findEntriesOfType(SetPropertyValueLogEntry.class)
    entries.size() == 1
    ((SetPropertyValueLogEntry)entries.get(0)).propertyDescription.propertyName == "child1"
  }

  static class CollectionFields1 {
    List<Bean1> values4
  }

  static class CollectionFields2 {
    List<Bean2> values4
  }

  static class MapBeanFinder<T, I> implements BeanFinder<T, I> {
    private Class<?> expectedBeanClass;
    private Map<I, T> beansMap;

    MapBeanFinder(Class<?> expectedBeanClass, Map<I, T> beansMap) {
      this.expectedBeanClass = expectedBeanClass
      this.beansMap = beansMap
    }

    T findBean(Class<?> beanClass, I identifier) {
      assert beanClass == expectedBeanClass
      return beansMap[identifier]
    }
  }

  abstract static class AbstractBean<I> {
    I id

    AbstractBean() {
    }

    AbstractBean(I id) {
      this.id = id
    }
  }

  static class Bean1 extends AbstractBean<Integer> {
    String title

    Bean1() {
    }

    Bean1(int id, String title) {
      super(id)
      this.title = title
    }
  }

  static class Bean2 extends AbstractBean<Integer> {
    String title

    Bean2() {
    }

    Bean2(int id, String title) {
      super(id)
      this.title = title
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      Bean2 bean2 = (Bean2) o

      if (id != bean2.id) return false
      if (title != bean2.title) return false

      return true
    }

    int hashCode() {
      int result
      result = id != null ? id : 0
      result = 31 * result + (title != null ? title.hashCode() : 0)
      return result
    }
  }

  static class ParentBean1 {
    Bean1 child1
    Bean1 child2
  }

  static class ParentBean2 {
    Bean2 child1
    Bean2 child2
  }

  static class Bean2Long extends AbstractBean<Long> {
    String title

    Bean2Long() {
    }

    Bean2Long(long id, String title) {
      super(id)
      this.title = title
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      Bean2Long bean2Long = (Bean2Long) o

      if (title != bean2Long.title) return false

      return true
    }

    int hashCode() {
      return (title != null ? title.hashCode() : 0)
    }
  }

  static class BeanWithId1 {
    Integer child1Id
    TitleContent child1;

    BeanWithId1(Integer child1Id, TitleContent child1) {
      this.child1Id = child1Id
      this.child1 = child1
    }
  }

  static class BeanWithId2 {
    Integer child1Id

    BeanWithId2(Integer child1Id) {
      this.child1Id = child1Id
    }
  }

  static class TitleContent {
    String title;

    TitleContent(String title) {
      this.title = title
    }
  }

}
