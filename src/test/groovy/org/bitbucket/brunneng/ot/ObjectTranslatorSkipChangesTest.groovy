package org.bitbucket.brunneng.ot


import spock.lang.Specification

import java.time.LocalDateTime

class ObjectTranslatorSkipChangesTest extends Specification {

  def 'test skip property changes'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    def o1 = new Bean1()
    o1.title = "test"
    o1.age = 5
    o1.note = "notes1"
    o1.v1 = "v1"
    o1.child1 = new ChildBean1("title")
    o1.collection1 = [1, 2]
    o1.collection2 = [1, 1, 2, null, null, 3]
    o1.collection3 = [new ChildBean1("aaa")]
    o1.collection4 = ["abc"]
    o1.collection5 = [new ChildBean2(1 ,"bbb")]
    o1.collection6 = [1, 2]
    o1.map1 = ["a":1, "b":2]
    o1.map2 = ["a":1, "b":1, "c":2, "d":null, "e":null, "f":3]
    o1.map3 = ["a":new ChildBean1("aaa")]
    o1.map4 = ["a":"abc"]
    o1.map5 = ["a":new ChildBean2(1 ,"bbb")]
    o1.map6 = ["a":1, "b":2]


    c.beanOfClass(ChildBean2.class).setIdentifierProperty("id")
    def beanConfig = c.beanOfClass(Bean2.class)
    beanConfig.property("age").skipChange(Configuration.ChangeType.SET_NOT_NULL)
    beanConfig.property("note").skipAllChanges()
    beanConfig.property("v1").skipChange(Configuration.ChangeType.SET_NOT_NULL)
    beanConfig.property("child1").skipChange(Configuration.ChangeType.BEAN_MAP)
    beanConfig.property("collection1").skipChange(Configuration.ChangeType.COLLECTION_ADD)
    beanConfig.property("collection2").skipChange(Configuration.ChangeType.COLLECTION_REMOVE)
    beanConfig.property("collection3").skipChange(Configuration.ChangeType.BEAN_MAP)
    beanConfig.property("collection4").skipChanges(Configuration.ChangeType.COLLECTION_ADD, Configuration.ChangeType.COLLECTION_REMOVE)
    beanConfig.property("collection5").skipChanges(Configuration.ChangeType.COLLECTION_ADD, Configuration.ChangeType.COLLECTION_REMOVE)
    beanConfig.property("collection6").skipChanges(Configuration.ChangeType.SET_NULL, Configuration.ChangeType.SET_NOT_NULL)
    beanConfig.property("map1").skipChange(Configuration.ChangeType.MAP_ADD)
    beanConfig.property("map2").skipChange(Configuration.ChangeType.MAP_REMOVE)
    beanConfig.property("map3").skipChange(Configuration.ChangeType.BEAN_MAP)
    beanConfig.property("map4").skipChanges(Configuration.ChangeType.MAP_ADD, Configuration.ChangeType.MAP_REMOVE,
            Configuration.ChangeType.MAP_REPLACE_VALUE)
    beanConfig.property("map5").skipChanges(Configuration.ChangeType.MAP_ADD, Configuration.ChangeType.MAP_REMOVE,
            Configuration.ChangeType.MAP_REPLACE_VALUE)
    beanConfig.property("map6").skipChanges(Configuration.ChangeType.SET_NULL, Configuration.ChangeType.SET_NOT_NULL,
            Configuration.ChangeType.MAP_REPLACE_VALUE)

    def o2 = ot.translate(o1, Bean2.class)
    then:
    o1.title == o2.title
    o2.age == null
    o2.note == null
    o2.child1 == new ChildBean1(null)
    o2.collection1 == []
    o2.collection2 == [1, 1, 2, null, null, 3]
    o2.collection3 == [new ChildBean1(null)]
    o2.collection4 == []
    o2.collection5 == []
    o2.collection6 == null
    o2.map1 == [:]
    o2.map2 == ["a":1, "b":1, "c":2, "d":null, "e":null, "f":3]
    o2.map3 == ["a":new ChildBean1(null)]
    o2.map4 == [:]
    o2.map5 == [:]
    o2.map6 == null
    when:
    o2.collection1 = [1, 3]
    o1.collection2 = [0, 1, 4]
    o2.collection4 = [1L]
    o2.collection5 = [new ChildBean2(1 ,null)]
    o2.collection6 = []
    o2.map1 = ["a":null, "c":3]
    o1.map2 = ["0":0, "a":2, "4":4]
    o2.map4 = ["a":1L]
    o2.map5 = ["a":new ChildBean2(1 ,null)]
    o2.map6 = ["a":11, "b":22]
    ot.mapBean(o1, o2)
    then:
    o1.title == o2.title
    o2.age == null
    o2.note == null
    o2.child1 == new ChildBean1(null)
    o2.collection1 == [1]
    o2.collection2 == [1, 0, 2, null, null, 3, 1, 4]
    o2.collection3 == [new ChildBean1(null)]
    o2.collection4 == [1L]
    o2.collection5 == [new ChildBean2(1 ,"bbb")]
    o2.collection6 == [1, 2]
    o2.map1 == ["a":1]
    o2.map2 == ["0":0, "a":2, "4":4, "b":1, "c":2, "d":null, "e":null, "f":3]
    o2.map3 == ["a":new ChildBean1(null)]
    o2.map4 == ["a":1L]
    o2.map5 == ["a":new ChildBean2(1 ,"bbb")]
    o2.map6 == ["a":11, "b":22]
  }

  def 'test map only not null properties'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    def o1 = new Bean1()
    o1.title = null
    o1.age = 5

    def o2 = new Bean1()
    o2.title = "test"
    o2.age = 1

    c.beanOfClass(Bean1.class).translationTo(Bean1.class).mapOnlyNotNullProperties()
    ot.mapBean(o1, o2)

    then:
    o2.title == "test"
    o2.age == 5
  }

  def 'test map only not null properties in abstract class'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    def o1 = new Bean3()
    o1.id = 5
    o1.value = "aaa"

    def patch = new Bean3()

    c.beanOfClass(Bean3.class).translationTo(AbstractBean.class).mapOnlyNotNullProperties()
    ot.mapBean(patch, o1)

    then:
    o1.id == 5
    o1.value == null
  }

  static class Bean1 {
    String title
    int age
    String note
    String v1
    ChildBean1 child1
    List<Integer> collection1
    List<Integer> collection2
    List<ChildBean1> collection3
    List<String> collection4
    List<ChildBean2> collection5
    List<Integer> collection6
    Map<String, Integer> map1
    Map<String, Integer> map2
    Map<String, ChildBean1> map3
    Map<String, String> map4
    Map<String, ChildBean2> map5
    Map<String, Integer> map6
  }

  static class Bean2 {
    String title
    Long age
    LocalDateTime note // non convertable type
    Long v1 // non convertable type
    ChildBean1 child1
    List<Integer> collection1
    List<Integer> collection2
    List<ChildBean1> collection3
    List<Long> collection4 // non convertable type
    List<ChildBean2> collection5
    List<Integer> collection6
    Map<String, Integer> map1
    Map<String, Integer> map2
    Map<String, ChildBean1> map3
    Map<String, Long> map4 // non convertable type
    Map<String, ChildBean2> map5
    Map<String, Integer> map6
  }

  static class ChildBean1 {
    String title;

    ChildBean1() {
    }

    ChildBean1(String title) {
      this.title = title
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      ChildBean1 that = (ChildBean1) o

      if (title != that.title) return false

      return true
    }

    int hashCode() {
      return (title != null ? title.hashCode() : 0)
    }
  }

  static class ChildBean2 {
    Integer id
    String title

    ChildBean2() {
    }

    ChildBean2(Integer id, String title) {
      this.id = id
      this.title = title
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      ChildBean2 that = (ChildBean2) o

      if (id != that.id) return false
      if (title != that.title) return false

      return true
    }

    int hashCode() {
      int result
      result = (id != null ? id.hashCode() : 0)
      result = 31 * result + (title != null ? title.hashCode() : 0)
      return result
    }


    @Override
    public String toString() {
      return new StringJoiner(", ", ChildBean2.class.getSimpleName() + "[", "]")
              .add("id=" + id)
              .add("title='" + title + "'")
              .toString();
    }
  }

  static abstract class AbstractBean {
    Long id
  }

  static class Bean3 extends AbstractBean {
    String value
  }
}
