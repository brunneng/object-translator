package org.bitbucket.brunneng.ot.performance;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import net.sf.brunneng.jom.IMergingContext;
import net.sf.brunneng.jom.MergingContext;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.RandomizerUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class PerformanceBenchmark {

   private Order sourceOrder;
   private ObjectTranslator objectTranslator;
   private IMergingContext mergingContext;
   private ModelMapper modelMapper;
   private MapperFacade orikaMapperFacade;

   public static void main(String[] args) throws RunnerException {
      Options opt = new OptionsBuilder().include(PerformanceBenchmark.class.getSimpleName())
            .forks(1)
            .threads(10)
            .warmupIterations(2)
            .measurementIterations(2)
            .addProfiler( StackProfiler.class , "lines=20")
            .build();

      new Runner(opt).run();
   }

   @Setup
   public void setup() {
      sourceOrder = buildOrder();

      configureObjectTranslator();
      //configureJavaObjectMerger();
      //configureModelMapper();
      //configureOrikaMapperFacade();
   }

   @BeforeEach
   public void beforeTest() {
      sourceOrder = buildOrder();
   }

   @Test
   public void validateObjectTranslator() {
      configureObjectTranslator();
      validateMapping(objectTranslator.translate(sourceOrder, OrderDTO.class));
   }

   @Disabled
   @Test
   public void validateJavaObjectMerger() {
      configureJavaObjectMerger();
      validateMapping(mergingContext.map(sourceOrder, OrderDTO.class));
   }

   @Disabled
   @Test
   public void validateModelMapper() {
      configureModelMapper();
      validateMapping(modelMapper.map(sourceOrder, OrderDTO.class));
   }

   @Disabled
   @Test
   public void validateOrikaMapperFacade() {
      configureOrikaMapperFacade();
      validateMapping(orikaMapperFacade.map(sourceOrder, OrderDTO.class));
   }

   private OrderDTO validateMapping(OrderDTO dto) {
      final Customer customer = sourceOrder.getCustomer();
      assertEquals(customer.getName(), dto.getCustomerName());

      final Address billingAddress = customer.getBillingAddress();
      assertEquals(billingAddress.getStreet(), dto.getBillingStreetAddress());
      assertEquals(billingAddress.getCity(), dto.getBillingCity());

      final Address shippingAddress = customer.getShippingAddress();
      assertEquals(shippingAddress.getStreet(), dto.getShippingStreetAddress());
      assertEquals(shippingAddress.getCity(), dto.getShippingCity());

      assertEquals(sourceOrder.getProducts().size(), dto.getProducts().size());
      int i = 0;
      for (Product p : sourceOrder.getProducts()) {
         ProductDTO pDto = dto.getProducts().get(i++);
         assertEquals(p.getName(), pDto.getName());
      }
      return dto;
   }

   private void configureObjectTranslator() {
      Configuration c = new Configuration();
      Configuration.Translation t = c.beanOfClass(Order.class).translationTo(OrderDTO.class);
      t.srcProperty("customer.name").translatesTo("customerName");
      t.srcProperty("customer.billingAddress.street").translatesTo("billingStreetAddress");
      t.srcProperty("customer.billingAddress.city").translatesTo("billingCity");
      t.srcProperty("customer.shippingAddress.street").translatesTo("shippingStreetAddress");
      t.srcProperty("customer.shippingAddress.city").translatesTo("shippingCity");

      objectTranslator = new ObjectTranslator(c);
   }

   private void configureJavaObjectMerger() {
      mergingContext = new MergingContext();
      mergingContext.forBeansOfClass(OrderDTO.class).property("customerName").mappedTo("customer.name");
      mergingContext.forBeansOfClass(OrderDTO.class).property("billingStreetAddress").mappedTo("customer.billingAddress.street");
      mergingContext.forBeansOfClass(OrderDTO.class).property("billingCity").mappedTo("customer.billingAddress.city");
      mergingContext.forBeansOfClass(OrderDTO.class).property("shippingStreetAddress").mappedTo("customer.shippingAddress.street");
      mergingContext.forBeansOfClass(OrderDTO.class).property("shippingCity").mappedTo("customer.shippingAddress.city");
   }

   private void configureModelMapper() {
      modelMapper = new ModelMapper();
      modelMapper.addMappings(new PropertyMap<Order, OrderDTO>() {
         @Override
         protected void configure() {
            map().setBillingStreetAddress(source.getCustomer().getBillingAddress().getStreet());
            map().setBillingCity(source.getCustomer().getBillingAddress().getCity());
            map().setShippingStreetAddress(source.getCustomer().getShippingAddress().getStreet());
            map().setShippingCity(source.getCustomer().getShippingAddress().getCity());
         }
      });
   }

   private void configureOrikaMapperFacade() {
      MapperFactory factory = new DefaultMapperFactory.Builder().build();
      factory.registerClassMap(ClassMapBuilder.map(Order.class, OrderDTO.class)
            .field("customer.name", "customerName")
            .field("customer.billingAddress.street",
                  "billingStreetAddress")
            .field("customer.billingAddress.city", "billingCity")
            .field("customer.shippingAddress.street",
                  "shippingStreetAddress")
            .field("customer.shippingAddress.city",
                  "shippingCity")
            .field("products", "products")
            .toClassMap());
      orikaMapperFacade = factory.getMapperFacade();
   }

   @Benchmark
   @OperationsPerInvocation(10000)
   public OrderDTO benchmarkObjectTranslator() {
      return objectTranslator.translate(sourceOrder, OrderDTO.class);
      //return validateMapping(objectTranslator.translate(sourceOrder, OrderDTO.class));
   }

//   @Benchmark
//   @OperationsPerInvocation(10000)
//   public OrderDTO benchmarkJavaObjectMerger() {
//      return mergingContext.map(sourceOrder, OrderDTO.class);
//   }

//   @Benchmark
//   @OperationsPerInvocation(10000)
//   public OrderDTO benchmarkModelMapper() {
//      return modelMapper.map(sourceOrder, OrderDTO.class);
//   }
//
//   @Benchmark
//   @OperationsPerInvocation(10000)
//   public OrderDTO benchmarkOrika() {
//      return orikaMapperFacade.map(sourceOrder, OrderDTO.class);
//   }

   static Order buildOrder() {
      return RandomizerUtils.generateObject(Order.class);
   }

   public static class Order {
      private Customer customer;
      private List<Product> products;

      public Customer getCustomer() {
         return customer;
      }
      public void setCustomer(Customer customer) {
         this.customer = customer;
      }

      public List<Product> getProducts() {
         return products;
      }
      public void setProducts(List<Product> products) {
         this.products = products;
      }
   }

   public static class Customer {
      private String name;
      private Address shippingAddress;
      private Address billingAddress;

      public String getName() {
         return name;
      }
      public void setName(String name) {
         this.name = name;
      }

      public Address getShippingAddress() {
         return shippingAddress;
      }
      public void setShippingAddress(Address shippingAddress) {
         this.shippingAddress = shippingAddress;
      }

      public Address getBillingAddress() {
         return billingAddress;
      }
      public void setBillingAddress(Address billingAddress) {
         this.billingAddress = billingAddress;
      }
   }

   public static class Product {
      private String name;

      Product(String name) {
         this.name = name;
      }

      public String getName() {
         return name;
      }
      public void setName(String name) {
         this.name = name;
      }
   }

   public static class Address {
      private String street;
      private String city;

      public String getStreet() {
         return street;
      }
      public void setStreet(String street) {
         this.street = street;
      }

      public String getCity() {
         return city;
      }
      public void setCity(String city) {
         this.city = city;
      }
   }

   public static class OrderDTO {
      private List<ProductDTO> products;
      private String customerName;
      private String shippingStreetAddress;
      private String shippingCity;
      private String billingStreetAddress;
      private String billingCity;

      public List<ProductDTO> getProducts() {
         return products;
      }

      public void setProducts(List<ProductDTO> products) {
         this.products = products;
      }

      public String getCustomerName() {
         return customerName;
      }

      public void setCustomerName(String customerName) {
         this.customerName = customerName;
      }

      public String getShippingStreetAddress() {
         return shippingStreetAddress;
      }

      public void setShippingStreetAddress(String shippingStreetAddress) {
         this.shippingStreetAddress = shippingStreetAddress;
      }

      public String getShippingCity() {
         return shippingCity;
      }

      public void setShippingCity(String shippingCity) {
         this.shippingCity = shippingCity;
      }

      public String getBillingStreetAddress() {
         return billingStreetAddress;
      }

      public void setBillingStreetAddress(String billingStreetAddress) {
         this.billingStreetAddress = billingStreetAddress;
      }

      public String getBillingCity() {
         return billingCity;
      }

      public void setBillingCity(String billingCity) {
         this.billingCity = billingCity;
      }
   }

   public static class ProductDTO {
      private String name;

      public String getName() {
         return name;
      }

      public void setName(String name) {
         this.name = name;
      }
   }
}
