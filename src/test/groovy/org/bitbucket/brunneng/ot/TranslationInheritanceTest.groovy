package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.property.PropertyConverter
import org.jetbrains.annotations.NotNull
import spock.lang.Specification

/**
 * @author greentea
 */
class TranslationInheritanceTest extends Specification {

    def 'test inherit property converter'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean1).translationTo(ParentDest)
                .srcProperty("age")
                .translatesTo("age", new StringToIntConverter())
        def t = new ObjectTranslator(c)
        Bean1 src = new Bean1("15", "Sam")
        then:
        ChildDest1 dest = t.translate(src, ChildDest1)
        dest.name == src.name
        dest.age == 15
    }

    def 'test inherit dest property path and converter'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean1).translationTo(ParentDest)
                .srcProperty("age")
                .translatesTo("value", new StringToIntConverter())
        def t = new ObjectTranslator(c)
        Bean1 src = new Bean1("15", "Sam")
        then:
        ChildDest1 dest = t.translate(src, ChildDest1)
        dest.name == src.name
        dest.value == 15
    }

    def 'test inherit skipped as source'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean1).translationTo(ParentDest).srcProperty("age").skipAsSource()
        def t = new ObjectTranslator(c)
        Bean1 src = new Bean1("15", "Sam")
        then:
        ChildDest1 dest = t.translate(src, ChildDest1)
        dest.name == src.name
        dest.age == null
    }

    def 'test inherit dest property path and skipped dest changes'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean1).translationTo(ParentDest).srcProperty("age")
                .translatesTo("value").skipAllChanges()
        def t = new ObjectTranslator(c)
        Bean1 src = new Bean1("15", "Sam")
        then:
        ChildDest2 dest = t.translate(src, ChildDest2)
        dest.name == src.name
        dest.value == null
    }

    static class StringToIntConverter implements PropertyConverter {
        @Override
        Object convertValue(@NotNull Object srcValue,
                            @NotNull PropertyDescription srcProperty,
                            @NotNull PropertyDescription destProperty) {
            return ((String)srcValue).toInteger()
        }
    }

    static class Bean1 {
        String age
        String name

        Bean1(String age, String name) {
            this.age = age
            this.name = name
        }
    }

    static abstract class ParentDest<T> {
        Integer age
        T value
    }

    static class ChildDest1 extends ParentDest<Integer> {
        String name
    }

    static class ChildDest2 extends ParentDest<String> {
        String name
    }

}
