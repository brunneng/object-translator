package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.exceptions.PropertyMappingException
import spock.lang.Specification

import java.util.stream.Collectors

class ObjectTranslatorCollectionFieldsTest extends Specification {

  def 'test translation of collection fields'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).setIdentifierProperty("id")
    c.beanOfClass(Bean2.class).setIdentifierProperty("id")

    def ot = new ObjectTranslator(c)
    CollectionFields1 o1 = RandomizerUtils.generateObject(CollectionFields1.class)
    o1.setValues4([new Bean1(1, "title1"), new Bean1(2, "title2")])
    o1.setValues5([new GenericBean<Integer>(5)])
    then:
    def o2 = ot.translate(o1, CollectionFields2.class)
    o1.values1 == o2.values1
    o1.values2.stream().map { v -> v.longValue() }.collect(Collectors.toList()) == o2.values2.toList()
    o1.values3.toList().stream().map { v -> v.longValue() }.collect(Collectors.toList()) == o2.values3
    o2.values4 == [new Bean2(1, "title1"), new Bean2(2, "title2")]
    o2.values5.size() == 1
    o2.values5[0].value instanceof Long
    o2.values5[0].value == 5L
  }

  def 'test mapping of collection fields'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).setIdentifierProperty("id")
    c.beanOfClass(Bean2.class).setIdentifierProperty("id")

    def ot = new ObjectTranslator(c)
    def o1 = new CollectionFields1()
    o1.setValues1([1, 2, 3])
    o1.setValues2([1, 2, 3])
    o1.setValues3([1, 2, 3] as Integer[])
    o1.setValues4([new Bean1(1, "title1"), new Bean1(2, "title2")])

    def o2 = new CollectionFields2()
    o2.setValues1([2, 3])
    o2.setValues2([2, 3] as Long[])
    o2.setValues3([2, 3])

    def b22 = new Bean2(2, "-")
    o2.setValues4([b22, new Bean2(3, "-")])

    then:
    ot.mapBean(o1, o2)
    o1.values1 == o2.values1
    o1.values2.stream().map { v -> v.longValue() }.collect(Collectors.toList()) == o2.values2.toList()
    o1.values3.toList().stream().map { v -> v.longValue() }.collect(Collectors.toList()) == o2.values3
    o2.values4 == [new Bean2(1, "title1"), new Bean2(2, "title2")]
    o2.values4.get(1).is(b22)
  }

  def 'test map with exception'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    def o1 = new CollectionFields1()
    o1.setValues1([1, 2, 3])

    ot.translate(o1, BeanWithUnmodifiableCollection.class)
    then:
    thrown PropertyMappingException
  }

  static class CollectionFields1 {
    List<Integer> values1
    List<Integer> values2
    Integer[] values3
    List<Bean1> values4
    List<GenericBean<Integer>> values5
  }

  static class CollectionFields2 {
    List<Integer> values1
    Long[] values2
    List<Long> values3
    List<Bean2> values4
    List<GenericBean<Long>> values5
  }

  static class GenericBean<T> {
    T value

    GenericBean() {
    }

    GenericBean(T value) {
      this.value = value
    }
  }

  static class BeanWithUnmodifiableCollection {
    List<Integer> values1 = Collections.unmodifiableList(new ArrayList())
  }

  static class Bean1 {
    int id
    String title

    Bean1() {
    }

    Bean1(int id, String title) {
      this.id = id
      this.title = title
    }
  }

  static class Bean2 {
    int id
    String title

    Bean2() {
    }

    Bean2(int id, String title) {
      this.id = id
      this.title = title
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      Bean2 bean2 = (Bean2) o

      if (id != bean2.id) return false
      if (title != bean2.title) return false

      return true
    }

    int hashCode() {
      int result
      result = id
      result = 31 * result + (title != null ? title.hashCode() : 0)
      return result
    }
  }


}
