package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorGenericFieldsTest extends Specification {

  def 'test map bean with generic fields'() {
    when:
    def b1 = new Bean1()
    def ch1 = new ChildGenericBean<Integer>();
    ch1.setValue(5);
    ch1.setValues([1, 2, 3])
    b1.setChild(ch1);

    def ot = new ObjectTranslator();
    def b2 = ot.translate(b1, Bean2.class)
    then:
    b2.child.value instanceof Long
    b2.child.values.stream().allMatch({it instanceof Long})
    b2.child.value == b1.child.value
    b2.child.values == [1L, 2L, 3L]
  }

  static class Bean1 {
    ChildGenericBean<Integer> child;
  }

  static class Bean2 {
    ChildGenericBean<Long> child;
  }

  static class ChildGenericBean<T> {
    T value
    List<T> values
  }
}
