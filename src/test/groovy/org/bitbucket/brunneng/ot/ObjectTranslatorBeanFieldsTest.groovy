package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorBeanFieldsTest extends Specification {

  def 'test translation of bean fields'() {
    when:
    def ot = new ObjectTranslator()
    Bean1 o1 = RandomizerUtils.generateObject(Bean1.class)
    then:
    def o2 = ot.translate(o1, Bean2.class)
    o2 != null
    o2.nested != null
    o2.nested.title == o1.nested.title
  }

  def 'test translation bean with recursion'() {
    when:
    def ot = new ObjectTranslator()
    def o1 = new RecursionBean()
    o1.setTitle("test")
    o1.setChild(o1)

    then:
    def o2 = ot.translate(o1, RecursionBean.class)
    o2.title == o1.title
    o2 != null
    o2.child == o2

    when:
    o1 = new RecursionBean()
    o1.setTitle("test1")
    def o11 = new RecursionBean()
    o1.setChild(o11)
    o11.setTitle("test2")
    o11.setChild(o1)

    then:
    def o3 = ot.translate(o1, RecursionBean.class)
    o3.title == o1.title
    o3.child != null
    o3.child.title == o11.title
    o3.child.child == o3
  }

  def 'test translation bean with recursion and wrong equals'() {
    when:
    def ot = new ObjectTranslator()
    def o1 = new RecursionWithWrongEqualsBean()
    o1.setTitle("test")
    o1.setChild(o1)

    then:
    def o2 = ot.translate(o1, RecursionWithWrongEqualsBean.class)
    o2.title == o1.title
    o2 != null
    o2.child == o2

    when:
    o1 = new RecursionWithWrongEqualsBean()
    o1.setTitle("test1")
    def o11 = new RecursionWithWrongEqualsBean()
    o1.setChild(o11)
    o11.setTitle("test2")
    o11.setChild(o1)

    then:
    def o3 = ot.translate(o1, RecursionWithWrongEqualsBean.class)
    o3.title == o1.title
    o3.child != null
    o3.child.title == o11.title
    o3.child.child == o3
  }

  static class Bean1 {
    NestedBean1 nested;
  }

  static class Bean2 {
    NestedBean2 nested;
  }

  static class NestedBean1 {
    String title;
  }

  static class NestedBean2 {
    String title;
  }

  static class RecursionBean {
    String title;
    RecursionBean child;
  }

  static class RecursionWithWrongEqualsBean {
    String title;
    RecursionWithWrongEqualsBean child;

    boolean equals(Object obj) {
      return false
    }
  }
}
