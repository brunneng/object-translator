package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorInterfaceFieldsTest extends Specification {

  def 'translates from interface source field'() {
    given:
    def ot = new ObjectTranslator()
    def o1 = createBean1("hello")
    when:
    def o2 = ot.translate(o1, Bean2.class)
    then:
    o1.data instanceof DataImpl
    o2.data.value == "hello"
  }

  def 'map to interface dest field'() {
    given:
    def ot = new ObjectTranslator()
    def o1 = createBean1("hello")
    def o2 = createBean1("world")
    when:
    ot.mapBean(o1, o2)
    then:
    o2.data.value == "hello"
  }

  def 'translate to interface dest field'() {
    given:
    def ot = new ObjectTranslator()
    Bean1 o1 = createBean1("hello")
    when:
    def o2 = ot.translate(o1, Bean1)
    then:
    o2.data.value == "hello"
  }

  private static Bean1 createBean1(String value) {
    def res = new Bean1()
    res.data = new DataImpl(value);
    return res
  }

  static class Bean1 {
    Data data
  }

  static class Bean2 {
    DataImpl data
  }

  static interface Data {
    String getValue()
  }

  static class DataImpl implements Data {
    String value

    DataImpl() {
    }

    DataImpl(String value) {
      this.value = value
    }
  }

}
