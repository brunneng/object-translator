package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.introspection.exceptions.NoSuchPropertyException
import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.ot.exceptions.ImpossibleToConvertException
import org.bitbucket.brunneng.ot.exceptions.NoGettersException
import org.bitbucket.brunneng.ot.exceptions.NoSettersException
import org.bitbucket.brunneng.ot.exceptions.TranslationAlreadyConfiguredException
import org.bitbucket.brunneng.ot.property.PropertyConverter
import spock.lang.Specification

import java.util.stream.Collectors

class ConfigurationTranslationTest extends Specification {

  def 'test translatesTo'() {
    setup:
    def t = new Configuration().beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)
    when:
    t.srcProperty("name").translatesTo("bbb")
    then:
    thrown NoSuchPropertyException
    when:
    t.srcProperty("name").translatesTo("count")
    then:
    thrown ImpossibleToConvertException
    when:
    t.srcProperty("name").translatesTo("words")
    then:
    thrown ImpossibleToConvertException
    when:
    t.srcProperty("list1").translatesTo("list1")
    then:
    thrown ImpossibleToConvertException
    when:
    t.srcProperty("map1").translatesTo("map1")
    then:
    thrown ImpossibleToConvertException
    when:
    t.srcProperty("map2").translatesTo("map2")
    then:
    thrown ImpossibleToConvertException
    when:
    t.srcProperty("map3").translatesTo("map3")
    then:
    noExceptionThrown()
    when:
    t.srcProperty("map4").translatesTo("map4")
    then:
    thrown ImpossibleToConvertException
    when:
    t.srcProperty("count").skipAsSource()
    t.srcProperty("name").translatesTo("count", new PropertyConverter() {
      @Override
      Object convertValue(Object srcValue, PropertyDescription srcProperty, PropertyDescription destProperty) {
        return null
      }
    })
    then:
    t.srcProperty("name").getTargetDestProperty() == t.destProperty("count")
    when:
    t = new Configuration().beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)
    t.srcProperty("name").translatesTo("title")
    then:
    t.srcProperty("name").getTargetDestProperty() == t.destProperty("title")
    when:
    t.srcProperty("name").translatesTo("address")
    then:
    thrown TranslationAlreadyConfiguredException
    t.srcProperty("name").getTargetDestProperty() == t.destProperty("title")
  }

  def 'test src and dest properties'() {
    setup:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)
    when:
    t.srcProperty("id") != null
    t.srcProperty("name") != null
    t.srcProperty("count") != null
    then:
    true
    when:
    t.srcProperty("total") != null
    then:
    thrown NoGettersException
    when:
    t.srcProperty("aaa") != null
    then:
    thrown NoSuchPropertyException
    when:
    t.destProperty("id") != null
    t.destProperty("title") != null
    t.destProperty("count") != null
    then:
    true
    when:
    t.destProperty("age") != null
    then:
    thrown NoSettersException
    when:
    t.destProperty("bbb") != null
    then:
    thrown NoSuchPropertyException
  }

  def 'test set skipped dest property change'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)

    def destProperty = t.destProperty("title")
    def changeType = Configuration.ChangeType.SET_NULL
    then:
    !destProperty.isSkippedChange(changeType)

    when:
    destProperty.skipChange(changeType)
    then:
    destProperty.isSkippedChange(changeType)
  }

  def 'test get all src and dest properties'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)

    then:

    t.srcProperties.stream().map{ p -> p.property.name }
            .collect(Collectors.toSet()) == ["id", "name", "count", "list1", "map1", "map2",
                                             "map3", "map4"] as Set

    t.destProperties.stream().map{ p -> p.property.name }
            .collect(Collectors.toSet()) == ["id", "title", "count", "address", "words", "list1", "map1", "map2",
                                             "map3", "map4"] as Set
  }

  def 'test set skip all changes for dest property'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)

    def destProperty = t.destProperty("title")
    then:
    for (changeType in Configuration.ChangeType.values()) {
      assert !destProperty.isSkippedChange(changeType)
    }

    when:
    destProperty.skipAllChanges()
    then:
    for (changeType in Configuration.ChangeType.values()) {
      assert destProperty.isSkippedChange(changeType)
    }
  }

  def 'test set skipped dest property change from property'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)

    def destProperty = t.destProperty("title")
    def changeType = Configuration.ChangeType.SET_NULL
    then:
    !destProperty.isSkippedChange(changeType)

    when:
    destProperty.skipChange(changeType)
    then:
    destProperty.isSkippedChange(changeType)
  }

  def 'test set skipped as source property'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)

    def srcProperty1 = t.srcProperty("id")
    def srcProperty2 = t.srcProperty("name")
    then:
    !srcProperty1.isSkippedAsSource()
    !srcProperty2.isSkippedAsSource()

    when:
    srcProperty1.skipAsSource()
    c.beanOfClass(TestBean.class).property("name").skipAsSource()
    then:
    srcProperty1.isSkippedAsSource()
    srcProperty2.isSkippedAsSource()
  }

  def 'test set default value'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(TestBean.class).translationTo(TestBeanDTO.class)

    def nameSrcProperty = t.srcProperty("name")
    def nameDestProperty = t.destProperty("title")

    def countProperty1 = c.beanOfClass(TestBean.class).property("count")
    def countProperty2 = c.beanOfClass(TestBeanDTO.class).property("count")

    def countSrcProperty = t.srcProperty("count")
    def countDestProperty = t.destProperty("count")
    then:
    nameSrcProperty.getDefaultValue() == null
    nameDestProperty.getDefaultValue() == null
    countSrcProperty.getDefaultValue() == null
    countDestProperty.getDefaultValue() == null
    when:
    nameSrcProperty.setDefaultValue("")
    nameDestProperty.setDefaultValue("")
    countProperty1.setDefaultValue(0.0d)
    countProperty2.setDefaultValue(0.0d)
    then:
    nameSrcProperty.getDefaultValue() == ""
    nameDestProperty.getDefaultValue() == ""
    countSrcProperty.getDefaultValue() == 0.0d
    countDestProperty.getDefaultValue() == 0.0d
  }

  static class TestBean {
    private int id;
    private String name;
    private Double count;
    private List<String> list1
    private Map<String, String> map1;
    private Map<String, String> map2;
    private Map<Key2, String> map3;
    private Map<Key1, String> map4;

    int getId() {
      return id
    }
    void setId(int id) {
      this.id = id
    }

    String getName() {
      return name
    }
    void setName(String name) {
      this.name = name
    }

    Double getCount() {
      return count
    }
    void setCount(Double count) {
      this.count = count
    }

    void setTotal(double total) {
    }
  }

  static class TestBeanDTO {
    private int id
    private String title
    private Double count
    private String address
    private List<String> words
    private List<Long> list1
    private Map<String, Integer> map1;
    private Map<Integer, String> map2;
    private Map<Key1, String> map3;
    private Map<Key2, String> map4;

    int getId() {
      return id
    }
    void setId(int id) {
      this.id = id
    }

    String getTitle() {
      return title
    }
    void setTitle(String name) {
      this.title = name
    }

    Double getCount() {
      return count
    }
    void setCount(Double count) {
      this.count = count
    }

    String getAddress() {
      return address
    }
    void setAddress(String address) {
      this.address = address
    }

    int getAge() {
      return 0
    }

    List<String> getWords() {
      return words
    }
    void setWords(List<String> words) {
      this.words = words
    }
  }

  static class Key1 {
  }

  static class Key2 extends Key1 {
  }
}
