package org.bitbucket.brunneng.ot

import spock.lang.Specification

class SetIdentifierPropertyTest extends Specification {

  public static final String ID_PROPERTY_NAME = "id"

  def 'test setIdentifierProperty'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.setIdentifierProperty(ID_PROPERTY_NAME)
    then:
    bean1Config.getIdentifierProperty().name == ID_PROPERTY_NAME
  }

  def 'test setIdentifierProperty on abstract'() {
    when:
    def c = new Configuration()
    def abstractBeanConfig = c.beanOfClass(AbstractBean.class)
    def bean1Config = c.beanOfClass(Bean1.class)
    abstractBeanConfig.setIdentifierProperty(ID_PROPERTY_NAME)
    then:
    bean1Config.getIdentifierProperty().name == ID_PROPERTY_NAME
    abstractBeanConfig.getIdentifierProperty().name == ID_PROPERTY_NAME
  }

  def 'test markAsIdentifier'() {
    when:
    def c = new Configuration()
    def bean1Config = c.beanOfClass(Bean1.class)
    bean1Config.property(ID_PROPERTY_NAME).markAsIdentifier()
    then:
    bean1Config.getIdentifierProperty().name == ID_PROPERTY_NAME
  }

  def 'test markAsIdentifier on abstract'() {
    when:
    def c = new Configuration()
    def abstractBeanConfig = c.beanOfClass(AbstractBean.class)
    def bean1Config = c.beanOfClass(Bean1.class)
    abstractBeanConfig.property(ID_PROPERTY_NAME).markAsIdentifier()
    then:
    bean1Config.getIdentifierProperty().name == ID_PROPERTY_NAME
    abstractBeanConfig.getIdentifierProperty().name == ID_PROPERTY_NAME
  }

  static class AbstractBean {
    int id
  }

  static class Bean1 extends AbstractBean {

  }
}
