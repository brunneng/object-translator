package org.bitbucket.brunneng.ot.package1;

/**
 * @author evvo
 */
public class SyntheticTestBean extends B {
}

class B {
    private int size;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
