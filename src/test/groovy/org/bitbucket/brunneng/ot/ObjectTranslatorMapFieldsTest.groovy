package org.bitbucket.brunneng.ot

import spock.lang.Specification

class ObjectTranslatorMapFieldsTest extends Specification {

  def 'test translation of map fields'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).setIdentifierProperty("id")
    c.beanOfClass(Bean2.class).setIdentifierProperty("id")

    def ot = new ObjectTranslator(c)
    def o1 = new MapFields1()
    o1.map1 = ["a":1, "b":2, "c":3]
    o1.map2 = ["a":1, "b":2, "c":3]
    o1.map3 = ["a":new Bean1(1, "title1"), "b":new Bean1(2, "title2")]
    o1.map4 = ["a":new GenericBean(1, 5)]
    then:
    def o2 = ot.translate(o1, MapFields2.class)
    o2.map1 == o1.map1
    o2.map2 == ["a":1L, "b":2L, "c":3L]
    o2.map3 == ["a":new Bean2(1, "title1"), "b":new Bean2(2, "title2")]
    o2.map4 == ["a": new GenericBean<>(1, 5L)]
  }

  def 'test mapping of map fields'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).setIdentifierProperty("id")
    c.beanOfClass(Bean2.class).setIdentifierProperty("id")
    c.beanOfClass(GenericBean.class).setIdentifierProperty("id")

    def ot = new ObjectTranslator(c)
    def o1 = new MapFields1()
    o1.map1 = ["a":1, "b":2, "c":3]
    o1.map2 = ["a":1, "b":2, "c":3]
    o1.map3 = ["a":new Bean1(1, "title1"), "c":new Bean1(2, "title2")]
    o1.map4 = ["a": new GenericBean(1, 5)]

    def o2 = new MapFields2()
    o2.map1 = ["a":1, "b":2]
    o2.map2 = ["a":2L, "b":3L]
    def b22 = new Bean2(2, "-")
    o2.map3 = ["a":b22, "b":new Bean2(3, "-")]
    def gb1 = new GenericBean(1, 1L)
    o2.map4 = ["a": gb1]

    then:
    ot.mapBean(o1, o2)
    o1.map1 == o2.map1
    o2.map2 == ["a":1L, "b":2L, "c":3L]
    o2.map2.values().stream().allMatch({ it instanceof Long })
    o2.map3 == ["a":new Bean2(1, "title1"), "c":new Bean2(2, "title2")]
    o2.map3["a"].is(b22)
    o2.map4 == ["a": new GenericBean<>(1, 5L)]
    o2.map4["a"].is(gb1)
  }

  static class MapFields1 {
    Map<String, Integer> map1
    Map<String, Integer> map2
    Map<String, Bean1> map3
    Map<String, GenericBean<Integer>> map4
  }

  static class MapFields2 {
    Map<String, Integer> map1
    Map<String, Long> map2
    Map<String, Bean2> map3
    Map<String, GenericBean<Long>> map4
  }

  static class GenericBean<T> {
    int id
    T value

    GenericBean(int id, T value) {
      this.id = id
      this.value = value
    }

    GenericBean() {
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      GenericBean that = (GenericBean) o

      if (id != that.id) return false
      if (value.getClass() != that.value.getClass()) return false
      if (value != that.value) return false

      return true
    }

    int hashCode() {
      int result
      result = id
      result = 31 * result + (value != null ? value.hashCode() : 0)
      return result
    }
  }

  static class Bean1 {
    int id
    String title

    Bean1() {
    }

    Bean1(int id, String title) {
      this.id = id
      this.title = title
    }
  }

  static class Bean2 {
    int id
    String title

    Bean2() {
    }

    Bean2(int id, String title) {
      this.id = id
      this.title = title
    }

    boolean equals(o) {
      if (this.is(o)) return true
      if (getClass() != o.class) return false

      Bean2 bean2 = (Bean2) o

      if (id != bean2.id) return false
      if (title != bean2.title) return false

      return true
    }

    int hashCode() {
      int result
      result = id
      result = 31 * result + (title != null ? title.hashCode() : 0)
      return result
    }
  }


}
