package org.bitbucket.brunneng.ot.concurrency

import org.bitbucket.brunneng.ot.ObjectTranslator
import org.bitbucket.brunneng.ot.RandomizerUtils
import spock.lang.Specification

import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.concurrent.CyclicBarrier

class ConcurrencyTest extends Specification {

  def 'test concurrent translation'() {
    when:
    PostTutorialSurveyVersion v = RandomizerUtils.generateObject(PostTutorialSurveyVersion.class)
    TutorialFeedback tf = RandomizerUtils.generateObject(TutorialFeedback.class);

    then:
    for (int n = 0; n < 100; ++n) {
      System.out.println("Attempt: " + (n + 1));
      ObjectTranslator ot = new ObjectTranslator()

      final CyclicBarrier gate = new CyclicBarrier(3);
      int repeatsCount = 1000;

      Thread t1 = new Thread(new Runnable() {
        @Override
        void run() {
          try {
            gate.await();
          } catch (Exception e) {
            e.printStackTrace();
          }
          for (int i = 0; i < repeatsCount; ++i) {
            ot.translate(v, PostTutorialSurveyVersionDTO.class);
          }
        }
      })
      Thread t2 = new Thread(new Runnable() {
        @Override
        void run() {
          try {
            gate.await();
          } catch (Exception e) {
            e.printStackTrace();
          }
          for (int i = 0; i < repeatsCount; ++i) {
            ot.translate(tf, TutorialFeedbackReadDTO.class);
          }
        }
      });

      t1.start();
      t2.start();
      gate.await();

      t1.join();
      t2.join();

    }
  }

  abstract static class AbstractEntity {

    String id;

    @Override
    boolean equals(Object o) {
      if (this.is(o)) return true;
      if (o == null || getClass() != o.getClass()) return false;

      AbstractEntity that = (AbstractEntity) o;

      if (id == null && that.id == null) {
        return false;
      }

      return Objects.equals(id, that.id);
    }

    @Override
    int hashCode() {
      return id != null ? id.hashCode() : 0;
    }
  }

  static class PostTutorialSurveyVersion extends AbstractEntity {
    String version;
    Boolean active;
    String description;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;

    List<PostTutorialSurveyQuestion> postTutorialSurveyQuestions = new ArrayList<>();
  }

  static class PostTutorialSurveyQuestion extends AbstractEntity {
    PostTutorialSurveyVersion postTutorialSurveyVersion;

    String htmlText;

    Integer order;

    PostTutorialSurveyQuestionType questionType;

    LocalDateTime createdAt;
    LocalDateTime updatedAt;

    List<PostTutorialSurveyQuestionOption> postTutorialSurveyQuestionOptions = new ArrayList<>();
  }

  enum PostTutorialSurveyQuestionType {
    EMOJI, SINGLE_CHOICE, FREE_TEXT
  }

  static class PostTutorialSurveyQuestionOption extends AbstractEntity {
    PostTutorialSurveyQuestion postTutorialSurveyQuestion;

    String htmlText;

    String triggerQuestionId;
    Integer order;

    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class PostTutorialSurveyVersionDTO {
    String id;
    String version;
    Boolean active;
    String description;
    List<PostTutorialSurveyQuestionDTO> postTutorialSurveyQuestions = new ArrayList<>();
  }

  static class PostTutorialSurveyQuestionDTO {
    String id;
    PostTutorialSurveyQuestionType questionType;
    Integer order;
    String htmlText;
    List<PostTutorialSurveyQuestionOptionDTO> postTutorialSurveyQuestionOptions = new ArrayList<>();
  }

  static class PostTutorialSurveyQuestionOptionDTO {
    String id;
    String htmlText;
    Integer order;
    String triggerQuestionId;
  }

  static class TutorialFeedback extends AbstractEntity {

    String versionId;

    Tutorial tutorial;

    Boolean hasRedFlags;

    Boolean completed = false;
    LocalDateTime completedAt;

    List<TutorialFeedbackAnswer> answers = new ArrayList<>();

    List<TutorialRedFlag> redFlags = new ArrayList<>();

    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  class Tutorial {
    String id;

    Session session;

    String tutorId;
    LocalDateTime startedAt;
    LocalDateTime endedAt;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;

    List<TutorialLesson> tutorialLessons = new ArrayList<>();

    List<PostTutorialSurvey> postTutorialSurveys = new ArrayList<>();

    List<LearningObjective> learningObjectives = new ArrayList<>();

    TutorialFeedback tutorialFeedback;

    List<EffortCoin> effortCoins = new ArrayList<>();
  }

  class Session extends AbstractEntity {
    Student student;
    Boolean preQuizCompleted;
    Boolean postQuizCompleted;
    Boolean completed;
    Boolean postTutorialSurveyCompleted;
    Boolean preTutorialMoodSurveyCompleted;
    Boolean postTutorialMoodSurveyCompleted;
    LocalDateTime startTime;
    LocalDateTime endTime;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;

    Tutorial tutorial;

    List<MoodCheckAnswer> moodCheckAnswers = new ArrayList<>();

    List<StudentAnswer> studentAnswers = new ArrayList<>();
  }

  static class Student {

    private String userId;

    private String name;

    private String email;

    private LocalDate dateOfBirth;

    private String noteToTutor;

    private String customerId;
    private Boolean englishFirstLanguage;

    private Gender gender;

    private SpeakingEnglishLevel speakingEnglishLevel;

    private UnderstandingEnglishLevel understandingEnglishLevel;

    private MathsLevel mathsLevel;

    private String programmeId;
    private String subscriptionDuration;

    private boolean deleted;
    private LocalDateTime deletedAt;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private List<EnrolledLesson> enrolledLessons = new ArrayList<>();

    private Set<LearningBarrier> learningBarriers = new HashSet<>();

    private Set<StudentYearGroup> studentYearGroups;

    private Set<SupervisorToStudentLink> supervisors = new HashSet<>();

    private Set<Session> sessions = new HashSet<>();

    private Set<StudentAnswer> studentAnswers = new HashSet<>();

    private Set<AttitudeSurvey> attitudeSurveys = new HashSet<>();

    private Set<MathsAssessment> mathsAssessments = new HashSet<>();

  }

  static enum Gender {
    MALE,
    FEMALE,
    OTHER;
  }

  static enum SpeakingEnglishLevel {
    DIFFICULT,
    OK,
    EASY;
  }

  static enum UnderstandingEnglishLevel {
    DIFFICULT,
    OK,
    EASY;
  }

  static enum MathsLevel {
    EMERGING,
    WORKING_AT,
    EXCEEDING,
    UNKNOWN;
  }

  static class EnrolledLesson extends AbstractEntity {

    String lessonId;
    String programmeId;

    Integer order;

    Student student;

    EnrolledLessonStatus status;

    boolean deleted;

    LocalDateTime createdAt;

    LocalDateTime updatedAt;
  }

  static enum EnrolledLessonStatus {
    INCOMPLETE,
    IN_PROGRESS,
    COMPLETED;
  }

  static class LearningBarrier extends AbstractEntity {

    Student student;

    BarrierType barrierType;

    Severity severity;

    LocalDateTime createdAt;
    LocalDateTime updatedAt;

  }

  static enum BarrierType {
    DYSLEXIA,
    ADD_ADHD,
    DYSCALCULIA,
    SPEECH_LANGUAGE_DIFFICULTIES,
    AUTISM,
    ASPERGER_SYNDROME,
    READING_DIFFICULTIES;
  }

  static enum Severity {
    MILD,
    MODERATE,
    SEVERE;
  }

  static class TutorialFeedbackAnswer extends AbstractEntity {
    String questionId;
    String answerId;
    String lessonId;
    String tutorTutorialFeedbackId;
    String freeText;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class TutorialRedFlag extends AbstractEntity {
    String tutorialRedFlagReasonId;
    String note;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class TutorialLesson extends AbstractEntity {
    Tutorial tutorial;
    String enrolledLessonId;
    String lessonId;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class PostTutorialSurvey extends AbstractEntity {
    PostTutorialSurveyVersion postTutorialSurveyVersion;
    Tutorial tutorial;
    Boolean completed;
    LocalDateTime completedAt;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
    List<PostTutorialSurveyStudentAnswer> postTutorialSurveyStudentAnswers = new ArrayList<>();
  }

  static class PostTutorialSurveyStudentAnswer extends AbstractEntity {
    PostTutorialSurvey postTutorialSurvey;
    String answerId;
    String questionId;
    String freeText;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class LearningObjective extends AbstractEntity {
    Tutorial tutorial;
    String learningObjectiveId;
    LearningObjectiveProgress progress;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static enum LearningObjectiveProgress {
    NOT_TAUGHT(0),
    NEEDS_MORE_WORK(1),
    KNOWLEDGE_REINFORCED(2),
    NEW_CONTENT_LEARNT(3);

    private int order;

    private LearningObjectiveProgress(int order) {
      this.order = order;
    }
  }

  static class EffortCoin extends AbstractEntity {
    Tutorial tutorial;
    Integer coins;
    Instant createdAt;
    Instant updatedAt;
  }

  static class MoodCheckAnswer extends AbstractEntity {
    Session session;
    Question questionId;
    Answer answerId;
    Context context;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static enum Question {
    TODAY_I_AM_FEELING,
    NOW_I_AM_FEELING;
  }

  static enum Answer {
    EXCITED,
    HAPPY,
    BORED,
    NERVOUS,
    SAD,
    ANGRY;
  }

  static enum Context {
    PRE,
    POST;
  }

  public class StudentAnswer extends AbstractEntity {

    private Student student;

    private MathsAssessment mathsAssessment;

    private String lessonId;
    private String questionId;

    private Session session;

    private String answerId;
    private Boolean isCorrect;

    private Integer timeTakenInSeconds;

    private StudentAnswerContext context;
    private DifficultyLevel difficultyLevel;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
  }

  static class MathsAssessment extends AbstractEntity {
    Student student;
    String questionnaireId;
    LocalDateTime viewedAt;
    String viewedBy;
    Boolean completed = false;
    LocalDateTime completedAt;
    Integer mark;
    LocalDateTime startedAt;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
    YearGroup yearGroup;
    MathsAssessmentOutcome testOutcome;
    MathsAssessmentType assessmentType;
    Boolean active = true;
    List<StudentAnswer> answers;
  }

  static enum YearGroup {
    YEAR_1,
    YEAR_2,
    YEAR_3,
    YEAR_4,
    YEAR_5,
    YEAR_6,
    YEAR_7,
    YEAR_6_EXT,
    ADULT,
    SATS;
  }

  static enum MathsAssessmentOutcome {
    EMERGING,
    WORKING_TOWARDS,
    WORKING_AT,
    GREATER_DEPTH,
    EXCEEDING;
  }

  static enum MathsAssessmentType {
    MATHS_LEVEL_TEST,
    MATHS_ASSESSMENT,
    SATS_ASSESSMENT;
  }

  static enum StudentAnswerContext {
    PRE,
    POST,
    MATHS_LEVEL_TEST,
    SATS_ASSESSMENT,
    MATHS_ASSESSMENT;
  }

  static enum DifficultyLevel {
    EMERGING,
    WORKING_AT,
    EXCEEDING;
  }

  static class StudentYearGroup extends AbstractEntity {
    Student student;
    YearGroup yearGroup;
    Boolean active;
    LocalDate startDate;
    LocalDate endDate;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class SupervisorToStudentLink extends AbstractEntity {
    String userId;
    Student student;
    Instant createdAt;
    Instant updatedAt;
  }

  static class AttitudeSurvey extends AbstractEntity {

    Student student;
    Boolean completed = false;
    LocalDateTime completedAt;
    List<AttitudeSurveyAnswer> answers = new ArrayList<>();
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class AttitudeSurveyAnswer extends AbstractEntity {
    AttitudeSurvey attitudeSurvey;
    String questionId;
    String answerId;
    String freeText;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
  }

  static class TutorialFeedbackReadDTO {
    String id;
    String tutorialId;
    String versionId;
    Boolean completed;
    LocalDateTime completedAt;
    Boolean hasRedFlags;
    List<TutorialFeedbackAnswerReadDTO> answers = new ArrayList<>();
    List<TutorialRedFlagReadDTO> redFlags = new ArrayList<>();
  }

  static class TutorialFeedbackAnswerReadDTO {
    String id;
    String questionId;
    String answerId;
    String lessonId;
    String freeText;
  }

  static class TutorialRedFlagReadDTO {
    String id;
    String tutorialRedFlagReasonId;
    String note;
  }
}
