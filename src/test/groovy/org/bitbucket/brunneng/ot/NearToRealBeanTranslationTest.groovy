package org.bitbucket.brunneng.ot


import spock.lang.Specification

class NearToRealBeanTranslationTest extends Specification {

  def 'test translate Order'() {
    when:
    def c = new Configuration()
    def t = c.beanOfClass(Order.class).translationTo(OrderDTO.class)
    t.srcProperty("customer.name").translatesTo("customerName")
    t.srcProperty("customer.billingAddress.street").translatesTo("billingStreetAddress")
    t.srcProperty("customer.billingAddress.city").translatesTo("billingCity")
    t.srcProperty("customer.shippingAddress.street").translatesTo("shippingStreetAddress")
    t.srcProperty("customer.shippingAddress.city").translatesTo("shippingCity")
    Order o = buildOrder()

    ObjectTranslator ot = new ObjectTranslator(c)
    def dto = ot.translate(o, OrderDTO.class)
    then:
    dto.customerName == o.customer.name
    dto.billingStreetAddress == o.customer.billingAddress.street
    dto.billingCity == o.customer.billingAddress.city
    dto.shippingStreetAddress == o.customer.shippingAddress.street
    dto.shippingCity == o.customer.shippingAddress.city
    dto.products[0].name == o.products[0].name
    dto.products[1].name == o.products[1].name
  }

  static Order buildOrder() {
    return RandomizerUtils.generateObject(Order.class)
  }

  static class Order {
    private Customer customer;
    private List<Product> products;
  }

  static class Customer {
    private String name;
    private Address shippingAddress;
    private Address billingAddress;
  }

  static class Product {
    private String name;

    Product(String name) {
      this.name = name;
    }
  }

  static class Address {
    private String street;
    private String city;
  }

  static class OrderDTO {
    private List<ProductDTO> products;
    private String customerName;
    private String shippingStreetAddress;
    private String shippingCity;
    private String billingStreetAddress;
    private String billingCity;
  }

  static class ProductDTO {
    private String name;
  }

}
