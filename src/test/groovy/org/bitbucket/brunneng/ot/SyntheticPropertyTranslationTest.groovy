package org.bitbucket.brunneng.ot

import org.bitbucket.brunneng.ot.package1.SyntheticTestBean
import spock.lang.Specification

/**
 * @author evvo
 */
class SyntheticPropertyTranslationTest extends Specification {

    def 'test synthetic property translation'() {
        when:
        def ot = new ObjectTranslator()
        def bean = new SyntheticTestBean()
        bean.setSize(5)
        def res = ot.translate(bean, SyntheticTestBean.class)
        then:
        bean.getSize() == res.getSize()
    }
}
