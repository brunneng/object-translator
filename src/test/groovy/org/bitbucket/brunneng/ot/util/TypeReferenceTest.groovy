package org.bitbucket.brunneng.ot.util


import spock.lang.Specification

class TypeReferenceTest extends Specification {

    def 'test getGenericParameterType'() {
        expect:
        (new TypeReference<Integer>() {}).getGenericParameterType() == Integer.class
        (new TypeReference() {}).getGenericParameterType() == Object.class
        (new TypeReference<long[]>() {}).getGenericParameterType() == long[].class
    }
}
