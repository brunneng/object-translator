package org.bitbucket.brunneng.ot.creators

import org.bitbucket.brunneng.ot.Configuration
import org.bitbucket.brunneng.ot.ObjectTranslator
import org.bitbucket.brunneng.ot.exceptions.DestValueCreationException
import spock.lang.Specification

import java.lang.reflect.Array

class DestValueCreatorsTest extends Specification {

  def 'test create with constructor without args'(Class objClass, boolean canCreate) {
    when:
    def oc = new ConstructorWithoutArgsDestValueCreator()
    def vcc = new ValueCreationContext(new Object(), null)
    then:
    oc.isCanCreate(objClass) == canCreate
    if (canCreate) {
      def obj = oc.create(objClass, vcc)
      assert obj != null
      assert objClass.isAssignableFrom(obj.getClass())
    }
    where:
    objClass     | canCreate
    String.class | true
    Bean1.class  | true
    Bean2.class  | true
    Bean3.class  | false
  }

  def 'test array creator'(Class arrayClass, Object srcObject, int size) {
    when:
    def oc = new ArrayDestValueCreator()
    def vcc = new ValueCreationContext(srcObject, null)
    then:
    oc.isCanCreate(arrayClass)
    def res = oc.create(arrayClass, vcc)
    res != null
    res.getClass().isArray()
    res.getClass().getComponentType() == arrayClass.getComponentType()
    Array.getLength(res) == size
    where:
    arrayClass           | srcObject              | size
    new Integer[0].class | Arrays.asList(1, 2, 3) | 3
    new String[0].class  | new String[2]          | 2
  }

  def 'test cant create array'() {
    when:
    def oc = new ArrayDestValueCreator()
    def vcc = new ValueCreationContext(new Object(), null)
    def arrayClass = new Integer[0].class
    oc.isCanCreate(arrayClass)
    oc.create(arrayClass, vcc)
    then:
    thrown DestValueCreationException
  }

  def 'test default implementation creation'(Class interfaceClass, Class resultClass) {
    when:
    def oc = new DefaultImplDestValueCreator()
    def vcc = new ValueCreationContext(new Object(), null)
    then:
    oc.isCanCreate(interfaceClass)
    oc.create(interfaceClass, vcc).getClass() == resultClass
    where:
    interfaceClass   | resultClass
    Collection.class | ArrayList.class
    List.class       | ArrayList.class
    Set.class        | HashSet.class
    Map.class        | HashMap.class
  }

  def 'test translation without dest value creator'() {
    when:
    def c = new Configuration()
    def ot = new ObjectTranslator(c)
    Bean1 b1 = new Bean1()
    ot.translate(b1, Bean4.class)
    then:
    thrown DestValueCreationException
  }

  static class Bean1 {
  }

  static class Bean2 {
    private Bean2() {}
  }

  static class Bean3 {
    Bean3(String param) {}
  }

  static class Bean4 {
    Bean4(String value) {
    }
  }
}
