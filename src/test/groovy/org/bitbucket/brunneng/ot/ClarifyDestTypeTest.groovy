package org.bitbucket.brunneng.ot


import org.bitbucket.brunneng.ot.exceptions.DestValueCreationException
import spock.lang.Specification

class ClarifyDestTypeTest extends Specification {

  def 'test clarify of dest value type'() {
    when:
    def ot = new ObjectTranslator()
    def o1 = new Bean1()
    def o2 = ot.translate(o1, I1.class)
    then:
    o2 != null
    o2.getClass() == Bean1.class
    when:
    ot.translate(o1, I2.class)
    then:
    thrown DestValueCreationException
  }

  def 'test dont clarify dest type of collections and maps'() {
    when:
    def ot = new ObjectTranslator()
    def o1 = new Bean2()
    o1.values1 = new LinkedList<Integer>()
    o1.values2 = new TreeMap<>()
    def o2 = ot.translate(o1, Bean2.class)
    then:
    o2.values1 instanceof ArrayList
    o2.values2 instanceof HashMap
  }

  static class Bean1 implements I1 {
  }

  interface I1 {
  }

  interface I2 {
  }

  static class Bean2 {
    List<Integer> values1;
    Map<String, Integer> values2;
  }
}
